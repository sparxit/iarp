<?php

$view = new view();
$view->name = 'products_line_show_case';
$view->description = '';
$view->tag = 'default';
$view->base_table = 'node';
$view->human_name = 'Products Line Show Case (in code)';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['css_class'] = 'row products-inline-showcase';
$handler->display->display_options['use_more_always'] = FALSE;
$handler->display->display_options['use_more_text'] = 'leggi tutto';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['query']['options']['distinct'] = TRUE;
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['exposed_form']['options']['submit_button'] = 'Applica';
$handler->display->display_options['exposed_form']['options']['reset_button_label'] = 'Ripristina';
$handler->display->display_options['exposed_form']['options']['exposed_sorts_label'] = 'Ordina per';
$handler->display->display_options['exposed_form']['options']['sort_desc_label'] = 'Disc';
$handler->display->display_options['pager']['type'] = 'some';
$handler->display->display_options['pager']['options']['items_per_page'] = '6';
$handler->display->display_options['style_plugin'] = 'default';
$handler->display->display_options['style_options']['row_class'] = 'row';
$handler->display->display_options['row_plugin'] = 'fields';
/* Campo: Contenuto: product_image */
$handler->display->display_options['fields']['field_product_showcase_image']['id'] = 'field_product_showcase_image';
$handler->display->display_options['fields']['field_product_showcase_image']['table'] = 'field_data_field_product_showcase_image';
$handler->display->display_options['fields']['field_product_showcase_image']['field'] = 'field_product_showcase_image';
$handler->display->display_options['fields']['field_product_showcase_image']['label'] = '';
$handler->display->display_options['fields']['field_product_showcase_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_showcase_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_product_showcase_image']['settings'] = array(
  'image_style' => '',
  'image_link' => 'content',
);
/* Campo: Contenuto: Titolo */
$handler->display->display_options['fields']['title']['id'] = 'title';
$handler->display->display_options['fields']['title']['table'] = 'node';
$handler->display->display_options['fields']['title']['field'] = 'title';
$handler->display->display_options['fields']['title']['label'] = '';
$handler->display->display_options['fields']['title']['alter']['word_boundary'] = FALSE;
$handler->display->display_options['fields']['title']['alter']['ellipsis'] = FALSE;
/* Criterio di ordinamento: Contenuto: product_order (field_product_order) */
$handler->display->display_options['sorts']['field_product_order_value']['id'] = 'field_product_order_value';
$handler->display->display_options['sorts']['field_product_order_value']['table'] = 'field_data_field_product_order';
$handler->display->display_options['sorts']['field_product_order_value']['field'] = 'field_product_order_value';
/* Filter criterion: Contenuto: Pubblicato */
$handler->display->display_options['filters']['status']['id'] = 'status';
$handler->display->display_options['filters']['status']['table'] = 'node';
$handler->display->display_options['filters']['status']['field'] = 'status';
$handler->display->display_options['filters']['status']['value'] = 1;
$handler->display->display_options['filters']['status']['group'] = 1;
$handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
/* Filter criterion: Contenuto: Tipo */
$handler->display->display_options['filters']['type']['id'] = 'type';
$handler->display->display_options['filters']['type']['table'] = 'node';
$handler->display->display_options['filters']['type']['field'] = 'type';
$handler->display->display_options['filters']['type']['value'] = array(
  'product_line_show_case' => 'product_line_show_case',
);

/* Display: Block */
$handler = $view->new_display('block', 'Block', 'block');
$handler->display->display_options['defaults']['fields'] = FALSE;
/* Campo: Contenuto: product_link */
$handler->display->display_options['fields']['field_product_link_1']['id'] = 'field_product_link_1';
$handler->display->display_options['fields']['field_product_link_1']['table'] = 'field_data_field_product_link';
$handler->display->display_options['fields']['field_product_link_1']['field'] = 'field_product_link';
$handler->display->display_options['fields']['field_product_link_1']['label'] = '';
$handler->display->display_options['fields']['field_product_link_1']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_product_link_1']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_link_1']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_product_link_1']['type'] = 'link_url';
/* Campo: Contenuto: product_image */
$handler->display->display_options['fields']['field_product_showcase_image']['id'] = 'field_product_showcase_image';
$handler->display->display_options['fields']['field_product_showcase_image']['table'] = 'field_data_field_product_showcase_image';
$handler->display->display_options['fields']['field_product_showcase_image']['field'] = 'field_product_showcase_image';
$handler->display->display_options['fields']['field_product_showcase_image']['label'] = '';
$handler->display->display_options['fields']['field_product_showcase_image']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_product_showcase_image']['alter']['path'] = '[field_product_link_1]';
$handler->display->display_options['fields']['field_product_showcase_image']['alter']['target'] = '_blank';
$handler->display->display_options['fields']['field_product_showcase_image']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_showcase_image']['element_wrapper_class'] = 'panel_image image_shadow ';
$handler->display->display_options['fields']['field_product_showcase_image']['click_sort_column'] = 'fid';
$handler->display->display_options['fields']['field_product_showcase_image']['settings'] = array(
  'image_style' => 'medium',
  'image_link' => '',
);
/* Campo: Contenuto: product_link */
$handler->display->display_options['fields']['field_product_link']['id'] = 'field_product_link';
$handler->display->display_options['fields']['field_product_link']['table'] = 'field_data_field_product_link';
$handler->display->display_options['fields']['field_product_link']['field'] = 'field_product_link';
$handler->display->display_options['fields']['field_product_link']['label'] = '';
$handler->display->display_options['fields']['field_product_link']['exclude'] = TRUE;
$handler->display->display_options['fields']['field_product_link']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['field_product_link']['alter']['path'] = '[field_product_link_1]';
$handler->display->display_options['fields']['field_product_link']['alter']['target'] = '_new';
$handler->display->display_options['fields']['field_product_link']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['field_product_link']['click_sort_column'] = 'url';
$handler->display->display_options['fields']['field_product_link']['type'] = 'link_url';
/* Campo: Contenuto: Titolo */
$handler->display->display_options['fields']['title_field']['id'] = 'title_field';
$handler->display->display_options['fields']['title_field']['table'] = 'field_data_title_field';
$handler->display->display_options['fields']['title_field']['field'] = 'title_field';
$handler->display->display_options['fields']['title_field']['label'] = '';
$handler->display->display_options['fields']['title_field']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['title_field']['alter']['path'] = '[field_product_link_1]';
$handler->display->display_options['fields']['title_field']['alter']['target'] = '_blank';
$handler->display->display_options['fields']['title_field']['element_label_colon'] = FALSE;
$handler->display->display_options['fields']['title_field']['element_wrapper_class'] = 'views-field-field-product-link panel_title';
$handler->display->display_options['fields']['title_field']['link_to_entity'] = 0;
$handler->display->display_options['block_description'] = 'Products Line Show case';
$translatables['products_line_show_case'] = array(
  t('Master'),
  t('leggi tutto'),
  t('Applica'),
  t('Ripristina'),
  t('Ordina per'),
  t('Asc'),
  t('Disc'),
  t('Block'),
  t('Products Line Show case'),
);


/* Define this new view from code in views list */
$views[$view->name] = $view;
<?php
/**
 * @file
 * crea_contenuto_slider_.features.inc
 */

/**
 * Implements hook_node_info().
 */
function crea_contenuto_slider__node_info() {
  $items = array(
    'slider' => array(
      'name' => t('Slider'),
      'base' => 'node_content',
      'description' => t('Use to create slides in header-slider.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

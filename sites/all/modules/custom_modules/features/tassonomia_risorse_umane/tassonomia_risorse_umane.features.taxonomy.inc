<?php
/**
 * @file
 * tassonomia_risorse_umane.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function tassonomia_risorse_umane_taxonomy_default_vocabularies() {
  return array(
    'risorse_umane' => array(
      'name' => 'Risorse Umane',
      'machine_name' => 'risorse_umane',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}

<?php
/**
 * @file
 * tassonomia_risorse_umane.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function tassonomia_risorse_umane_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'taxonomy_term-risorse_umane-description_field'
  $field_instances['taxonomy_term-risorse_umane-description_field'] = array(
    'bundle' => 'risorse_umane',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'anteprima_tk' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 4,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'description_field',
    'label' => 'Descrizione',
    'required' => FALSE,
    'settings' => array(
      'display_summary' => 0,
      'entity_translation_sync' => FALSE,
      'hide_label' => array(
        'entity' => FALSE,
        'page' => FALSE,
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'rows' => 20,
        'summary_rows' => 5,
      ),
      'type' => 'text_textarea_with_summary',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'taxonomy_term-risorse_umane-field_image'
  $field_instances['taxonomy_term-risorse_umane-field_image'] = array(
    'bundle' => 'risorse_umane',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'anteprima_tk' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => 'content',
          'image_style' => '95x95',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'our_team_80x80',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_image',
    'label' => 'Immagine Logo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'entity_translation_sync' => 0,
      'file_directory' => 'imm_risorse_umane',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '1MB',
      'max_resolution' => '150x150',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'taxonomy_term-risorse_umane-field_immagine_testata'
  $field_instances['taxonomy_term-risorse_umane-field_immagine_testata'] = array(
    'bundle' => 'risorse_umane',
    'deleted' => 0,
    'description' => 'Inserire qui l\'immagine da far comparire come testata della pagina della specifica Area Aziendale',
    'display' => array(
      'anteprima_tk' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'threshold-768',
        ),
        'type' => 'image',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_immagine_testata',
    'label' => 'Immagine Testata',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 393,
      'entity_translation_sync' => FALSE,
      'file_directory' => 'imm_risorse_umane',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '3MB',
      'max_resolution' => '10000x1000',
      'min_resolution' => '',
      'title_field' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'taxonomy_term-risorse_umane-name_field'
  $field_instances['taxonomy_term-risorse_umane-name_field'] = array(
    'bundle' => 'risorse_umane',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'anteprima_tk' => array(
        'label' => 'hidden',
        'module' => 'title',
        'settings' => array(
          'title_class' => '',
          'title_link' => 'content',
          'title_style' => 'div',
        ),
        'type' => 'title_linked',
        'weight' => 1,
      ),
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'full' => array(
        'label' => 'hidden',
        'module' => 'title',
        'settings' => array(
          'title_class' => '',
          'title_link' => '',
          'title_style' => 'h2',
        ),
        'type' => 'title_linked',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'name_field',
    'label' => 'Nome',
    'required' => TRUE,
    'settings' => array(
      'entity_translation_sync' => FALSE,
      'hide_label' => array(
        'entity' => FALSE,
        'page' => FALSE,
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Descrizione');
  t('Immagine Logo');
  t('Immagine Testata');
  t('Inserire qui l\'immagine da far comparire come testata della pagina della specifica Area Aziendale');
  t('Nome');

  return $field_instances;
}

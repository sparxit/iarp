<link href="/<?php echo $path; ?>/css/concept.css" rel="stylesheet" type="text/css" />
<link href="/<?php echo $path; ?>/timeline/css/eurocryor_flat.css" rel="stylesheet" type="text/css" />
<link href="/<?php echo $path; ?>/timeline/css/style.css" rel="stylesheet" type="text/css" />
<link href="/<?php echo $path; ?>/timeline/css/lightbox.css" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="/<?php echo $path; ?>/timeline/js/jquery.mCustomScrollbar.js"></script>
<script type="text/javascript" src="/<?php echo $path; ?>/timeline/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="/<?php echo $path; ?>/timeline/js/jquery.timeline.js"></script>
<script type="text/javascript" src="/<?php echo $path; ?>/timeline/js/image.js"></script>
<script type="text/javascript" src="/<?php echo $path; ?>/timeline/js/lightbox.js"></script>

<!--<script type="text/javascript" src="/sites/all/themes/goodnex/js/fancybox/helpers/jquery.fancybox-media.js?v=1.0.6"></script>-->

<script>
$(document).ready(function(){
   
   	// light
	$('#myTimeline').timeline({
		openTriggerClass : '.read_more',
                closeText: '',
//		startItem : '01/03/2014',
//                startItem : '<?php echo date("d/m/Y"); ?>',
                startItem : 'first',
                categories : [
                    '<?php echo t('January'); ?>', 
                    '<?php echo t('February'); ?>',
                    '<?php echo t('Marzo'); ?>', 
                    '<?php echo t('April'); ?>',
                    '<?php echo t('May'); ?>',
                    '<?php echo t('June'); ?>',
                    '<?php echo t('July'); ?>',
                    '<?php echo t('August'); ?>',
                    '<?php echo t('September'); ?>',
                    '<?php echo t('October'); ?>',
                    '<?php echo t('November'); ?>',
                    '<?php echo t('December'); ?>',
                ],
                yearsOn: true,
	});
        
        
});

</script>

<?php // print_r($concept);die; ?>
<div class="concept_container">

    <div class="title_container">
        <h2><?php echo $concept->features['title']; ?></h2>
        <?php if(strlen($concept->features['description'])>0): ?>
            <p><?php echo $concept->features['description']; ?></p>
        <?php endif; ?>
        
    </div>
    
    <div class="timelineLoader">
            <img src="/<?php echo $path; ?>/timeline/images/timeline/loadingAnimation.gif" />
    </div>
    <div id="myTimeline" class="timelineFlat timelineFlatBlog tl2">
        
<?php foreach ($concept->resources as $cnt => $res ): ?>
    <?php 
        $res_date   = date('d/m/Y', $res['res_features']['DATA']['integer_value'] );
        $res_month  = date('M', $res['res_features']['DATA']['integer_value'] );
        $res_day    = date('d', $res['res_features']['DATA']['integer_value'] );
        $res_mm     = date('d', $res['res_features']['DATA']['integer_value'] );
        switch ($res['mime_type']){
            case "image/jpg":
            case "image/png":
            default:
                
                break;
            
            case "application/pdf":
                
                break;
            
        }
    ?>
        <div id="<?php echo $res['id_resource'];?>" class="item" data-name="<?php echo $res['res_features']['IMG_TITLE']['string_value']; ?>"  data-id="<?php echo $res_date; ?>" data-description="<?php echo $res_date; ?>">
    
<?php 
/**
 * Resource is: VIDEO, IMG, DOC
 */
?>            <!--<a class="image_rollover_bottom con_borderImage" rel="lightbox[timeline]" href="<?php echo variable_get('url_esp2_exporteweb')."/". $res['file_name']; ?>">-->
        <?php 
        if($res['has_video']) { //VIDEO VIMEO
            $lb_class = "fancybox lab_video";
            $lb_href  = "http://player.vimeo.com/video/".$res['res_features']['ID_VIMEO']['string_value']; //embed vimeo player  (VIMEO ONLY)
//            $lb_href  = "http://player.vimeo.com/video/101274579"; //embed vimeo player  (VIMEO ONLY)
//            http://player.vimeo.com/video/101274579?width=800&height=600&iframe=true&fullscreen=1&autoplay=1
//            $lb_thumb = "/$path/images/video_icon.png";
            $lb_thumb = variable_get('url_esp2_exporteweb')."/". $res['file_name'];
        }  else {
            if($res['mime_type']=="application/pdf") { //DOC PDF
                $lb_class = "lab_pdf";
                strlen($res['file_name'])>0 ? $lb_href = variable_get('url_esp2_exporteweb')."/". $res['file_name'] : $lb_href = "#";
//                $lb_thumb = "/$path/images/dwnl_icon.png";
                $lb_thumb = variable_get('url_esp2_exporteweb')."/". $res['file_name'];
                $lb_target = "target='_blank'";
            } else { //IMG
                $lb_class = "fancybox lab_img";
                strlen($res['file_name'])>0 ? $lb_href = variable_get('url_esp2_exporteweb')."/". $res['file_name'] : $lb_href = "#";
                $lb_thumb = variable_get('url_esp2_exporteweb')."/". $res['file_name'];
            }

        }

		$descr = filter_var( trim($res['res_features']['DESCRIZIONE']['text_value']), FILTER_SANITIZE_STRING) ;
		$descr = trim(preg_replace('/[^(\x20-\x7F)]*/','', $descr)); //remove non utf8 char and blank spaces
//                $extra_descr = " The path of the righteous man is beset on all sides by the iniquities of the selfish and the tyranny of evil men. Blessed is he who, in the name of charity and good will, shepherds the weak through the valley of darkness, for he is truly his brother's keeper and the finder of lost children. And I ";
        ?>
                <!--image_rollover_bottom con_borderImage-->

            <a class="<?php echo $lb_class; ?>" data-rel="<?php echo $res['mime_type']; ?>" href="<?php echo $lb_href; ?>" <?php echo $lb_target; ?>>
               <img src="<?php echo $lb_thumb; ?>">
            </a>

        <!--<div class="post_date" ><?php echo $res_day; ?><span><?php echo t($res_month); ?></span></div>-->
        <!--<h2><?php echo $res_date; ?></h2>-->
        <span>
            <h4><?php echo $res['res_features']['IMG_TITLE']['string_value']; ?></h4>
            <p><?php echo truncate_utf8($descr, 70, true, true); ?></p>
        </span>
        <div data-id="<?php echo $res_date; ?>" class="read_more" data-count="0"><?php echo t('Read more'); ?></div>

    </div>
    <div class="item_open" data-id="<?php echo $res_date; ?>">
        
        <div class="item_open_cwrapper" style="position: relative;">
            <div data-id="<?php echo $res_date; ?>" data-count="3" class="t_close"><?php echo t('Close'); ?></div>
            <div class="item_open_content">
                    <div class="timeline_open_content">
                        <h2><?php echo $res['res_features']['IMG_TITLE']['string_value']; ?></h2>
                        <p> <?php echo $res['res_features']['DESCRIZIONE']['text_value']; ?></p>
                    </div>
            </div>
        </div>
          
    </div>
    
<?php endforeach; ?>
        
    </div>
</div>


<?php // echo"<br>Docs:<br>"; print_r($aDocs);die; ?>
<link rel="stylesheet" href="/<?php echo $path ?>/css/concept.css">
<script type="text/javascript" src="/<?php echo $path; ?>/js/concept.js"></script>

<div class="concept_container">
    <ul>
    <?php foreach ($aDocs as $cat => $doc): ?> 
    
        <?php $p = $doc['product']; ?>
        <li class="concept_item" id="<?php echo $p->idProduct; ?>">
            <a href="<?php echo url('lab', array( "query"=>array('cid'=>$p->idProduct) ) ); ?>">
                <div class="concept_thumb">
                    <img src="<?php echo variable_get('url_esp2_exporteweb', NULL)."/".$p->mainImg; ?>">
                </div>
                <div class="concept_data">
                    <div class="concept_title"><?php echo $p->features['title']; ?></div>
                    <div class="concept_date">
                        <div><?php echo t('Date').":".$p->features['data_start_f']; ?></div>
                        <!--<div><?php echo t('End date').":".$p->features['data_end_f']; ?></div>-->
                    </div>
                </div>
            </a>
        </li>
    <?php endforeach; ?>
    </ul>
</div>



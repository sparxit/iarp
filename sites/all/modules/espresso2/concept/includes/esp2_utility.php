<?php

/**
 * Class for Espresso2 general utility and constants
 * 
 */

class Esp2ConceptUtility {
  
    const CHANNEL_CODE              = 'web';
    const MAIN_TAXONOMY             = 'TAS_EUROCRYOR';
//    const CATALOGUE_CODE    = 'EUROCRYOR';
    const CONCEPT_PROD_CLASS        = "CL_PR_EUROCRYOR_LAB";
    const CONCEPT_TAXTERM           = "contenuti_aggiuntivi";
    const CONCEPT_RESGROUP          = "concept"; //Resource Group delle risorse dei prodotti concept
    const CONCEPT_RESCLASS          = "CL_RIS_CONCEPT"; //Resource Class delle risorse dei prodotti concept
    const CONCEPT_FEATURE_TITLE     = "CONCEPT_TITLE"; //feature per attributo titolo
    const CONCEPT_FEATURE_CAT       = "CAT_CONCEPT_LAB_EUROCRYOR"; //feature category x prodotti concept
    const CONCEPT_RES_FEATURE_CAT   = "CAT_RIS_CONCEPT_LAB_EUROCRYOR"; //feature category x attributi delle risorse dei prodotti concept
    const P_DESC_FEATURE            = 'DESCRIZIONE_PRODOTTO'; //feature: descrizione prodotto
    const P_DT_START_FEATURE        = 'DATA_INIZIO'; //feature: data inizio
    const P_DT_END_FEATURE          = 'DATA_FINE'; //feature: data fine
    const P_PRODNAME_FEATURE        = 'CONCEPT_TITLE'; //feature nome prodotto
//    const P_RESCLASS_MAIN_IMG       = 'CL_RIS_IMMAGINI_PRINCIPALE'; //resource class dell'immagine principale
    const P_RES_DATE_FEATURE_CODE   = 'DATA'; //Codice feature per l'attributo della risorsa da usare come data
    const P_VIDEO_FEATURE           = 'ID_VIMEO'; //Feature code x identificare l'attributo VIDEO
//    const P_RESCLASS_PDF_SHEET = 'CL_RIS_SCHEDAPDF'; //resoource class della scheda pdf

    
    /** 
     * Mappa livelli di privilegio
     * Mappatura dei valori  e' statica, andrebbe aggiunto un campo su db x mapparlo direttamente li (tab. company_class) 
     */
    static $cfg = array(
        array( 
            'res' => array(
                'INTERNO' => 20,
                'CLIENTE' => 10,
                'PUBBLICO' => 1,
            ),
            'usr' => array(
                'PUBBLICO' => 1,
                'CLIENTE' => 10,
                'INTERNO' => 20,
                'GESTORE' => 30,
            )
        )
    );
    /**
     * Ritorna mappa dei livelli di privilegio (risorsa e utente)
     * @return array
     */
    public static function getPrivileges(){
        return self::$cfg;
    }
    
    
    public static function getProductClass() {
        return self::CONCEPT_PROD_CLASS;
    }
    public static function getIdProductClass($code=null) {
        if($code==null) $code=self::CONCEPT_PROD_CLASS;
        db_set_active('espresso2');
        $sel= db_select('product_class', 'id_product_class')
                ->fields('id_product_class');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_product_class']; 
    }
    
    public static function getTitleIdFeature($code=null) {
        if($code==null) $code=self::CONCEPT_FEATURE_TITLE;
        db_set_active('espresso2');
        $sel= db_select('feature', 'id_feature')
                ->fields('id_feature');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature']; 
    }
    
    public static function getIdConceptResGroup($code=null) {
        if($code==null) $code=self::CONCEPT_RESGROUP;
        db_set_active('espresso2');
        $sel= db_select('resource_group', 'id_resource_group')
                ->fields('id_resource_group');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_resource_group']; 
    }
    
    public static function getIdChannel($code=null) {
        if($code==null) $code=self::CHANNEL_CODE;
        db_set_active('espresso2');
        $sel= db_select('channel', 'id_channel')
                ->fields('id_channel');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_channel'];
        
    }
    
    public static function getIdLang($code) {
        db_set_active('espresso2');
        $sel= db_select('lang', 'id_lang')
                ->fields('id_lang');
        $sel->condition("iso_code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_lang'];
        
    }

    public static function getIdTaxonomy ( $code=null ) {
        if($code==null) $code=self::MAIN_TAXONOMY;
        db_set_active('espresso2');
        $sel= db_select('taxonomy', 't')
                ->fields('t');
        $sel->condition("erp_code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_taxonomy']; 
    }
    public static function getIdConceptTaxTerm ( $code=null ) {
        if($code==null) $code=self::CONCEPT_TAXTERM;
        $id_taxonomy = self::getIdTaxonomy();
        db_set_active('espresso2');
        $sel= db_select('taxonomy_term', 't')
                ->fields('t');
        $sel->condition("erp_code",$code,"=");
        $sel->condition("id_taxonomy",$id_taxonomy,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_taxonomy_term']; 
    }
    public static function getIdFeatcat($code=null) {
        if($code==null) $code=self::CONCEPT_FEATURE_CAT;
        db_set_active('espresso2');
        $sel= db_select('feature_category', 'id_feature_category')
                ->fields('id_feature_category');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature_category']; 
    }
    
//    
//    public static function getFeatureResource( $idFeature, $featCode, $idLang ) {
//
//        $idResGroup  = Esp2ConceptUtility::getIdConceptResGroup();
////        $idCatalog      = self::getIdCatalogue();
//        $idChannel      = self::getIdChannel(); 
//        $idFeatCatIcon  = self::getIconIdFeatcat(); 
//
//        db_set_active('espresso2');
//        $query ="SELECT 
//                r.id_resource,r.code, r.localized, rc.store_dam, rl.id_lang, 
//                m.id_media, m.id_channel,m.erp_code,m.uri,m.uri_thumb,m.mime_type,m.file_name,m.keywords
//                FROM resource AS r 
//                INNER JOIN resource_class AS rc ON r.id_resource_class = rc.id_resource_class
//                INNER JOIN resource_group_channel AS rgc ON r.id_resource = rgc.id_resource
//                INNER JOIN resource_lang AS rl ON r.id_resource= rl.id_resource AND (rl.id_lang=:id_lang OR rl.id_lang=0)
//                INNER JOIN media AS m ON rl.id_media = m.id_media AND m.id_channel = :id_channel
//
//                INNER JOIN resource_has_feature_value AS rhfv ON r.id_resource = rhfv.id_resource
//                INNER JOIN feature_value_lang AS fvl ON (
//                    rhfv.id_feature_value = fvl.id_feature_value 
//                    AND rhfv.id_feature_category = :id_feature_category 
//    ## ==> id_feature NON VA USATA X JOIN : 
//    ##      feature_code della feature deve essere uguale al feature_value della feature associata alla risorsa
//    ##      FEATURE: Id:3, Code:MERC_CARNE --> feature risorsa: Id:112,Code:RIS_COD_FEATURE,StringValue:MERC_CARNE
//        #AND fvl.id_feature = :id_feature ##id_feature DOESN'T MATCH !!
//                    AND fvl.id_lang in(:id_lang,0)
//                    )
//                WHERE 
//                fvl.string_value = :feature_code
//                AND rgc.id_resource_group = :id_resource_group
//                AND rgc.id_channel = :id_channel
//                LIMIT 1";
//        $result = db_query($query, array( 
//            ':id_resource_group' => $idResGroup, 
//            ':id_feature_category' => $idFeatCatIcon, 
//            ':id_channel' => $idChannel, 
//            ':feature_code' => $featCode, 
//            ':id_feature' => $idFeature, 
//            ':id_catalog' => $idCatalog, 
//            ':id_lang' => $idLang 
//             ), 
//             array( 'fetch' => PDO::FETCH_ASSOC )  
//        );
//        $return =  array();
//        if ($result) {
//            $return = $result->fetchAssoc();
//        } 
//        db_set_active();
//        return $return;
//
//    }
    
}

?>

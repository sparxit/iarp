<?php
/**
 * Class for Espresso2 Concepts page
 * 
 */
class Esp2ConceptLab {

    private $idLang;
    private $idChannel;
    private $idTaxTerm;
    private $prodClass;
    private $idProdClass;
    private $idResGroup;
    private $idFeature;
    private $idFeatureCat;
    private $usrLevel;
    private $cfg;
    

    public function __construct( $langCode ) {
        $this->idLang       = Esp2ConceptUtility::getIdLang($langCode);
        $this->idChannel    = Esp2ConceptUtility::getIdChannel();
        $this->idTaxTerm    = Esp2ConceptUtility::getIdConceptTaxTerm();
        $this->cfg          = Esp2ConceptUtility::getPrivileges();
        //$this->idResGroup   = Esp2ConceptUtility::getIdDocResGroup();
        $this->prodClass    = Esp2ConceptUtility::getProductClass();
        $this->idProdClass  = Esp2ConceptUtility::getIdProductClass();
        $this->idFeatureCat = Esp2ConceptUtility::getIdFeatcat();
        $this->titleIdFeature = Esp2ConceptUtility::getTitleIdFeature();
//        print_r($this);die;
//        return true;
    }
    
    public function getConcepts () {
//        ini_set("display_errors", "on");error_reporting(E_ALL);
        if(isset($_SESSION['espresso2']['user']) && $_SESSION['espresso2']['user']['company_class'] != 'GESTORE') {
            $sql_hide_standby = " HAVING pubblica=1";
        } else {
            $sql_hide_standby = "";
        }
        db_set_active('espresso2');
        $query = "SELECT 
                phtt.id_taxonomy_term,
                p.id_product,
                p.erp_code,
                pc.code as prod_class,
                #phfv.id_feature_category,
                #fc.code as feat_cat,
                #fvl.id_feature,
                #f.code as feature,
                fvl.string_value as name,
                p.active = 1,
                isProductStandby(p.id_product) as pubblica	
                FROM 
                product_has_taxonomy_term as phtt
                INNER JOIN product as p ON p.id_product = phtt.id_product
                join product_class pc on(p.id_product_class=pc.id_product_class)
                ## prod name
                LEFT JOIN (
                        product_has_feature_value as phfv  
                        INNER JOIN feature_value_lang as fvl ON (
                                                phfv.id_feature_value = fvl.id_feature_value
                                                AND phfv.id_feature_category = '49'
                                                AND fvl.id_feature = :id_feature 
                                                and fvl.id_lang in( 0, :id_lang )
                                                )
                        JOIN feature f on(fvl.id_feature=f.id_feature)
                        ##join feature_category fc on(phfv.id_feature_category=fc.id_feature_category)
                ) ON p.id_product = phfv.id_product

                WHERE 
                phtt.id_taxonomy_term = :id_taxonomy_term 
                AND pc.code = :prod_class
                AND p.active = 1
                $sql_hide_standby
                ORDER BY phtt.prog ASC, phtt.id
            ";
//        echo $query."<br>";
//        echo $this->idLang.PHP_EOL.$this->prodClass.PHP_EOL.$this->titleIdFeature.PHP_EOL.$this->idTaxTerm."<br>";
        $result = db_query(
                $query, array(
                ':id_lang' => $this->idLang,
                ':prod_class' => $this->prodClass,
                ':id_feature' => $this->titleIdFeature,
                ':id_taxonomy_term' => $this->idTaxTerm,
//                ':id_resource_group' => $this->idResGroup,
//                ':id_feature_category' => $this->idFeatureCat,
            ), array('fetch' => PDO::FETCH_ASSOC)
        );
        $return = array();
//        print_r($result);die;

        if ($result) {
        /**
         ** Manually include single concept class
         */
        module_load_include('php', 'concept', 'includes/concept' );
//        print_r($result->fetchAssoc());die;
            while ($row = $result->fetchAssoc()) {

                $prod_cfg = array(
                    'id_lang'           => $this->idLang,
                    'id_taxonomy_term'  => $this->idTaxonomyTerm,
                    'id_channel'        => $this->idChannel,
                    'img_id_res_class'  => $this->imgIdResClass,
                    'short_version'  => false,
                    'resourceOrder'  => 'desc'
                );
                $p = new Esp2Concept( $row['id_product'] , $prod_cfg );
//                $p->getProduct();
                $p->prodname = $row['name'];
//                unset($p->docs, $p->features, $p->resources, $p->accessories, $p->doc_tax_tree);
                $row['product'] = $p;
                $return[$p->features['data_start']] = $row;
                
            }
        }

        db_set_active();
        //$ret2 = array_reverse(aso$return, TRUE);
	ksort($return);
//        print_r($return);die;        
        return $return;
        
    }
    
    /**
     * Product::isResourceAuthorized()
     * controlla livello di visibilita' della risorsa con il livello di privilegio dell'utente (company_class) in sessione
     * 
     * @param string $visibility
     * @return bool
     */
    private function isResourceAuthorized($visibility) {

        if (!isset($this->usrLevel)) {
            $this->calcUsrLevel();
        }
        $visibility == '' ? $visibility = 'PUBBLICO' : $visibility = $visibility;
        return $this->usrLevel >= $this->cfg['res'][$visibility];
    }

    private function calcUsrLevel() {

        $logged = $_SESSION['espresso2']['user']['isLogged'];
        if ($logged) {
//            $user = $session->get(LoginCostanController::SESSION_USER);
            $user = $_SESSION['espresso2']['user'];
            $this->usrLevel = $this->cfg['usr'][$user['company_class']]; //Utente loggato	
        } else {
            $this->usrLevel = $this->cfg['usr']['PUBBLICO']; //Utente pubblico
        }
    }

    private function setUsrLevel($usrLevel = null) {
        $this->usrLevel = isset($usrLevel) ? $usrLevel : $this->cfg['usr']['PUBBLICO'];
    }
    
    
}

?>

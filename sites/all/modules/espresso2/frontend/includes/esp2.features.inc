<?php

db_set_active('espresso2');
//define('MASTER_TAXONOMY', 'TAS_EUROCRYOR');


global $language;
$idLang = getIdLanguage ( $language->language );

$ar_feat_cat = array(
    'CAT_MERC_EUROCRYOR', 
//    'CAT_TEC_EUROCRYOR'
);
$ar_id_feat_cat = array();


/** 
 * ritorna gli Id delle feature category categ merceologiche e caratteristiche tecniche
 */
foreach( $ar_feat_cat as $feat_cat_code) {
    $id = getIdFeatureCat($feat_cat_code);
    $ar_id_feat_cat[] = $id;
}

//print_r($ar_id_feat_cat);die;
$arFeat = getFeatureList($ar_id_feat_cat , $idLang);


function getIdFeatureCat( $feat_cat_code ) {
    $query = "select id_feature_category from feature_category where code=:feat_cat_code";
    $result = db_query($query, array( ':feat_cat_code' => $feat_cat_code ), array( 'fetch' => PDO::FETCH_ASSOC )  );
    $row = $result->fetchAssoc();
    return $row['id_feature_category'];
}

function getFeatureList( $ar_id_feat_cat , $id_lang) {
    db_set_active('espresso2');
    $query = "select
        f.id_feature, f.code as feat, fl.name as feature_name, fc.id_feature_category, fc.code as fcat,
        fvl.id_lang, fvl.string_value as feature_string, fvl.text_value as feature_text 
        from 
        feature f 
        join feature_lang fl on(f.id_feature=fl.id_feature and fl.id_lang=$id_lang)
        join feature_category_has_feature fchf on( f.id_feature=fchf.id_feature)
        join feature_category fc on(fchf.id_feature_category=fc.id_feature_category)
        left join feature_value_lang fvl on(f.id_feature=fvl.id_feature and fvl.id_lang=$id_lang)
        #left join  on()
        where
        fc.id_feature_category in (". implode(',', $ar_id_feat_cat) .")";
    $result = db_query($query, array( ':tax_code' => MASTER_TAXONOMY, ':id_lang' => $id_lang ), array( 'fetch' => PDO::FETCH_ASSOC )  );
    $return =  array();
    if ($result) {
          while ($row = $result->fetchAssoc()) {
//              print_r($row);
              $row['media'] = Esp2Utility::getFeatureResource( $row['id_feature'], $row['feat'], $id_lang);
              $return[] = $row;
          }
      }
//    echo"<br>";print_r($return);die;
    db_set_active();
    return $return;
    
}

//function getFeatureResource( $idFeature, $featCode, $idLang ) {
//    
//    $idResGrupIcon  = Esp2Utility::getIconIdResgroup();
//    $idCatalog      = Esp2Utility::getIdCatalogue();
//    $idChannel      = Esp2Utility::getIdChannel(); 
//    $idFeatCatIcon  = Esp2Utility::getIconIdFeatcat(); 
//    
//    db_set_active('espresso2');
//    $query ="SELECT 
//            r.id_resource,r.code, r.localized, rc.store_dam, rl.id_lang, 
//            m.id_media, m.id_channel,m.erp_code,m.uri,m.uri_thumb,m.mime_type,m.file_name,m.keywords
//            FROM resource AS r 
//            INNER JOIN catalog_has_resource AS chr ON r.id_resource = chr.id_resource AND chr.id_catalog= :id_catalog
//            INNER JOIN resource_class AS rc ON r.id_resource_class = rc.id_resource_class
//            INNER JOIN resource_group_channel AS rgc ON r.id_resource = rgc.id_resource
//            INNER JOIN resource_lang AS rl ON r.id_resource= rl.id_resource AND (rl.id_lang=:id_lang OR rl.id_lang=0)
//            INNER JOIN media AS m ON rl.id_media = m.id_media AND m.id_channel = :id_channel
//
//            INNER JOIN resource_has_feature_value AS rhfv ON r.id_resource = rhfv.id_resource
//            INNER JOIN feature_value_lang AS fvl ON (
//                rhfv.id_feature_value = fvl.id_feature_value 
//                AND rhfv.id_feature_category = :id_feature_category 
//## ==> id_feature NON VA USATA X JOIN : 
//##      feature_code della feature deve essere uguale al feature_value della feature associata alla risorsa
//##      FEATURE: Id:3, Code:MERC_CARNE --> feature risorsa: Id:112,Code:RIS_COD_FEATURE,StringValue:MERC_CARNE
//    #AND fvl.id_feature = :id_feature ##id_feature DOESN'T MATCH !!
//                AND fvl.id_lang in(:id_lang,0)
//                )
//            WHERE 
//            fvl.string_value = :feature_code
//            AND rgc.id_resource_group = :id_resource_group
//            AND rgc.id_channel = :id_channel
//            LIMIT 1";
//    $result = db_query($query, array( 
//        ':id_resource_group' => $idResGrupIcon, 
//        ':id_feature_category' => $idFeatCatIcon, 
//        ':id_channel' => $idChannel, 
//        ':feature_code' => $featCode, 
//        ':id_feature' => $idFeature, 
//        ':id_catalog' => $idCatalog, 
//        ':id_lang' => $idLang 
//         ), 
//         array( 'fetch' => PDO::FETCH_ASSOC )  
//    );
//    $return =  array();
//    if ($result) {
//        $return = $result->fetchAssoc();
//    } 
////    echo"<br>FeatureResource:";
////    echo $query."<br>";
////    echo "idResGrupIcon:$idResGrupIcon | ";
////    echo "idFeatCatIcon:$idFeatCatIcon | ";
////    echo "idChannel:$idChannel | ";
////    echo "featCode:$featCode | ";
////    echo "idFeature:$idFeature | ";
////    echo "idCatalog:$idCatalog | ";
////    echo "idLang:$idLang<br>";
////    print_r($return);die;
//    db_set_active();
//    return $return;
//    
//}

db_set_active('default');
    
?>
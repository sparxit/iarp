<?php
    
db_set_active('espresso2');
//define('MASTER_TAXONOMY', 'TAS_EUROCRYOR');
    
/**
 * function to retrieve the level 0 taxonomy terms
 * direct query to espresso2 db for now, should call a web service or an api exposed in a Symfony frontend
 * 
 */
function getTaxonomyList ( $idLang ) {

    $query = "select
        tt.*, ttl.name,ttl.description, ttl.id_lang
        from taxonomy t join taxonomy_term tt on(t.id_taxonomy=tt.id_taxonomy) 
        join taxonomy_term_lang ttl on(tt.id_taxonomy_term=ttl.id_taxonomy_term and ttl.id_lang=:id_lang)
        where t.erp_code=:tax_code
        and tt.id_parent is null
        and tt.active=1
        and tt.allow_product=1
        order by tt.prog
        ";
    
    $result = db_query($query, array( ':tax_code' => MASTER_TAXONOMY, ':id_lang' => $idLang ), array( 'fetch' => PDO::FETCH_ASSOC )  );
    $return =  array();
    if ($result) {
          while ($row = $result->fetchAssoc()) {
//              print_r($row);
              $return[] = $row;
          }
      }
    return $return;
    
}

/**
 * fetches the Espresso2 language id given a language iso code
 * 
 * @param string $lang_code
 */
function getIdLanguage ( $lang_code ) {
    $query =" Select id_lang from lang where iso_code=:langCode";
	$sel= db_select('lang', 'id_lang')
		->fields('id_lang')
	;
        $sel->condition("iso_code",$lang_code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
                
//    $result = db_query($query, array( ':langCode' => $lang_code), array( 'fetch' => PDO::FETCH_ASSOC )  );
    
    return $row['id_lang'];
}



    global $language;
    $idLang = getIdLanguage ( $language->language );
    $arTax = getTaxonomyList($idLang);
    
    
db_set_active('default');
    
?>
<link rel="stylesheet" href="/<?php echo drupal_get_path('module', 'frontend'); ?>/css/frontend.css">
<script type="text/javascript" src="/<?php echo drupal_get_path('module', 'frontend'); ?>/js/frontend.js"></script>

<div id="prod-menu-container">
    <div id="prod-menu-header">
        <div class="container">
            <ul>
                <li class="active"><a id="taxonomies" href="#"><?php echo t('BY CATEGORY'); ?></a></li>
                <li><a id="features" href="#"><?php echo t('BY NEED'); ?></a></li>
            </ul>
        </div>
    </div>
    <div id="prod-menu-content">
        <div class="" id="box-taxonomies">
            <div class="container">
            <ul id="esp2-taxonomies">
                <?php foreach ($aTax as $cnt => $tax ):?>
                <li>
                    <?php $url = 'products/tax/'.$tax['id_taxonomy_term']; ?>
                    <a href="<?php echo url($url); ?>" id="<?php echo $tax['id_taxonomy_term'];?>">
                        <img src="/<?php echo drupal_get_path('theme', 'goodnex_sub'); ?>/images/<?php echo $aTaxImages[$tax['erp_code']] ?>">
                        <span><?php echo $tax['name'];?></span>
                    </a>
                </li>
                <?php endforeach; ?>
            </ul>
            </div>
        </div>
        <div class="hidden" id="box-features">
            <div class="container">
            <div class="" id="">
                <ul>
                <?php foreach ($aFeat as $cnt => $feature ):?>
                <li>
                    <a href="#" id="<?php echo $feature['id_feature'];?>">
                        <?php if(count($feature['media'])>1): ?>
                            <img src="<?php echo $exportWebPath.$feature['media']['file_name']; ?>"> 
                        <?php else: ?>
                            <img src="/sites/all/themes/goodnex_sub/images/espresso2/icons/icon_not_found.png"> <!-- SAMPLE IMAGE !!!! -->
                        <?php endif; ?>
                        <span><?php echo $feature['feature_name'];?></span>
                    </a>
                </li>
                <?php endforeach; ?>
                </ul>
            </div>
                <div class="submit-div" style="">
                    <span class="submit-btn" style="" ><a href="#"><?php print t('view result'); ?></a></span>
                </div>
            </div>
        </div>
    </div>

</div>


sel_features = new Array();

function saveFeature( ft ) {
    var exist=jQuery.inArray(ft, sel_features);
    if(exist>=0) {
        sel_features.splice(exist, 1);
    } else {
        sel_features.push(ft);
    }
}


(function ($, win, Modernizr, nav, doc) {

    $(document).ready(function(){
        $('#prod-menu-header a').click(function(e){
            e.preventDefault();
            
            $(this).parent().parent().find('li').removeClass('active');
            $(this).parent().addClass('active');
            
            var id='box-' + $(this).attr('id');
            $('#prod-menu-content > div').addClass('hidden');
            $('#prod-menu-content > div#'+id).removeClass('hidden');
            
        });
        /** Features multiple choice  **/
        $('#box-features li > a').click(function(e){
            e.preventDefault();
            $(this).toggleClass('active');
            saveFeature( $(this).attr('id') );
        });
        /** Selected features serializing (json) for GET submit  **/
        $('.submit-div a').click(function(e){
            e.preventDefault();
            var json_string = JSON.stringify( sel_features );
            window.location.href = '/products/feat/'+encodeURIComponent(json_string);
        });
    });
    
}(jQuery, window, Modernizr, navigator, document));
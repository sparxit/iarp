<link rel="stylesheet" href="/<?php echo drupal_get_path('module', 'register'); ?>/css/register.css">
<div id="esp2-register">
    
    <form accept-charset="UTF-8" id="esp2-register-form" method="post" action="/register" >
        <div>
            <div class="form-item form-type-textfield form-item-name">
                <label for="edit-name">
                    <?php echo t('Nome'); ?> <span title="" class="form-required" >*</span>
                </label>
                <input type="text" class="form-text required" maxlength="255" size="60" value="" name="nome" id="edit-name">
            </div>
            
            <div class="form-item form-type-textfield form-item-name">
                <label for="edit-lastname">
                    <?php echo t('Cognome'); ?> <span title="" class="form-required" >*</span>
                </label>
                <input type="text" class="form-text required" maxlength="255" size="60" value="" name="cognome" id="edit-lastname">
            </div>
            
            <div class="form-item form-type-textfield form-item-name">
                <label for="edit-societa">
                    <?php echo t('Societa'); ?> <span title="" class="form-required" >*</span>
                </label>
                <input type="text" class="form-text required" maxlength="255" size="60" value="" name="societa" id="edit-societa">
            </div>
            
            <div class="form-item form-type-textfield form-item-mail">
                <label for="edit-email">
                    <?php echo t('Email'); ?> <span title="" class="form-required">*</span>
                </label>
                <input type="email" class="form-text required" required="" maxlength="255" size="60" value="" name="email" id="edit-email">
            </div>
            
            <div class="form-item form-type-select form-item-nazione">
                <label for="edit-regione">
                    <?php echo t('Regione'); ?> <span title="" class="form-required">*</span>
                </label>
                <select name="regione" id="edit-regione" required="">
                    <option value=""></option>
                    <?php foreach($aRegioni as $regione): ?>
                    <option value="<?php echo $regione; ?>"><?php echo $regione; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-item form-type-textarea form-item-message">
              <label for="edit-motivo">
                  <?php echo t('motivo della richiesta'); ?><span title="" class="form-required">*</span>
              </label>
             <div class="form-textarea-wrapper resizable textarea-processed resizable-textarea">
                 <textarea class="form-textarea required" rows="5" cols="60" name="motivo" id="edit-motivo"></textarea>
             </div>
            </div>

            <div id="edit-actions" class="form-actions form-wrapper">
                <input type="submit" class="button default form-submit" value="Register" name="op" id="edit-submit">
            </div>
        </div>
        <div class="contact-form-responce" style="">
                
            <?php if($esito!==false): ?>
                <?php if($esito->esito): ?>
                    <p class="esito ok"><?php echo t('Registration successfull'); ?> !</p>
                <?php else: ?>
                    <p class="esito ko"><?php echo t('Registration error'); ?></p>
                <?php endif; ?>
            <?php endif; ?>
                
            </div>
    </form>
    
</div>
<?php

/**
 * Class for Espresso2 general utility and constants
 * 
 */
class Esp2Register {

    const REGISTER_URL = "http://espresso2.localhost/web/app_dev.php/frontendeurocryor/serviceregistered";
//    const REGISTER_URL = "http://192.168.103.12/espresso2/web/app_dev.php/frontendeurocryor/serviceregistered";
//    const REGISTER_URL = "http://192.168.1.182/espresso2/web/app_dev.php/frontendeurocryor/serviceregistered";    
    
    public static function getElencoRegioni() {

        $arRegioni = array();
        $arRegioni["Abruzzo"] = "Abruzzo";
        $arRegioni["Basilicata"] = "Basilicata";
        $arRegioni["Calabria"] = "Calabria";
        $arRegioni["Campania"] = "Campania";
        $arRegioni["Emilia-Romagna"] = "Emilia-Romagna";
        $arRegioni["Friuli-Venezia"] = "Giulia Friuli-Venezia Giulia";
        $arRegioni["Lazio"] = "Lazio";
        $arRegioni["Liguria"] = "Liguria";
        $arRegioni["Lombardia"] = "Lombardia";
        $arRegioni["Marche"] = "Marche";
        $arRegioni["Molise"] = "Molise";
        $arRegioni["Piemonte"] = "Piemonte";
        $arRegioni["Puglia"] = "Puglia";
        $arRegioni["Sardegna"] = "Sardegna";
        $arRegioni["Sicilia"] = "Sicilia";
        $arRegioni["Toscana"] = "Toscana";
        $arRegioni["Trentino-Alto"] = "Adige Trentino-Alto Adige";
        $arRegioni["Umbria"] = "Umbria";
        $arRegioni["Valle d'Aosta"] = "Valle d'Aosta";
        $arRegioni["Veneto"] = "Veneto";
        return $arRegioni;
    }
    
    public static function register ( $data ) {

        $url = self::REGISTER_URL;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;

    }

}

?>

<?php

/**
 * Class for Espresso2 general utility and constants
 * 
 */
class Esp2Utility {
    
    const CHANNEL_CODE              ='web';
    const CATALOGUE_CODE            ='EUROCRYOR';
    const RESCLASS_MAIN_IMG         ='CL_RIS_IMMAGINI_PRINCIPALE';
    const MAIN_TAXONOMY             = 'TAS_EUROCRYOR';
    const DOC_TAXONOMY              = 'DOC';
    const PRODNAME_FEATURE          ='NOME';
    const DOCNAME_FEATURE           ='DOC_TITLE';
    const ACCESSORYNAME_FEATURE     ='NOME_ACC';
    const ICON_RESGROUP             ='icon';
    const SECTIONS_RESGROUP         ='Sezione';
    const SECTIONS_CAPTION_FEAT     ='CAPTION_LANG';
    const SECTIONS_CAPTION_FEATCAT  ='CAT_CAPTION';
    const ICON_FEATCAT              ='CAT_RIS_ICONE';
    const PRODUCT_RELATION_CLASS    ='Prodotto_Accessorio'; //for accessories
    
    const DOC_RESGROUP_PUB          = 'Tecnico-PUBBLICO';
    const DOC_RES_FEATURE           = 'RIS_COD_FEATURE';
    const DOC_RES_FEATURE_CAT       = 'CAT_RIS_PDFCERT'; //??
    const DOC_FEATURE_CAT_EDMS      = 'CAT_DOCEDMS'; //FeatCat x doc EDMS (esterni)
    
    const DOC_RESCLASS_EDMS         = 'RS_DOCEDMS'; //ResourceClass x doc EDMS (esterni)
    const DOC_RESCLASS_ESP          = 'RS_DOCPDFESP'; //ResourceClass x doc Espresso (interni o Razuna (store_dam=1))
    const DOC_RESCLASS_HTML         = 'RS_DOCHTML'; //ResourceClass x doc HTML (feature text/title su espresso)
    
    const DOC_RESGROUP_EDMS         = 'edms_areadoc'; //Resource Group x doc EDMS
    const DOC_RESGROUP_AREADOC      = 'doc_areadoc'; //Resource Group x doc AREADOC
    const DOC_RESGROUP_VIDEO        = 'video_areadoc'; //Resource Group x doc video
    
    const TPL_HTML_FEATCAT          = 'CAT_DOCHTML'; //ID:32
    const TPL_HTML_FEATURE          = 'HTML_CODE'; //ID: 141
    
    
//    const EXPORTWEB_PATH = '/../ExportWeb/'; // 1 lev above docroot
//    const EXPORTWEB_PATH = '/ExportWeb/'; // inside docroot
    const EXPORTWEB_PATH = 'http://catalogue.costan.com/../ExportWeb/'; //production repository
//    const EXPORTWEB_PATH = 'http://eurocryor-check.tksoluzioni.it/ExportWeb/'; //dev repository : ExportWeb inside DocumentRoot
    
    static $values;
    /** 
     * Mappa livelli di privilegio
     * Mappatura dei valori  e' statica, andrebbe aggiunto un campo su db x mapparlo direttamente li (tab. company_class) 
     */
    static $cfg = array(
            'res' => array(
                'INTERNO' => 20,
            	'SERVICE' => 20,
                'CLIENTE' => 10,
                'PUBBLICO' => 1,
            ),
            'usr' => array(
                'PUBBLICO' => 1,
                'CLIENTE' => 10,
            	'SERVICE' => 20,
                'INTERNO' => 20,
                'GESTORE' => 30,
            )
    );
    
    /**
     * Ritorna mappa dei livelli di privilegio (risorsa e utente)
     * @return array
     */
    public static function getPrivileges(){
        return self::$cfg;
    }
    
    public static function getIdProdnameFeature($code=null){
        if($code==null) $code=self::PRODNAME_FEATURE;
        db_set_active('espresso2');
        $sel= db_select('feature', 'id_feature')
                ->fields('id_feature');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature'];
    }
    public static function getIdCatalogue($code=null) {
        if($code==null) $code=self::CATALOGUE_CODE;
        db_set_active('espresso2');
        $sel= db_select('catalog', 'id_catalog')
                ->fields('id_catalog');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_catalog'];
        
    }
    public static function getIdChannel($code=null) {
        if($code==null) $code=self::CHANNEL_CODE;
        db_set_active('espresso2');
        $sel= db_select('channel', 'id_channel')
                ->fields('id_channel');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_channel'];
        
    }
    public static function getIdLang($code) {
        db_set_active('espresso2');
        $sel= db_select('lang', 'id_lang')
                ->fields('id_lang');
        $sel->condition("iso_code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_lang'];
        
    }
    public static function getIdMainImgResclass($code=null) {
        if($code==null) $code=self::RESCLASS_MAIN_IMG;
        db_set_active('espresso2');
        $sel= db_select('resource_class', 'id_resource_class')
                ->fields('id_resource_class');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_resource_class']; 
    }
    public static function getIdProductRelationClass($code=null) {
        if($code==null) $code=self::PRODUCT_RELATION_CLASS;
        db_set_active('espresso2');
        $sel= db_select('product_relation_class', 'pr')
                ->fields('pr');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_product_relation_class']; 
    }
    public static function getIconIdResgroup($code=null) {
        if($code==null) $code=self::ICON_RESGROUP;
        db_set_active('espresso2');
        $sel= db_select('resource_group', 'id_resource_group')
                ->fields('id_resource_group');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_resource_group']; 
    }
    public static function getSectionsIdResgroup($code=null) {
        if($code==null) $code=self::SECTIONS_RESGROUP;
        db_set_active('espresso2');
        $sel= db_select('resource_group', 'id_resource_group')
                ->fields('id_resource_group');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_resource_group']; 
    }
    public static function getIconIdFeatcat($code=null) {
        if($code==null) $code=self::ICON_FEATCAT;
        db_set_active('espresso2');
        $sel= db_select('feature_category', 'id_feature_category')
                ->fields('id_feature_category');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature_category']; 
    }
    public static function getCaptionIdFeatcat($code=null) {
        if($code==null) $code=self::SECTIONS_CAPTION_FEATCAT;
        db_set_active('espresso2');
        $sel= db_select('feature_category', 'id_feature_category')
                ->fields('id_feature_category');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature_category']; 
    }
    public static function getCaptionIdFeat($code=null) {
        if($code==null) $code=self::SECTIONS_CAPTION_FEAT;
        db_set_active('espresso2');
        $sel= db_select('feature', 'f')
                ->fields('f');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature']; 
    }
    
    public static function getIdTaxonomy ( $code=null ) {
        if($code==null) $code=self::MAIN_TAXONOMY;
        db_set_active('espresso2');
        $sel= db_select('taxonomy', 't')
                ->fields('t');
        $sel->condition("erp_code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_taxonomy']; 
    }
    public static function getIdDocTaxonomy ( $code=null ) {
        if($code==null) $code=self::DOC_TAXONOMY;
        $id_taxonomy = self::getIdTaxonomy();
        db_set_active('espresso2');
        $sel= db_select('taxonomy_term', 't')
                ->fields('t');
        $sel->condition("erp_code",$code,"=");
        $sel->condition("id_taxonomy",$id_taxonomy,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_taxonomy_term']; 
    }

    public static function getTaxonomyTerm($id_tax_term, $id_lang){

        db_set_active('espresso2');
        $query = "Select * from taxonomy_term_lang where id_taxonomy_term=:id_taxonomy_term and id_lang=:id_lang";
        $result = db_query(
                $query, 
                array( 
                        ':id_lang' => $id_lang, 
                        ':id_taxonomy_term'=>  $id_tax_term,
                ), 
                array( 'fetch' => PDO::FETCH_ASSOC )  
        );
        $row = $result->fetchAssoc();
        self::$values = new stdClass();
        
        self::$values->id_taxonomy_term  = $row['id_taxonomy_term'];
        self::$values->name              = $row['name'];
        self::$values->description       = $row['description'];
        db_set_active();
        return self::$values;
    }
    
    public static function getFeatureById($idFeature) {
        db_set_active('espresso2');
        $sel= db_select('feature', 'f')
                ->fields('f');
        $sel->condition("id_feature",$idFeature,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row; 
    }
    
    public static function getFeatureByCode($codeFeature) {
        db_set_active('espresso2');
        $sel= db_select('feature', 'f')
                ->fields('f');
        $sel->condition("code",$codeFeature,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row; 
    }
    
    public static function getFeatureCategoryById($idFeatureCat) {
        db_set_active('espresso2');
        $sel= db_select('feature_category', 'f')
                ->fields('f');
        $sel->condition("id_feature_category",$idFeatureCat,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row; 
    }
    
    public static function getFeatureCategoryByCode($codeFeatureCat) {
        db_set_active('espresso2');
        $sel= db_select('feature_category', 'f')
                ->fields('f');
        $sel->condition("code",$codeFeatureCat,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row; 
    }
    
    public static function getFeatureResource( $idFeature, $featCode, $idLang ) {

        $idResGrupIcon  = self::getIconIdResgroup();
        $idCatalog      = self::getIdCatalogue();
        $idChannel      = self::getIdChannel(); 
        $idFeatCatIcon  = self::getIconIdFeatcat(); 

        db_set_active('espresso2');
        $query ="SELECT 
                r.id_resource,r.code, r.localized, rc.store_dam, rl.id_lang, 
                m.id_media, m.id_channel,m.erp_code,m.uri,m.uri_thumb,m.mime_type,m.file_name,m.keywords
                FROM resource AS r 
                INNER JOIN catalog_has_resource AS chr ON r.id_resource = chr.id_resource AND chr.id_catalog= :id_catalog
                INNER JOIN resource_class AS rc ON r.id_resource_class = rc.id_resource_class
                INNER JOIN resource_group_channel AS rgc ON r.id_resource = rgc.id_resource
                INNER JOIN resource_lang AS rl ON r.id_resource= rl.id_resource AND (rl.id_lang=:id_lang OR rl.id_lang=0)
                INNER JOIN media AS m ON rl.id_media = m.id_media AND m.id_channel = :id_channel

                INNER JOIN resource_has_feature_value AS rhfv ON r.id_resource = rhfv.id_resource
                INNER JOIN feature_value_lang AS fvl ON (
                    rhfv.id_feature_value = fvl.id_feature_value 
                    AND rhfv.id_feature_category = :id_feature_category 
    ## ==> id_feature NON VA USATA X JOIN : 
    ##      feature_code della feature deve essere uguale al feature_value della feature associata alla risorsa
    ##      FEATURE: Id:3, Code:MERC_CARNE --> feature risorsa: Id:112,Code:RIS_COD_FEATURE,StringValue:MERC_CARNE
        #AND fvl.id_feature = :id_feature ##id_feature DOESN'T MATCH !!
                    AND fvl.id_lang in(:id_lang,0)
                    )
                WHERE 
                fvl.string_value = :feature_code
                AND rgc.id_resource_group = :id_resource_group
                AND rgc.id_channel = :id_channel
                LIMIT 1";
        $result = db_query($query, array( 
            ':id_resource_group' => $idResGrupIcon, 
            ':id_feature_category' => $idFeatCatIcon, 
            ':id_channel' => $idChannel, 
            ':feature_code' => $featCode, 
            ':id_feature' => $idFeature, 
            ':id_catalog' => $idCatalog, 
            ':id_lang' => $idLang 
             ), 
             array( 'fetch' => PDO::FETCH_ASSOC )  
        );
        $return =  array();
        if ($result) {
            $return = $result->fetchAssoc();
        } 
        db_set_active();
        return $return;

    }
    
    
    /**
     * Gets full, recursive taxonomy (sub) tree
     */
    public static function getTaxonomyTree( $idParent, $level=0,$taxonomyToExclude=null) {
        global $language;
        $idTaxonomy     = Esp2Utility::getIdTaxonomy();
        $idLang         = Esp2Utility::getIdLang($language->language);
        
//        $idParent == null ? $sql_parent = " and t.id_parent IS NULL" : $sql_parent=$idParent;
		if($taxonomyToExclude != null)
                    $exclude_erp = " and t.erp_code != '$taxonomyToExclude' ";
		else
		    $exclude_erp = "";

        db_set_active('espresso2');
        $query = "SELECT ta.erp_code AS erp_code_tax, ttl.name, t.id_taxonomy_term, t.id_taxonomy, t.id_parent, t.erp_code AS erp_code_term, 
            t.level_depth, t.active, t.prog
            FROM taxonomy_term t
            LEFT JOIN taxonomy_term_lang as ttl on ttl.id_taxonomy_term = t.id_taxonomy_term
            LEFT JOIN taxonomy ta on ta.id_taxonomy=t.id_taxonomy
            WHERE t.id_taxonomy = :id_taxonomy AND t.id_parent = :id_parent AND (ttl.id_lang = :id_lang OR ttl.id_lang IS NULL) AND t.active = 1
            ".$exclude_erp."
            ORDER BY t.prog";
        $result = db_query($query, array( 
            ':id_parent' => $idParent, 
//            ':id_doc_taxonomy' => $idDocTaxonomy, 
            ':id_taxonomy' => $idTaxonomy, 
            ':id_lang' => $idLang 
             ), 
             array( 'fetch' => PDO::FETCH_ASSOC )  
        );

        $termFirst =  array();
        if ($result) {
              while ($row = $result->fetchAssoc()) {
                  $termFirst[] = $row;
              }
        } else {
            return false;
        }
        foreach($termFirst as $cnt => $arTax ) {
            $tax_tree[$arTax['id_taxonomy_term']]['name']           = $arTax['name'];
            $tax_tree[$arTax['id_taxonomy_term']]['id']             = $arTax['id_taxonomy_term'];
            $tax_tree[$arTax['id_taxonomy_term']]['erp_code_term']  = $arTax['erp_code_term'];
            $tax_tree[$arTax['id_taxonomy_term']]['children']       = self::getTaxonomyTree($arTax['id_taxonomy_term']);
        }
        db_set_active();
        return $tax_tree;
    }
    
    public static function pc_array_power_set($array) {
        // initialize by adding the empty set
        $results = array(array( ));

        foreach ($array as $element)
            foreach ($results as $combination)
                array_push($results, array_merge(array($element), $combination));

        return $results;
    }

    public static function getResourceById($idResource) {
        db_set_active('espresso2');
        $sel= db_select('resource', 'f')
                ->fields('f');
        $sel->condition("id_feature",$idResource,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row; 
    }
    
    public static function getResourceByCode($codeResource) {
        db_set_active('espresso2');
        $sel= db_select('resource', 'f')
                ->fields('f');
        $sel->condition("code",$codeResource,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();
        return $row; 
    }
    /**
     * Retrieves all features associated with a given resource 
     * optional filters by feature category or feature code
     * 
     * @param int $idResource
     * @param string $featureCat optional
     * @param string $featureCode optional
     * @return array
     */
    public static function getResourceFeatures ( $idResource, $featureCat =null , $featureCode=null ) {
        
        global $language;
        $idLang = Esp2Utility::getIdLang($language->language);
        if($featureCat!=null)
                $sql_feat_cat = "and fc.code = '$featureCat'";
        if($featureCode!=null)
                $sql_feat_code = "and f.code = '$featureCode'";
        $query = "select
                rhfv.id_resource, f.id_feature, f.code, fvl.id_feature_value, fvl.text_value, fvl.string_value, fc.code as feat_cat
                from 
                resource_has_feature_value rhfv
                join feature_value_lang fvl on(rhfv.id_feature_value=fvl.id_feature_value and fvl.id_lang in(0,:id_lang) )
                join feature f on(fvl.id_feature=f.id_feature)
                left join 
                ( feature_category_has_feature fchf join feature_category fc on(fchf.id_feature_category=fc.id_feature_category)
                ) on(f.id_feature=fchf.id_feature)

                where
                rhfv.id_resource=:id_resource
                $sql_feat_cat
                $sql_feat_code
                ";
        
        db_set_active('espresso2');
        $result = db_query($query, array( 
            ':id_lang' => $idLang, 
            ':id_resource' => $idResource 
             ), 
             array( 'fetch' => PDO::FETCH_ASSOC )  
        );
        db_set_active();
        if ($result) {
              while ($row = $result->fetchAssoc()) {
                  $aFeatures[] = $row;
              }
        } else {
            return false;
        }
        return $aFeatures;
    }
}

?>

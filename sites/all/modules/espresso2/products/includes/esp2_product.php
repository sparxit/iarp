<?php

/**
 * Class for single Espresso2 product
 * 
 */
class Esp2Product {
//    const P_TITLE_FEATURE = 'DOC_TITLE'; //Feature: titolo prodotto

    const P_DESC_FEATURE = 'DESCRIZIONE'; //feature: descrizione prodotto
    const P_RESCLASS_MAIN_IMG = 'CL_RIS_IMMAGINI_PRINCIPALE'; //resource class dell'immagine principale
    const P_RESCLASS_MAIN_IMG_THUMB = 'CL_RIS_THUMB_400'; //resource class dell'immagine principale in formato thumb 400
    const P_RESCLASS_PDF_SHEET = 'CL_RIS_SCHEDAPDF'; //resoource class della scheda pdf
    const P_PRODNAME_FEATURE = 'NOME'; //feature nome prodotto

    public $prodIsAccessory; // FALSE=prodotto normale, TRUE= prodotto accessorio (sottoprodotto)
    public $prodIsShort; // FALSE=prodotto completo, TRUE= prodotto con solo nome e mainImg
    public $idProduct;
    public $idCatalogProduct;
    public $idLang;
    public $idChannelDefault = 2;
    public $idChannel;
    public $idTaxonomyTerm;
    public $idCatalogue;
    public $ImgIdResClass;
    public $idSectionsResgroup;
    public $idCaptionFeature;
    public $idCaptionFeatureCat;
    public $prodname;
    public $mainImg;
    public $pdfSheet;
    public $usrLevel;
    public $cfg;
    public $features;
    public $resources;
    public $accessories;
    public $docs;
    public $doc_tax_tree;
    public $resCount = array(); //array conteggio risorse (img, video, etc)
    public $html_tpl;

    public function __construct($id_product, $ar_cfg = array()) {

        if (!$this->checkIsProduct($id_product))
            return false;
        if ($ar_cfg['accessory'])
            $this->prodIsAccessory = true;
        if ($ar_cfg['short_version'])
            $this->prodIsShort = true;
        $this->cfg = Esp2Utility::getPrivileges(); //mappa livelli privilegi utente
        $this->idProduct = $id_product;
        $this->idTaxonomyTerm = $ar_cfg['id_taxonomy_term'];
        $this->idLang = $ar_cfg['id_lang'];
        $ar_cfg['id_channel'] == '' ? $this->idChannel = $this->idChannelDefault : $this->idChannel = $ar_cfg['id_channel'];
        $this->idCatalogue = $ar_cfg['id_catalogue'];
        $this->ImgIdResClass = $ar_cfg['img_id_res_class'];
        $this->idSectionsResgroup = Esp2Utility::getSectionsIdResgroup();
        $this->idCaptionFeature = Esp2Utility::getCaptionIdFeat();
        $this->idCaptionFeatureCat = Esp2Utility::getCaptionIdFeatcat();
        $this->getProduct( $this->prodIsShort );

//        print_r($this);die;
        return;
    }

    private function checkIsProduct($idProduct) {

        db_set_active('espresso2');
        $sel = db_select('product', 'p')
                ->fields('p');
        $sel->condition("id_product", $idProduct, "=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();
        if (!$row)
            return false;
        else
            return true;
    }

    /**
     * Retrieves all product details (features, resources, etc ) and stores them.
     * 
     * @return object Esp2Product
     */
    public function getProduct( $short_version ) {

//        print_r(func_get_args());die; 
        if($short_version) { 
            /** 
             * CHECK : nuova classe risorse per immagini thumbnail
             */
            $this->resources = $this->getResources( null, $this->idCaptionFeatureCat, $this->idCaptionFeature ); //img principale in formato thumb
            //print_r($this->resources);die;
            $this->setMainImg();
//            $this->setMainImg();
            return; //short version of product for product list
        }

        $this->resources = $this->getResources( null, $this->idCaptionFeatureCat, $this->idCaptionFeature); // CHIAMATA ATTIVA, con anche CAPTION x sezioni
//        $this->resources    = $this->getResources();
        
        $this->features = $this->getFeatures();
        $this->setProdName();
        
        $this->accessories = $this->getAccessories();
        if (!$this->prodIsAccessory)
            $this->doc_tax_tree = Esp2Utility::getTaxonomyTree(Esp2Utility::getIdDocTaxonomy());
        $this->docs = $this->getDocuments();
//        $this->setProdName();
        $this->setMainImg();
        $this->setPdfSheet();
        return;
    }

    /**
     * loops through product features to retrieve the P_PRODNAME_FEATURE feature
     */
    private function setProdName() {
        if (!is_array($this->features) || count($this->features) <= 0) {
            return false;
        }
            
        if ($this->prodIsAccessory) {
            $name_feature = Esp2Utility::ACCESSORYNAME_FEATURE; //codice feature x il nome di un accessorio
        } else {
            $name_feature = Esp2Utility::PRODNAME_FEATURE; //codice feature x il nome prodotto standard
        }
        foreach ($this->features as $cnt => $aFeature) {
            if ($aFeature['feature_code'] == $name_feature) {
                $this->prodname = $aFeature['value_string'];
                return true;
            }
            
        }
        return false;
    }

    /**
     * loops through product resources to retrieve the P_RESCLASS_MAIN_IMG or the P_RESCLASS_MAIN_IMG_THUMB resource
     */
    private function setMainImg( $img_class=null ) {
//        print_r(func_get_args());die;
        if (!is_array($this->resources) || count($this->resources) <= 0) {
//            echo "nessuna risorsa esistente. skip";
            return false;
        }
        $img_class==null ? $img_class=self::P_RESCLASS_MAIN_IMG : $img_class=self::P_RESCLASS_MAIN_IMG_THUMB;
        foreach ($this->resources as $cnt => $aResource) {
            
            if ($aResource['resource_class'] == $img_class ) {
                $this->mainImg = $aResource['file_name'];
                return true;
            }
        }

        return false;
    }
    /**
     * loops through product resources to retrieve the CL_RIS_SCHEDAPDF resource class resource
     */
    private function setPdfSheet() {
        if (!is_array($this->resources) || count($this->resources) <= 0) {
            return false;
        }
        foreach ($this->resources as $cnt => $aResource) {
            if ($aResource['resource_class'] == self::P_RESCLASS_PDF_SHEET) {
                $this->pdfSheet = $aResource['file_name'];
                return true;
            }
        }
        return false;
    }

    /**
     * Finds current product sub-products (accessories)
     * 
     * @param int $idSubProd
     * @return array
     */
    private function getAccessories() {
        /**
         * prod_relation_class = 3 // prodotto -> accessorio
         */
        $idProdRelClass = Esp2Utility::getIdProductRelationClass();
        db_set_active('espresso2');
        $query = "Select pr.* from product_relation pr join product p on(pr.id_product=p.id_product) 
            where p.active=1 and pr.id_main_product=" . $this->idProduct . " and pr.id_product_relation_class=" . $idProdRelClass;
        $result = db_query(
                $query, array(), array('fetch' => PDO::FETCH_ASSOC)
        );
        $return = array();
        if ($result) {
            $prod_cfg = array(
                'id_lang' => $this->idLang,
                'id_taxonomy_term' => $this->idTaxonomyTerm,
                'id_channel' => $this->idChannel,
                'id_catalogue' => $this->idCatalogue,
                'img_id_res_class' => $this->ImgIdResClass,
                'accessory' => true,
            );
            while ($row = $result->fetchAssoc()) {
                $subprod = new Esp2Product($row['id_product'], $prod_cfg);
                $return[] = $subprod;
            }
        }
        db_set_active();
        return $return;
    }

    /**
     * Retrieves document taxonomy and look for associated document (to product AND taxonomy)
     */
    public function getDocuments() {

        if (!isset($this->doc_tax_tree) || !is_array($this->doc_tax_tree) || count($this->doc_tax_tree) <= 0)
            return false;
//        print_r($this->doc_tax_tree); die;
        foreach ($this->doc_tax_tree as $k0 => $aTax0) {

            $docs = $this->getDocsByTaxonomy($aTax0['id']);
            if (count($docs) > 0)
                $this->doc_tax_tree[$k0]['docs'] = $docs;

            if (count($aTax0['children']) > 0) {
                foreach ($aTax0['children'] as $k1 => $aTax1) {
                    $subdocs = $this->getDocsByTaxonomy($aTax1['id']);
                    if (count($subdocs) > 0)
                        $this->doc_tax_tree[$k0]['children'][$k1]['docs'] = $subdocs;
                    else
                        unset($this->doc_tax_tree[$k0]['children'][$k1]);
                }
            }
            /** Check if there are children tax still, otherwise unset * */
            if (count($this->doc_tax_tree[$k0]['children']) == 0)
                unset($this->doc_tax_tree[$k0]);
        }
//        print_r($this->doc_tax_tree);die;
        return;
    }

    /**
     * Finds products docs by taxonomy
     * @global object $language
     * @return array
     */
    private function getDocsByTaxonomy($idTaxonomyTerm) {
        global $language;
        $idChannel = Esp2Utility::getIdChannel();
        $idLang = Esp2Utility::getIdLang($language->language);

        db_set_active('espresso2');
        $query = "select p.id_product, p.erp_code, tt.id_taxonomy_term,
tt.erp_code as taxonomy_erp_code,
ttl.name as taxonomy_name,
tt.allow_product,tt.active, 
p.id_product, p.erp_code as prod_code, 
r.id_resource, r.code as resource_code, 
getResourceVisibility(r.id_resource) as res_visibility, 
isResourceStandby(r.id_resource) as res_pubblicata,
rc.code as resource_class_code,rc.store_dam as store_dam, 
rg.code as resource_group_code,
m.*
from
product p 
JOIN
(   
    product_has_resource phr 
    JOIN resource r on(phr.id_resource=r.id_resource) 
    JOIN resource_class rc on(r.id_resource_class=rc.id_resource_class)
) on (p.id_product=phr.id_product)

 JOIN 
( 
	taxonomy_term tt
	LEFT JOIN taxonomy_term_lang as ttl ON tt.id_taxonomy_term= ttl.id_taxonomy_term and (ttl.id_lang  = :id_lang or ttl.id_lang is null)
	JOIN taxonomy_term_has_resource tthr on(tt.id_taxonomy_term=tthr.id_taxonomy_term)
	 
) on( r.id_resource=tthr.id_resource)
 JOIN 
(   resource_group_channel rgc join resource_group rg on(rgc.id_resource_group=rg.id_resource_group) 
    ) on(r.id_resource=rgc.id_resource and rgc.id_channel=:id_channel)
 JOIN
(
	resource_lang rl JOIN media m on(rl.id_media=m.id_media)
) on(r.id_resource=rl.id_resource and m.id_channel=:id_channel)

where 1 
and rl.id_lang in (0,:id_lang)
and tt.id_taxonomy_term = :id_taxonomy_term
and p.id_product=:id_product
#and rc.code in ( 'RS_DOCEDMS', 'RS_DOCPDFESP', 'RS_DOCHTML')
order by p.id_product";
        $result = db_query(
                $query, array(
            ':id_channel' => $idChannel,
            ':id_lang' => $idLang,
            ':id_taxonomy_term' => $idTaxonomyTerm,
            ':id_product' => $this->idProduct,
                ), array('fetch' => PDO::FETCH_ASSOC)
        );

        $return = array();
        if ($result) {
            while ($row = $result->fetchAssoc()) {
                /**
                 * CONTROLLO VISIBILITA' RISORSA <--> LIVELLO UTENTE
                 */
                if (!$this->isResourceAuthorized($row['visibility'])) {
                    //risorsa NON visibile
                    unset($row); 
                    /** DEBUG **/
//                    echo "Risorsa NON AUTORIZZATA:" . $row['code'] . "<br>";
//                    $res_feat = $this->getFeaturesResourceLang($row['id_resource'], Esp2Utility::DOCNAME_FEATURE);
//                    $row['doc_title'] = $res_feat[0]['string_value'];
//                    $return[] = $row;
                } else {
                    $res_feat = $this->getFeaturesResourceLang($row['id_resource'], Esp2Utility::DOCNAME_FEATURE);
                    $row['doc_title'] = $res_feat[0]['string_value'];
                    $return[] = $row;
                }
                

            }
        }
        db_set_active();
        return $return;
    }

    /* Restituisce le features delle risorse
     * OPZIONALE: Filtro per lingua e codice feature
     */

    private function getFeaturesResourceLang($idResource, $featureCode = '') {
//        print_r(func_get_args());
        $where_clause = ' AND ((fvl.id_lang = :id_lang) OR (fvl.id_lang = 0)) ';
        if ($featureCode != '') {
            $where_clause .= ' AND f.code = :feature_code ';
        }
        $where_clause .= ' AND fvl.cancelled = 0 ';
        db_set_active('espresso2');
        $query = 'SELECT f.code as code,fvl.*
                            FROM resource_has_feature_value rhfv
                            INNER JOIN feature_value_lang fvl ON fvl.id_feature_value = rhfv.id_feature_value
                            INNER JOIN feature as f ON f.id_feature = fvl.id_feature
                            WHERE rhfv.id_resource = :id_resource 
                            ' . $where_clause . '
                            ORDER BY fvl.prog';
//        echo $query;die;
        $result = db_query(
                $query, array(
            ':id_lang' => $this->idLang,
            ':id_resource' => $idResource,
            ':feature_code' => $featureCode,
                ), array('fetch' => PDO::FETCH_ASSOC)
        );
        $return = array();
        if ($result) {
            while ($row = $result->fetchAssoc()) {
                $return[] = $row;
            }
        }
        db_set_active();
//        print_r($return);die;
        return $return;
    }

    /**
     * Funzione per restituire tutte le risorse di un prodotto, filtrate x tassonomia di appartenenza (es. DOC)
     */
    /*
      private function getDocTaxonomyResource( $id_taxonomy_term, $id_product ) {

      $idLang = $this->getRequest()->getSession()->get(LoginCostanController::SESSION_ID_LANG_FRONTEND);
      $this->calcUsrLevel();
      $resources = $this->getDoctrine()->getRepository('Espresso2PimBundle:Product')->getResourceByTaxonomy ( $id_product, $id_taxonomy_term, $this->usrLevel, $idLang );
      $ciclo=0;
      if(count($resources)>0) {
      foreach($resources as $cnt => $aRes) {

      if( !$this->isResourceAuthorized($aRes['res_visibility']) ) {
      $resources[$cnt]['isAuthorized'] = 'NO';
      unset($resources[$cnt]);
      continue;
      }
      $aFeatures = $this->getDoctrine()->getRepository('Espresso2PimBundle:Resource')->
      getFeaturesResourceLang($aRes['id_resource'], $idLang, null);
      foreach($aFeatures as $kk => $aFeature) {
      $resources[$cnt]['features'][$aFeature['code']] = $aFeature;
      }
      $ciclo++;
      }
      }
      return $resources;
      }
     */

    /**
     * Restituisce array di tutte i valori delle features associate al prodotto, raggruppate per id feature e codice feature
     * 
     * @return array 
     */
    public function getFeatures() {

        global $language;
        db_set_active('espresso2');
        $query = "SELECT feature_value_lang_1.id_feature_value as id_feat_value, feature_value_lang_1.id_feature, 
                fv.name as feature_name,
                feature_value_lang_1.string_value as value_string, 
                feature_value_lang_1.text_value as value_text, 
                feature_value_lang_1.boolean_value as value_boolean, 
                feature_value_lang_1.integer_value as value_integer, 
                feature_value_lang_1.decimal_value as value_decimal, 
                f.code as feature_code, f.id_feature_type, 
                phfv.id_feature_category as id_feature_category,
                fc.code as feature_category,
                fcl.name as feature_category_name,
                feature_value_lang_1.id_lang 
                FROM product_has_feature_value as phfv  
                INNER JOIN feature_category fc on (phfv.id_feature_category=fc.id_feature_category)
                left JOIN feature_category_lang fcl on(fc.id_feature_category=fcl.id_feature_category and (fcl.id_lang=:id_lang OR fcl.id_lang=0))
                INNER JOIN feature_value_lang as feature_value_lang_1 ON phfv.id_feature_value = feature_value_lang_1.id_feature_value
                INNER JOIN feature f on(feature_value_lang_1.id_feature=f.id_feature)
                and feature_value_lang_1.id_lang in(0, :id_lang)
                left join feature_lang fv on(f.id_feature=fv.id_feature and (fv.id_lang=:id_lang or fv.id_lang=0) )
                WHERE 1 
                and phfv.id_product = :id_product
                and feature_value_lang_1.cancelled=0
                #and feature_value_lang_1.boolean_value=1
                order by feature_value_lang_1.prog
                ";

        $result = db_query(
                $query, array(
            ':id_lang' => $this->idLang,
            ':id_product' => $this->idProduct,
                ), array('fetch' => PDO::FETCH_ASSOC)
        );

        $return = array();
        if ($result) {
            while ($row = $result->fetchAssoc()) {
//                  print_r($row);echo "<br>";
                $row['media'] = Esp2Utility::getFeatureResource($row['id_feature'], $row['feature_code'], Esp2Utility::getIdLang($language->language));
                $return[] = $row;
            }
        }
        db_set_active();
        return $return;
    }

    /**
     * 
     * 
     * @param type $idResourceGroup
     * @param type $idFeatureCategory
     * @param type $feature
     * @return type
     */
    public function getResources($idResourceGroup = null, $idFeatureCategory = null, $idFeature = null, $codeResourceClass=null) {
//        print_r(func_get_args());
        
        db_set_active('espresso2');
        $idProduct = $this->idProduct;
        $idLang = $this->idLang;
        $idChannel = $this->idChannel;
        if ($idResourceGroup != null) {
            $sql_resgroup = 'AND rgc.id_resource_group = ' . $idResourceGroup;
        } else {
            $sql_resgroup = '';
        }
        
        $sql_resgclass = '';
        if (isset($codeResourceClass)){
                $sql_resgclass = " AND rc.code = '$codeResourceClass'";
        }
        
        if($_SESSION['espresso2']['user']['company_class']!='GESTORE')
//            $sql_hide_standby = " AND isResourceStandby(r.id_resource)=1 ";
            $sql_hide_standby = "  ";
        
        if (isset($idFeature) && isset($idFeatureCategory)) {

            $sel = ",fvl.string_value as caption";
            $join = 'LEFT JOIN ( resource_has_feature_value AS rhfv
                         INNER JOIN feature_value_lang AS fvl ON rhfv.id_feature_value = fvl.id_feature_value 
        	    AND rhfv.id_feature_category = :id_feature_cat 
                    AND fvl.id_feature = :id_feature 
                    AND fvl.id_lang in( 0, :id_lang)
        	    )  ON r.id_resource = rhfv.id_resource';
        } else {
            $sel = "";
            $join = "";
        }

        
        $query = 'SELECT 
                isResourceStandby(r.id_resource) as isResStandBy,
                r.id_resource, r.code, r.localized, rc.store_dam, rc.id_resource_class,rc.code as resource_class,
                rl.id_lang, 
                getResourceVisibility(r.id_resource) as visibility,
                m.id_media, m.id_channel, m.erp_code, m.uri, m.uri_thumb, m.mime_type, m.file_name, m.keywords
                ' . $sel . '			 
                FROM resource AS r 
                INNER JOIN product_has_resource AS phr ON r.id_resource= phr.id_resource
                INNER JOIN resource_class AS rc ON r.id_resource_class = rc.id_resource_class
                LEFT JOIN resource_group_channel AS rgc ON (r.id_resource = rgc.id_resource AND rgc.id_channel = :id_channel ' . $sql_resgroup . ')
                INNER JOIN resource_lang AS rl ON (r.id_resource= rl.id_resource AND (rl.id_lang=:id_lang OR rl.id_lang=0) )
                INNER JOIN media AS m ON rl.id_media = m.id_media AND m.id_channel = :id_channel			
                ' . $join . '					
                WHERE 
                phr.id_product = :id_product 
                '.$sql_hide_standby.' 
                '.$sql_resgclass.'
                ORDER BY phr.prog';
        
                //#OR phr.id_product IN (select product_relation.id_main_product from product_relation where product_relation.id_product = :id_product)
                
//        echo "RISORSE:".$query;
//        echo "<br>".$this->idChannel."-".$this->idProduct."-".$this->idLang."-".$idFeature."-".$idFeatureCategory;
//        echo "<hr>";
//        die;

        $result = db_query(
                $query, array(
                    ':id_lang' => $this->idLang,
                    ':id_product' => $this->idProduct,
                    ':id_channel' => $this->idChannel,
                    ':id_feature' => $idFeature,
                    ':id_feature_cat' => $idFeatureCategory,
                ), array('fetch' => PDO::FETCH_ASSOC)
        );

        $return = array();

        if ($result) {
            while ($row = $result->fetchAssoc()) {
//                echo "ciclo risorse<<br>";
                /**
                 * CONTROLLO VISIBILITA' RISORSA <--> LIVELLO UTENTE
                 */
                if (!$this->isResourceAuthorized($row['visibility'])) {
                    //risorsa NON visibile
                    unset($row);
//                    $return[] = $row; //DEBUG
                } else {
                    //conteggio tipo risorsa
                    $this->resCount[$row['resource_class']][$row['mime_type']] ++;
                    $row ['res_url'] = $this->getResUrl($row);
                    $return[] = $row;
                }
            }
        }  else{
//            echo "nessuna risorsa";
        }
//          print_r($return);die;
        db_set_active();
//        echo"<br>";print_r($return);
//        die;
        return $return;
    }

    /**
     * Product::isResourceAuthorized()
     * controlla livello di visibilita' della risorsa con il livello di privilegio dell'utente (company_class) in sessione
     * 
     * @param string $visibility
     * @return bool
     */
    private function isResourceAuthorized($visibility) {

        if (!isset($this->usrLevel)) {
            $this->calcUsrLevel();
        }
        $visibility == '' ? $visibility = 'PUBBLICO' : $visibility = $visibility;
        $res_level = $this->cfg['res'][$visibility];
        return $this->usrLevel >= $res_level;
    }

    private function calcUsrLevel() {

        $logged = $_SESSION['espresso2']['user']['isLogged'];
        if ($logged) {
//            $user = $session->get(LoginCostanController::SESSION_USER);
            $user = $_SESSION['espresso2']['user'];
            $this->usrLevel = $this->cfg['usr'][$user['company_class']]; //Utente loggato	
        } else {
            $this->usrLevel = $this->cfg['usr']['PUBBLICO']; //Utente pubblico
        }
    }

    private function setUsrLevel($usrLevel = null) {
        $this->usrLevel = isset($usrLevel) ? $usrLevel : $this->cfg['usr']['PUBBLICO'];
    }

    /**
     * Retrieves resource url
     * NOT IMPLEMENTED
     *      * 
     * @return string
     */
    private function getResUrl() {

        $url = '';
        return $url;
    }

    /**
     * Retrieves Html Template (HTML_CODE) associated to the product and its corrensponding feature values (Title, Text1, text2, id_vimeo)
     * 
     * 1. resource with resclass=RS_DOCHTML --> resource feature (TEMPLATE_CODE) value == resource code --> feature = HTML_CODE
     * 2. resource feature (TEMPLATE_CODE) value
     * 3. resource code = feature value
     * 4. resource feature = HTML_CODE
     * 
     * @param type $idResource
     * @param type $idMedia
     * @param type $storeDam
     */
    private function templatePreviewAction($idResource, $idMedia, $storeDam) {
        
        $feat_cat = Esp2Utility::getFeatureCategoryByCode(Esp2Utility::TPL_HTML_FEATCAT);
        $template = Esp2Utility::getFeatureByCode(Esp2Utility::TPL_HTML_FEATURE);
        $this->html_tpl = $template;
        return true;
    }
    
}

?>

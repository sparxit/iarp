<?php
/**
 * Class for single Espresso2 TaxonomyTerm
 * 
 */
class Esp2Taxonomy {

    public $id_taxonomy_term;
    public $name;
    public $description;
    
    public function __construct($id_tax_term, $id_lang){

        db_set_active('espresso2');
        $query = "Select * from taxonomy_term_lang where id_taxonomy_term=:id_taxonomy_term and id_lang=:id_lang";
        $result = db_query(
                $query, 
                array( 
                        ':id_lang' => $id_lang, 
                        ':id_taxonomy_term'=>  $id_tax_term,
                ), 
                array( 'fetch' => PDO::FETCH_ASSOC )  
        );
        $row = $result->fetchAssoc();
        $this->id_taxonomy_term  = $row['id_taxonomy_term'];
        $this->name              = $row['name'];
        $this->description       = $row['description'];
        db_set_active();
        return $this;
    }
}

?>

<?php

/**
 * Class for retrieving Espresso2 products
 * 
 */
class Esp2Products {

    const CATALOGUE_CODE = 'EUROCRYOR';
    const PRODNAME_FEATURE = 'NOME';

    public $idLang;
    public $idCatalogue;
    public $idChannel = 2;
    public $idNameFeature;
    public $idTaxonomyTerm;
    public $taxonomyTerm;
    public $imgIdResClass;

    public function __construct($langCode) {
//        $this->idCatalogue      = $this->getIdCatalogue(self::CATALOGUE_CODE);
        $this->idCatalogue = Esp2Utility::getIdCatalogue(self::CATALOGUE_CODE);
//        $this->idNameFeature    = $this->getIdProdnameFeature(self::PRODNAME_FEATURE);
//        module_load_include('php', 'products', 'includes/esp2_utility');
        $this->idNameFeature = Esp2Utility::getIdProdnameFeature(self::PRODNAME_FEATURE);
//        $this->idLang           = $this->getIdLang($langCode);
        $this->idLang = Esp2Utility::getIdLang($langCode);
        return;
    }

    public function getProductList($filter_type, $filter_value) {

        switch ($filter_type) {
            case "tax":
                $this->idTaxonomyTerm = $filter_value;
                $this->taxonomyTerm = Esp2Utility::getTaxonomyTerm($this->idTaxonomyTerm, $this->idLang)->name;
                $arProds = $this->getProductsByTaxonomy($filter_value);
                break;
            case "feat":
                $arProds = $this->getProductsByFeatures($filter_value);
                break;
        }

        /**
         * * Manually include product class
         */
//        module_load_include('php', 'products', 'includes/esp2_product' );

        return $arProds;
    }

    /**
     * Ricerca prodotti per tassonomia:
     * 1. id tax term
     */
    public function getProductsByTaxonomy($idTaxonomyTerm) {
//        die(__FUNCTION__);

        if ($_SESSION['espresso2']['user']['company_class'] != 'GESTORE')
            $sql_hide_standby = "HAVING pubblica=1";
        else
            $sql_hide_standby = "";

        $query = "SELECT 
                cphtt.id_taxonomy_term,
                cp.id_catalog_product,
                p.id_product,
                p.erp_code,
                feature_value_lang.string_value as name
                ,isProductStandby(p.id_product) as pubblica	
                FROM 
                catalog_product_has_taxonomy_term as cphtt
                INNER JOIN catalog_product as cp ON cphtt.id_catalog_product = cp.id_catalog_product
                INNER JOIN product as p ON p.id_product = cp.id_product
                ## prod name
                LEFT JOIN (product_has_feature_value as product_has_feature_value  
                INNER JOIN feature_value_lang as feature_value_lang ON 
                product_has_feature_value.id_feature_value = feature_value_lang.id_feature_value
                AND product_has_feature_value.id_feature_category = '39'
                AND feature_value_lang.id_feature = :id_feature 
                and feature_value_lang.id_lang in( 0, :id_lang )
                ) ON p.id_product = product_has_feature_value.id_product

                WHERE 
                cphtt.id_taxonomy_term = :id_tax_term 
                AND cp.id_main_product is null
                AND cp.id_catalog = :id_catalog
                AND p.active = 1
                $sql_hide_standby
                ORDER BY cphtt.prog ASC, cphtt.id";
//        echo "Query:".$query;
//        echo "<hr>".$this->idCatalogue."-".$this->idNameFeature;
//        die;
        db_set_active('espresso2');
//die($query."<br>:id_tax_term".$idTaxonomyTerm."| :id_lang ".$this->idLang."| :id_catalog ".$this->idCatalogue."| :id_feature ".$this->idNameFeature);
        $result = db_query(
                $query, array(':id_tax_term' => $idTaxonomyTerm,
                    ':id_lang' => $this->idLang,
                    ':id_catalog' => $this->idCatalogue,
                    ':id_feature' => $this->idNameFeature
                ), array('fetch' => PDO::FETCH_ASSOC)
        );

        $return = array();
        if ($result) {
            while ($row = $result->fetchAssoc()) {

                $prod_cfg = array(
                    'id_lang' => $this->idLang,
                    'id_taxonomy_term' => $this->idTaxonomyTerm,
                    'id_channel' => $this->idChannel,
                    'id_catalogue' => $this->idCatalogue,
                    'img_id_res_class' => $this->imgIdResClass,
                    'short_version' => true,
                );

                //var_dump($row['name']);
                $row['taxonomy_term'] = $this->taxonomyTerm;
                $p = new Esp2Product($row['id_product'], $prod_cfg);
                $p->getProduct(true);
                $p->prodname = $row['name'];
//                echo "<br>";print_r($p);die;
                unset($p->docs, $p->features, $p->resources, $p->accessories, $p->doc_tax_tree);
                $row['product'] = $p;

                $return[] = $row;
            }
        }
        db_set_active();
        return $return;
    }

    /**
     * Ricerca prodotti per array di features:
     * 1. array features
     */
    public function getProductsByFeatures($arFeatures, $operator = 'OR', $order_by = 'match') {

        $combinations = Esp2Utility::pc_array_power_set($arFeatures); //tutte le combinazioni possibili delle feature selezionate
//                print_r($combinations);
//                die;

        $having = '';
        $str_features = '';
        //SE c'è almeno una feature selezionata,  aggiungi filtro
        $feat_cnt = count($arFeatures);
        if (is_array($arFeatures) && count($arFeatures) > 0) {
            /**
             * AGGIUNGERE: filtro x boolean_value=1 x le features
             */
            switch ($operator) {
                case 'AND': //products must satisfy ALL filters
                    $having = " HAVING num_matches=:feat_count ";
                    $str_features = " and f.id_feature in(" . implode(',', $arFeatures) . ") ";
                    break;
                default:
                case 'OR': //products must satisfy AT LEAST ONE filter

                    $having = "";
                    $str_features = " and (";
                    foreach ($arFeatures as $feat) {
                        $str_features .= " f.id_feature=$feat OR";
                    }
                    $str_features = substr($str_features, 0, strlen($str_features) - 3);
                    $str_features .= " )";
                    break;
            }
        }
        //ORDINAMENTO
        switch ($order_by) {
            case "match":
                $sql_order = "ORDER BY relevance desc, t.prog";
                break;
            default:
            case "prod":
                $sql_order = "ORDER BY erp_code asc";
                break;
        }
        db_set_active('espresso2');
        /**
         * TO DO: aggiungere filtro x feature STAND-BY (creare stored function)
         */
        $query = "SELECT :feat_count as num_filters, count(f.id_feature) as num_matches, round((count(f.id_feature)/:feat_count)*100,0 ) as relevance, 
            group_concat(f.id_feature SEPARATOR ';') as str_features,
        fl.name as feature_name, fl.id_lang, f.id_feature, f.id_feature_type as id_feature_type,f.code as feature_code,f.localized_name,fvl.id_feature_value,pfv.id_feature_category,p.id_product as id_product, p.id_product_class as id_product_class, p.erp_code as erp_code, c.code as catalog_name, t.id_taxonomy_term as id_taxonomy_term, t.erp_code as tax_erp_code, tl.name as tax_name
        
         ,feature_value_lang_name.string_value as name
                FROM (
                	feature_lang fl 
                	JOIN feature f on(fl.id_feature=f.id_feature) 
                	JOIN feature_value_lang fvl on(f.id_feature=fvl.id_feature)
                )
                JOIN
                (
                	product_has_feature_value pfv 
                	JOIN product p on(p.id_product=pfv.id_product) 
                	JOIN catalog_product cp on(p.id_product=cp.id_product) 
                	JOIN catalog c on(cp.id_catalog=c.id_catalog)
                ) on(fvl.id_feature_value=pfv.id_feature_value and fvl.boolean_value=1)
                JOIN
                (
                	product_has_taxonomy_term ptt 
                	JOIN taxonomy_term t on(ptt.id_taxonomy_term=t.id_taxonomy_term) 
                	JOIN taxonomy_term_lang tl on(t.id_taxonomy_term=tl.id_taxonomy_term and tl.id_lang=:id_lang)
                ) on(p.id_product=ptt.id_product)
                
                
                
                
                
 				## prod name
                LEFT JOIN (product_has_feature_value as product_has_feature_value  
                INNER JOIN feature_value_lang as feature_value_lang_name ON 
                product_has_feature_value.id_feature_value = feature_value_lang_name.id_feature_value
                AND product_has_feature_value.id_feature_category = '39'
                AND feature_value_lang_name.id_feature = :id_feature 
                and feature_value_lang_name.id_lang in( 0, :id_lang )
                ) ON p.id_product = product_has_feature_value.id_product
                
                
                WHERE
                (fl.id_lang=:id_lang OR fl.id_lang=0) and cp.id_catalog= :id_catalog 
                $str_features  
                and p.active = 1
                GROUP BY p.id_product 
                $having
                $sql_order
                ";


        //  var_dump($query);
        $result = db_query(
                $query, array(
            ':feat_count' => $feat_cnt,
            ':id_lang' => $this->idLang,
            ':id_catalog' => $this->idCatalogue,
            ':id_feature' => $this->idNameFeature
                ), array('fetch' => PDO::FETCH_ASSOC)
        );
        $return = array();
        if ($result) {
            while ($row = $result->fetchAssoc()) {
                /** instantiates prouct class * */
                $prod_cfg = array(
                    'id_lang' => $this->idLang,
                    'id_taxonomy_term' => $this->idTaxonomyTerm,
                    'id_channel' => $this->idChannel,
                    'id_catalogue' => $this->idCatalogue,
                    'img_id_res_class' => $this->imgIdResClass,
                    'short_version' => true,
                );
                $row['taxonomy_term'] = $this->taxonomyTerm;
                $p = new Esp2Product($row['id_product'], $prod_cfg);
                $p->getProduct(true);
                $p->prodname = $row['name'];
                unset($p->docs, $p->features, $p->resources, $p->accessories, $p->doc_tax_tree);
                $row['product'] = $p;

                /**
                 * check current product against the array of all combinations of selected features 
                 * group by relevance desc and by combination of features 
                 * * */
                $found_features = explode(';', $row['str_features']);
                foreach ($combinations as $combi) {

                    $str_combi = implode(';', $combi);
                    //if current combination ==  features found for the current product --> save product in $return
                    if (!array_diff($found_features, $combi) && !array_diff($combi, $found_features)) {
                        $return[$str_combi][] = $row;
                    }
                }
            }
        }
//          print_r($return);  die;
        db_set_active();
        return $return;
    }

}

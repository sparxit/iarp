<div id="tax_title_box"><div class="tax_title"><?php echo t('Cross Sections'); ?></div></div>

<div id="sections-container" class="p-container">
    <ul>
    <?php foreach ($product->resources as $cnt => $aFeature): ?>
        <?php if($aFeature['resource_class']=='CL_RIS_IMMAGINI_SEZIONI'): ?>
        <li>
            <div class="caption_box"><?php echo $aFeature['caption'];?></div>
            <a rel="prod-sections" class="bwWrapper single-image plus-icon preloader" href="<?php echo $exportweb_path.$aFeature['file_name']; ?>" title="<?php echo $aFeature['caption'];?>">
                    <img src="<?php echo $exportweb_path."thumb_sezioni/".$aFeature['file_name']; ?>">
                </a>
        </li>
        <?php endif; ?>
    <?php endforeach; ?>
    </ul>
</div>
<div id="tax_title_box"><div class="tax_title"><?php echo t('Technical details'); ?></div></div>
<div class="p-container">
    <table>
        <thead>
            <th colspan="2"><?php echo $product->prodname ?></th>
        </thead>
        <tbody>
    <?php foreach ($product->features as $cnt => $aFeature): ?>
        <?php if($aFeature['feature_category']=='CAT_TAB_TEC_EUROCRYOR'): ?>
        <tr>
            <td class="left"><?php echo $aFeature['feature_name'];?></td>
            <td class="right"><?php echo $aFeature['value_string'];?></td>
        </tr>
        <?php endif; ?>
    <?php endforeach; ?>            
        </tbody>
    </table>

</div>

<div id="tax_title_box"><div class="tax_title"><?php echo t('Accessories'); ?></div></div>

<div id="accessories-container" class="">
<?php if(count($product->accessories)>0): ?>
    <ul>
    <?php foreach ($product->accessories as $cnt => $prodObj ): ?>
        <?php $prodObj->mainImg!='' ? $hasImage=true : $hasImage=false; ?>
        <li>
            <div class="acc_tooltip" id="<?php echo $prodObj->idProduct;?>">
                <?php if($hasImage && $hasImage!=''): ?>
                <img src="<?php echo variable_get(url_esp2_exporteweb)."/".$prodObj->mainImg; ?>">
                <?php endif; ?>
            </div>
            <div class="acc_trigger"><a href="#" class="<?php if($hasImage): ?> hasImage<?php else: ?>noImage<?php endif; ?>" rel="<?php echo $prodObj->idProduct;?>">
                <span class="acc_marker <?php if(!$hasImage): ?> empty <?php endif; ?>"></span> 
                <span class="acc_name"><?php echo $prodObj->prodname; ?></span> 
                </a>
            </div>
        </li>
    <?php endforeach; ?>
    </ul>
<?php else: ?>
    <p><?php echo t('no accessories found'); ?></p>
<?php endif; ?>
</div>
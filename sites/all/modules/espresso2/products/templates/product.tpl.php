<link rel="stylesheet" href="/<?php echo $module_path ?>/css/products.css">
<script type="text/javascript" src="/<?php echo $module_path; ?>/js/products.js"></script>
<script type="text/javascript" src="/<?php echo $theme_path; ?>/js/ddpowerzoomer.js">

/***********************************************
* Image Power Zoomer- (c) Dynamic Drive DHTML code library (www.dynamicdrive.com)
* This notice MUST stay intact for legal use
* Visit Dynamic Drive at http://www.dynamicdrive.com/ for this script and 100s more
***********************************************/

</script>
<style>
    
</style>
<div id="product_page">
    <div id="title_box">
        <span class="title_tax"><a href="/products/tax/<?php echo $product->idTaxonomyTerm; ?>"><?php echo $tax->name; ?></a></span> / 
        <span class="title_prodsku"><?php echo $product->prodname; ?></span>
    </div>
    <div class="prod_img preloader">
        <span class="prod_img_zoom"><a></a></span> <!-- CORNER: zoom -->
        <!-- fancybox -->
        <a href="<?php echo $exportweb_path.$product->mainImg; ?>" class="main_img_zoom">
            <img src="<?php echo $exportweb_path."main_image/".$product->mainImg; ?>" >
        </a>
        
    </div>
    
    <div id="features-block">
        <?php include $module_path."/templates/product_features.tpl.php"; ?>
    </div>
    
    <div id="resources-block">
        <?php include $module_path."/templates/product_resources.tpl.php"; ?>
    </div>
    
    <div id="technical-block">
        <?php include $module_path."/templates/product_technical.tpl.php"; ?>
    </div>
    
    <div id="sections-block">
        <?php include $module_path."/templates/product_sections.tpl.php"; ?>
    </div>
    
    <div id="accessories-block">
        <?php include $module_path."/templates/product_accessories.tpl.php"; ?>
    </div>
    
    
    
    
    
</div>
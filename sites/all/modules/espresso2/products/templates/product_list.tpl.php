<?php
/**
 * WARNING: esiste un array $aProducts che contiene un migliaio di informazioni di vari moduli (da Panels, a Block,Overlay, Node, Metatag, Menu, Media, etc)
 * !!!!!  NON USARE questa variabile !!!!
 */

$path_2_product = "product/";
?>
<link rel="stylesheet" href="/<?php echo $module_path ?>/css/products.css">
<script type="text/javascript" src="/<?php echo $module_path; ?>/js/products.js"></script>

<?php 
    if($filter_type=='tax'){
        $tpl = "product_list_tax.tpl.php";
    } elseif($filter_type=='feat') {
        $tpl = "product_list_feat.tpl.php";
    } else {
        
    }
    $sub_tpl = $module_path."/templates/".$tpl;
    include($sub_tpl);

?>

<?php 

$ciclo=0;
$nota = null;
foreach ($product->features as $cnt => $aFeature) {
    $ar_ff[$aFeature['feature_category']]['name']               = $aFeature['feature_category_name'];
    $ar_ff[$aFeature['feature_category']]['features'][$ciclo]   = $aFeature;
    $ciclo++;
    
    if ($aFeature['feature_category']== "CAT_DEFAULT_EUROCRYOR" && $aFeature["feature_code"]=="NOTA"){
    	$nota = $aFeature;
    }
}
/**
 * CAT_DEFAULT_EUROCRYOR (nome e nota)
 * CAT_MERC_EUROCRYOR
 * CAT_TEC_EUROCRYOR
 * CAT_TAB_TEC_EUROCRYOR
 * CAT_DEFAULT_EUROCRYOR_ACC --> accessori
 * 
 */
?>
<div id="tabs-header">
        <ul>
            
            <?php if( count($ar_ff['CAT_DEFAULT_EUROCRYOR']['features'])>0 ): ?>
            <li rel="CAT_DEFAULT_EUROCRYOR_DESC" class="active"><?php echo t('Description'); ?></li>
            <li>|</li>
            <?php endif; ?>            
            
            <?php if( count($ar_ff['CAT_MERC_EUROCRYOR']['features'])>0 ): ?>
            <li rel="CAT_MERC_EUROCRYOR"><?php echo t('Product categories'); ?></li>
            <li>|</li>
            <?php endif; ?>

            <?php if( count($ar_ff['CAT_TEC_EUROCRYOR']['features'])>0 ): ?>
            <li rel="CAT_TEC_EUROCRYOR"><?php echo t('Technical features'); ?></li>
            <li>|</li>
            <?php endif; ?>            
            
            <?php  if( isset($nota )): ?>
            <li rel="CAT_DEFAULT_EUROCRYOR_NOTE"><?php echo t('Notes'); ?></li>
            <li>|</li>
            <?php endif; ?>
            
            
            <?php if( count($product->doc_tax_tree)>0 ): ?>
            <li rel="DOCS"><?php echo t('Documents'); ?></li> <!-- DOC FEATURE CATEGORY IS MISSING -->
            <?php endif; ?>
        </ul>
    <span>
        <a target="_blank" href="<?php echo $exportweb_path.$product->pdfSheet; ?>">
            <img src="/sites/all/themes/goodnex_sub/images/PDF-copia.png"> <?php echo t('product data sheet'); ?>
        </a>
    </span>
</div>

<div id="tabs-container">
    <ul>
        <li id="CAT_DEFAULT_EUROCRYOR_DESC" class="active">
            <div>
            <?php foreach($ar_ff['CAT_DEFAULT_EUROCRYOR']['features'] as $kk => $aFeature): ?>
            <?php if($aFeature['feature_code']=="DESCRIZIONE_PRODOTTO" || $aFeature['feature_code']=="DESCRIZIONE"  )
                    echo $aFeature['value_text']; 
            ?>
            <?php endforeach; ?>
            </div>
        </li>
        
        
        <li id="CAT_MERC_EUROCRYOR">
            <ul class="cat_merc esp2-icon-container">
            <?php foreach($ar_ff['CAT_MERC_EUROCRYOR']['features'] as $kk => $aFeature): ?>
                <?php if($aFeature['value_boolean']==1): ?>
                <li id="<?php echo $aFeature['id_feature']; ?>">
                    <div>
                    <?php if(count($aFeature['media'])>1): ?>
                        <img class="esp2-icon" src="<?php echo Esp2Utility::EXPORTWEB_PATH.$aFeature['media']['file_name']; ?>" >
                    <?php else: ?>
                        <img src="/sites/all/themes/goodnex_sub/images/espresso2/icons/icon_not_found.png" >
                    <?php endif; ?>
                    <span><?php echo $aFeature['feature_name']; ?></span>
                    </div>
                </li>
                <?php endif; ?>
            <?php endforeach; ?>
            </ul>
        </li>
        <li id="CAT_TEC_EUROCRYOR" class="">
            <ul class="cat_tec esp2-icon-container">
            <?php foreach($ar_ff['CAT_TEC_EUROCRYOR']['features'] as $kk => $aFeature): ?>
                <?php if($aFeature['value_boolean']==1): ?>
                <li>
                    <div>
                    <?php if(count($aFeature['media'])>1): ?>
                        <img class="esp2-icon" src="<?php echo Esp2Utility::EXPORTWEB_PATH.$aFeature['media']['file_name']; ?>" >
                    <?php else: ?>
                        <img src="/sites/all/themes/goodnex_sub/images/espresso2/icons/icon_not_found.png" >
                    <?php endif; ?>
                    <span><?php echo $aFeature['feature_name']; ?></span>
                    </div>
                </li>
                <?php endif; ?>
            <?php endforeach; ?>
            </ul>
        </li>
        <li id="CAT_DEFAULT_EUROCRYOR_NOTE" class="note">
            <div>
            <?php if(isset($nota))
                echo $nota['value_text']; 
            ?>
            </div>
        </li>
        <li id="DOCS" class="docs">
            <div id="docs_header">
                <ul>
                <?php foreach($product->doc_tax_tree as $k0 => $aTax0): ?>
                    <li><a rel="<?php echo $k0;?>" href="#"><?php echo $aTax0['name']; ?></a></li>
                <?php endforeach; ?>
                </ul>
            </div>
            
            <div id="docs_container">
                <ul>

            <?php foreach($product->doc_tax_tree as $k0 => $aTax0): ?>

                    <li id="<?php echo $k0; ?>" class="doc-single-container">
                        <ul>

                        <?php foreach($aTax0['children'] as $k1 => $aTax1): ?>
<?php  
    $tax_count = count($aTax0['children']);
    $tax_count==0 ? $tax_count=1 : $tax_count=$tax_count;
    $col_width = round(100/$tax_count,0)-1; //% larghezza di ciascuna colonna x tassonomie doc di 1° livello
?>                
                            <li style="width:<?php echo $col_width; ?>%"> 
                                <div class="doc_tax_header_1">
                                    <?php echo $aTax1['name']; ?>
                                </div>
                                <ul class="doc_tax_container_1">
                                    <?php foreach($aTax1['docs'] as $k2 => $aDocs): ?>
                                    <li class="doc-link">
                                        <a target="_blank" href="<?php echo Esp2Utility::EXPORTWEB_PATH.$aDocs['file_name'];?>">
                                            <img src="/sites/all/themes/goodnex_sub/images/PDF-copia.png">
                                        </a>
                                        <span><?php echo $aDocs['doc_title'];?></span>
                                    </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>    
                                
                        <?php endforeach; ?>
                    </ul>
                    </li>
            <?php endforeach; ?>
                </ul>
            </div>
        </li>
    </ul>
</div>
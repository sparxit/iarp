
<?php foreach ($arProds as $str_features => $arProdSub): ?>

    <div id="tax_title_box">
        <div class="tax_title">
            <ul>
                <?php foreach(explode(';',$str_features) as $kk => $idFeat): ?>
                <?php /**  Get icon for the current combination of features **/
                    $aFeat      = Esp2Utility::getFeatureById($idFeat);
                    $feat_icon  = Esp2Utility::getFeatureResource($idFeat, $aFeat['code'] , $idLang);
                ?>
                    <li>
                        <img class="esp2-icon" src="<?php echo variable_get('url_esp2_exporteweb', NULL)."/".$feat_icon['file_name']; ?>" >
                    </li>
                <?php endforeach; ?>
            </ul>

        </div>
        <div class="tax_prod_count"><?php echo count($arProdSub)." ".t('products found'); ?></div>
    </div>


    <div id="prod_list">
        <ul>
        <?php foreach ($arProdSub as $k => $prod): ?>
            <li>

                <div class="prod_img_box preloader">
                    <a href="<?php echo url($path_2_product.$prod['id_taxonomy_term']."/".$prod['id_product']); ?>">
                        <span class="prod_img_zoom"></span> <!-- CORNER: zoom -->
                        <img src="<?php echo $exportweb_path."thumb_prodotti/".$prod['product']->mainImg; ?>"> 
                    </a>
                </div>

                <div class="prod_name_box">
                    <span class="prod_name"><?php echo $prod['product']->prodname ?> </span>
                    <span class="prod_tax"> <?php echo $prod['tax_name'] ?></span>
                    <span class="prod_relevance">
                        <?php if(isset($prod['num_matches']))
                            echo "(".round(($prod['num_matches']/$prod['num_filters']*100),0)."%)"; 
                        ?>
                    </span>
                    <!--<span class="prod_link"> » scheda prodotto</span>-->
                </div>

            </li>
            <?php endforeach; ?>
        </ul>
    </div>

<?php endforeach; ?>
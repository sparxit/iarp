                        <?php //  print_r($product->resCount);die; ?>
<div id="tax_title_box"><div class="tax_title"><?php echo t('Gallery'); ?></div></div>

<div id="gallery-container" class="p-container">
    <div id="gallery-tags">
        <!--<span class="" data-categories="*">(<?php echo t('all'); ?>)</span>-->
        <?php 
        /**
         * MIME/TYPE è sempre image/jpg anche x le risorse video (media associato è l immagine thumb): id del video è invece una feature
         */
        /*
        foreach($product->resCount['CL_RIS_IMMAGINI_GALLERY'] as $mtype => $count ) {
            if(strpos($mtype, 'image')!==false)
                    $has_images = true;
            if(strpos($mtype, 'video')!==false)
                    $has_videos = true;
        }
        foreach($product->resCount['RS_DOCHTML'] as $mtype => $count ) {
            if(strpos($mtype, 'image')!==false)
                    $has_images = true;
            if(strpos($mtype, 'video')!==false)
                    $has_videos = true;
        }
        */
        if( count($product->resCount['CL_RIS_IMMAGINI_GALLERY'])>0 ) {
            $has_images = true;
        }
        if( count($product->resCount['RS_DOCHTML'])>0 ) {
            $has_videos = true;
        }
        
        ?>
        <?php if($has_images): ?>
        <span class="image" data-categories="image"><?php echo t('foto'); ?></span>
        <?php endif; ?>
        <?php if($has_videos): ?>
        <span class="video" data-categories="video"><?php echo t('video'); ?></span>
        <?php endif; ?>
    </div>
    <ul>
    <?php foreach ($product->resources as $cnt => $aFeature): ?>
        
        <?php 
//            <!-- CHECK RISORSA IN StandBy ( isResStandBy=0 ): visibile solo x utente GESTORE -->
            if($aFeature['isResStandBy']!=1 && $_SESSION['espresso2']['user']['company_class']!= 'GESTORE' ) {
                continue;
            } 
        ?>
        <?php if($aFeature['resource_class']=='CL_RIS_IMMAGINI_GALLERY' || $aFeature['resource_class']=='RS_DOCHTML' ): ?>
            <?php
                    switch ($aFeature['resource_class']) {
                        case 'CL_RIS_IMMAGINI_GALLERY':
                            $class = "prod_img_box preloader";
                            $tag = 'image';
                            break;
                        case 'RS_DOCHTML':
                            $class = "prod_video_box preloader";
                            $tag = 'video';
                            break;
                    }
            ?>
        <li data-categories="<?php echo $tag; ?>" id="<?php echo $aFeature['id_resource']; ?>">
            <div class="<?php echo $class; ?>">
                <span class="prod_img_zoom"><a></a></span> <!-- CORNER: zoom -->
            <?php if($tag=='image'): ?>
                <a class="bwWrapper single-image plus-icon" href="<?php echo $exportweb_path.$aFeature['file_name']; ?>" rel="prod-img">        
                    <img src="<?php echo $exportweb_path."thumb_gallery/".$aFeature['file_name']; ?>">
                </a>
            <?php elseif($tag=='video'): ?>
                <a class="bwWrapper single-image video-icon" href="<?php echo url("product_video/".$product->idProduct."/".$aFeature['id_resource']); ?>" rel="prod-img">        
                    <img src="<?php echo $exportweb_path."thumb_gallery/".$aFeature['file_name']; ?>">
                </a>
            <?php endif; ?>
            </div>
        </li>
        <?php endif; ?>
    <?php endforeach; ?>
    </ul>
</div>
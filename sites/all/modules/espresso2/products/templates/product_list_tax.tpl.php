
<div id="tax_title_box">
    <div class="tax_title">
    <?php if($filter_type=='tax'): ?>
        <?php echo $tax_title; ?>
    <?php    elseif ($filter_type=='feat') : ?>
        <ul>
            <?php foreach($selected_icons as $kk => $aIcon): ?>
                <li>
                    <img class="esp2-icon" src="<?php echo variable_get('url_esp2_exporteweb', NULL)."/".$aIcon['file_name']; ?>" >
                </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    </div>
    <div class="tax_prod_count"><?php echo $num_prods." ".t('products found'); ?></div>
</div>


<div id="prod_list">
    <ul>
    <?php foreach ($arProds as $cnt => $arProd): ?>
        <li>
            
            <div class="prod_img_box preloader">
                <a href="<?php echo url($path_2_product.$arProd['id_taxonomy_term']."/".$arProd['id_product']); ?>">
                    <span class="prod_img_zoom"></span> <!-- CORNER: zoom -->
                    <img src="<?php echo $exportweb_path."thumb_prodotti/".$arProd['product']->mainImg; ?>"> 
                </a>
            </div>
            
            <div class="prod_name_box">
                <span class="prod_name"><?php echo $arProd['product']->prodname ?> </span>
                <span class="prod_tax"> <?php echo $arProd['tax_name'] ?></span>
                <span class="prod_relevance">
                    <?php if(isset($arProd['num_matches']))
                        echo "(".round(($arProd['num_matches']/$arProd['num_filters']*100),0)."%)"; 
                    ?>
                </span>
                <!--<span class="prod_link"> » scheda prodotto</span>-->
            </div>
            
        </li>
    <?php endforeach; ?>
    </ul>
</div>


(function($) {

    $(document).ready(function() {

        var prodMenu = $('#esp2prod').parent();
        prodMenu.addClass('current-menu-item');
        /** 
         * set active class for both headers and containers
         */
        $('#tabs-header li').click(function(e) {
            e.preventDefault();
            $('#tabs-header li').removeClass('active');
            $(this).addClass('active');
            var rel = $(this).attr('rel');
            $('#tabs-container > ul > li').removeClass('active');
            $('#tabs-container > ul > li#' + rel).toggleClass('active');
        });

        $(function() {
            $('#gallery-container').jcarousel();
        });

        $(function() {
            $('#sections-container').jcarousel();
        });
        
        /** Docs **/
        $('#docs_header a').click(function(e){
           e.preventDefault();
           $('#docs_header li').removeClass('active');
           $(this).parent('li').addClass('active');
           var cId = $(this).attr('rel');
           
           $('#docs_container > ul > li').each(function(index){
               if($(this).attr('id')!=cId) {
                   $(this).css('display','none');
               }
               else {
                   $(this).css('display','block');
               }
                   
           });

        });
        
        /** Gallery filter: image or video **/
        $('#gallery-tags span').click(function(e) {
            e.preventDefault();
            var type = $(this).attr('data-categories');
            var boxes = $('#gallery-container ul li');

            boxes.each(function(index) {
                if (type == '*') {
                    $(this).fadeIn(300);
                }
                else if ($(this).attr('data-categories') != type) {
                    $(this).fadeOut(300);
                } else {
                    $(this).fadeIn(300);
                }
            });
            //SCROLL TO LEFT TO FIRST ELEMENT
            $('#gallery-container').jcarousel('scroll',1); 

        });


        /** Accessories Tooltip **/
        $('#accessories-block .acc_trigger a').click(function(e) {
            e.preventDefault();
            var trigId = $(this).attr('rel'); //accessory id (rel on trigger, id on tooltip)
            var hasImage = $(this).hasClass('hasImage');
            $('#accessories-block .acc_tooltip').each(function(index) {

                var isDisplayed = $(this).css('display');
                var tipId = $(this).attr('id');
                if (trigId != tipId) {
                    /** Close any other tooltip **/
                    $(this).hide();
                } else {
                    //toggle if element has image
                    if (hasImage) {
                        $(this).slideToggle(200);
                    }
                }

            });

        });

    });


}(jQuery));
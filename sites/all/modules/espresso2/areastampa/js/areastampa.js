

(function ($, nav, doc) {

    $(document).ready(function () {

        $('.block-areastampa .year_header > a').click(function (e) {
            e.preventDefault();
            var year = $(this).attr('rel');
            var header = $(this).parent();
            var table = $('.table_container#' + year);
            table.slideToggle();
            header.toggleClass('open');
        });

        var dF = jQuery("#press_date_from");
        var dT = jQuery("#press_date_to");

        dF.datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            showOn: "button",
            buttonImage: "/sites/all/modules/espresso2/areastampa/images/calendar.png",
            buttonImageOnly: true
        });
        dT.datepicker({
            showOtherMonths: true,
            selectOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            showOn: "button",
            buttonImage: "/sites/all/modules/espresso2/areastampa/images/calendar.png",
            buttonImageOnly: true
        });
//                dF.on('change', function(){
//                        setSingleDateFilter( dF.val() , 'f');
//                });
//                dT.on('change', function(){
//                        setSingleDateFilter( dT.val() , 't');
//                });

//                jQuery('#custom-search-form input[name="keys"]').on('change', function(){
//                        aSearch[0] = jQuery(this).val();
//                        //aSearch[1] = jQuery(this).val();
//                });
        jQuery('.years_array li').on('click', 'a', function (e) {
            e.preventDefault();
            // Set Date filter with $(this) year
            var anno = jQuery(this).attr('rel');
            $('input#press_date_from').val(anno + '-01-01');
            $('input#press_date_to').val(anno + '-12-31');
            $('input#pagination_page').val(1);
            $('form#custom-search-form').submit();
        });
        

        $('#pagi_holder').keydown(function (event) {
            var keypressed = event.keyCode || event.which;
            if (keypressed === 13) {
                var pagi_no = $('input#pagi_holder').val();
                $('input#pagination_page').val(pagi_no);
                
                $('form#custom-search-form').submit();
            }
        });


    });

}(jQuery, navigator, document));


function load_pagination(page) {

    jQuery('input#pagination_page').val(page);
    jQuery('form#custom-search-form').submit();

    return false;
} 
<link href="/<?php echo $path; ?>/css/areadownload.css" type="text/css" rel="stylesheet">
<script src="/<?php echo $path; ?>/js/areadownload.js" type="text/javascript"></script>
<?php //print_r($aDocs);die; ?>

<?php foreach ($aDocs as $cat => $docs): ?>
    <div class="year_header">
        <span><?php echo $cat; ?></span> <a rel='<?php echo str_replace(' ', '_', $cat);; ?>' href="#"></a>
    </div>
    <div id="<?php echo str_replace(' ', '_', $cat); ?>" class="table_container">
        <!--
        <table class="sticky-header" style="position: fixed; top: 0px; left: 381.5px; visibility: hidden; width: 864px;">
            <thead style="display: block;">
                <tr>
                    <th class="col-file_name" id="head-file_name" style="width: 90px; display: table-cell;"><?php echo t('Pdf'); ?></th>
                    <th class="col-titolo" id="head-titolo" style="width: 600px; display: table-cell;"><?php echo t('Title'); ?></th>
                    <th class="col-input_data" id="head-input_data" style="width: 140px; display: table-cell;"><?php echo t('Date'); ?></th> 
                </tr>
            </thead>
        </table>
        -->
        <table class="sticky-enabled tableheader-processed sticky-table">
            <!--
            <thead>
                <tr>
                    <th class="col-file_name" id="head-file_name" style="width: 60px; display: table-cell;"><?php echo t('Pdf'); ?><</th>
                    <th class="col-titolo" id="head-titolo"><?php echo t('Title'); ?></th>
                    <th class="col-input_data" id="head-input_data"><?php echo t('Date'); ?></th> 
                </tr>
            </thead>
            -->
            <tbody>
                <?php foreach ($docs as $cnt => $doc): ?>
                <?php ($cnt % 2)==1 ? $line='even' : $line='odd';?>
                    <tr class="<?php echo $line; ?> ">
                        <td class="col-file_name" id="row<?php echo $cnt; ?>-file_name">
                            <a target="_blank" href="<?php echo variable_get('url_esp2_exporteweb')."/".$doc['file_name'];?>">
                                <img src="/sites/all/modules/espresso2/areadownload/images/rassegna_stampa.png">
                            </a>
                        </td>
                        <td class="col-titolo" id="row<?php echo $cnt; ?>-titolo">
                            <?php echo str_replace('_', ' ', $doc['code']); ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php endforeach; ?>
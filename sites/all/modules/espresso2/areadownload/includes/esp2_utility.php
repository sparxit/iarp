<?php

/**
 * Class for Espresso2 general utility and constants
 * 
 */

class Esp2DwnlUtility {
  
    const CHANNEL_CODE      = 'web';
    const MAIN_TAXONOMY             = 'TAS_EUROCRYOR';
    const CATALOGUE_CODE    = 'EUROCRYOR';
    const DWNLD_TAXTERM     = "contenuti_aggiuntivi";
    const DWNLD_RESGROUP    = "doc_areadownload";
    const DWNLD_FEATURE     = "CAT_AREADOWNLOAD";
    const DWNLD_FEATURE_CAT = "ATTR_AREADOWNLOAD";
    /** 
     * Mappa livelli di privilegio
     * Mappatura dei valori  e' statica, andrebbe aggiunto un campo su db x mapparlo direttamente li (tab. company_class) 
     */
    static $cfg = array(
        array( 
            'res' => array(
                'INTERNO' => 20,
                'CLIENTE' => 10,
                'PUBBLICO' => 1,
            ),
            'usr' => array(
                'PUBBLICO' => 1,
                'CLIENTE' => 10,
                'INTERNO' => 20,
                'GESTORE' => 30,
            )
        )
    );
    /**
     * Ritorna mappa dei livelli di privilegio (risorsa e utente)
     * @return array
     */
    public static function getPrivileges(){
        return self::$cfg;
    }
    
    public static function getIdDocResGroup($code=null) {
        if($code==null) $code=self::DWNLD_RESGROUP;
        db_set_active('espresso2');
        $sel= db_select('resource_group', 'id_resource_group')
                ->fields('id_resource_group');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_resource_group']; 
    }
    
    public static function getIdChannel($code=null) {
        if($code==null) $code=self::CHANNEL_CODE;
        db_set_active('espresso2');
        $sel= db_select('channel', 'id_channel')
                ->fields('id_channel');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_channel'];
        
    }
    
    public static function getIdLang($code) {
        db_set_active('espresso2');
        $sel= db_select('lang', 'id_lang')
                ->fields('id_lang');
        $sel->condition("iso_code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_lang'];
        
    }

    public static function getIdTaxonomy ( $code=null ) {
        if($code==null) $code=self::MAIN_TAXONOMY;
        db_set_active('espresso2');
        $sel= db_select('taxonomy', 't')
                ->fields('t');
        $sel->condition("erp_code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_taxonomy']; 
    }
    public static function getIdDocTaxonomy ( $code=null ) {
        if($code==null) $code=self::DWNLD_TAXTERM;
        $id_taxonomy = self::getIdTaxonomy();
        db_set_active('espresso2');
        $sel= db_select('taxonomy_term', 't')
                ->fields('t');
        $sel->condition("erp_code",$code,"=");
        $sel->condition("id_taxonomy",$id_taxonomy,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_taxonomy_term']; 
    }
    public static function getIdFeatcat($code=null) {
        if($code==null) $code=self::DWNLD_FEATURE_CAT;
        db_set_active('espresso2');
        $sel= db_select('feature_category', 'id_feature_category')
                ->fields('id_feature_category');
        $sel->condition("code",$code,"=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();    
        return $row['id_feature_category']; 
    }
}

?>

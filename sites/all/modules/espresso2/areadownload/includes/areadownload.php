<?php

class Esp2Areadownload {

    private $idLang;
    private $idChannel;
    private $idTaxTerm;
    private $idResGroup;
    private $idFeature;
    private $idFeatureCat;
    private $usrLevel;
    private $cfg;
    

    public function __construct( $langCode ) {
        $this->idLang       = Esp2DwnlUtility::getIdLang($langCode);
        $this->idChannel    = Esp2DwnlUtility::getIdChannel();
        $this->idTaxTerm    = Esp2DwnlUtility::getIdDocTaxonomy();
        $this->cfg          = Esp2DwnlUtility::getPrivileges();
        $this->idResGroup   = Esp2DwnlUtility::getIdDocResGroup();
        $this->idFeatureCat = Esp2DwnlUtility::getIdFeatcat();
//        print_r($this);die;
//        return true;
    }
    
    public function getDocs () {
        
        if(isset($_SESSION['espresso2']['user']) && $_SESSION['espresso2']['user']['company_class'] != 'GESTORE') {
            $sql_hide_standby = " HAVING pubblica=1";
        } else {
            $sql_hide_standby = "";
        }
        db_set_active('espresso2');
        $query = "SELECT rgc.`id_channel`,tthr.id_taxonomy_term, tt.erp_code as taxonomy_term, r.* ,
            m.*, DATE_FORMAT(m.date_upd, '%m/%d/%Y') as date_f,
            fvl.string_value as category, f.code as feature_code,
            getResourceVisibility(r.id_resource) as visibility,
            isResourceStandby(r.id_resource) as pubblica
            FROM  `resource_group_channel` rgc
            join resource r on(rgc.id_resource=r.id_resource)
            join taxonomy_term_has_resource tthr on(tthr.id_resource=r.id_resource)
            join taxonomy_term tt on(tthr.id_taxonomy_term=tt.id_taxonomy_term)
            join resource_lang rl on(r.id_resource=rl.id_resource)
            join media m on(rl.id_media=m.id_media and rl.id_lang in(0,:id_lang))
            left join (
                    resource_has_feature_value rhfv 
                    join feature_value_lang fvl on(rhfv.id_feature_value=fvl.id_feature_value and rhfv.id_feature_category=:id_feature_category)
                    join feature f on(fvl.id_feature=f.id_feature)
            ) on(r.id_resource=rhfv.id_resource)
            WHERE  1
            and tt.id_taxonomy_term = :id_taxonomy_term
            AND rgc.`id_resource_group` = :id_resource_group
            AND  rgc.`id_channel` = :id_channel
AND m.id_channel=rgc.id_channel
            $sql_hide_standby
            ORDER BY fvl.string_value, rhfv.prog
            ";
        $result = db_query(
                $query, array(
                ':id_channel' => $this->idChannel,
                ':id_lang' => $this->idLang,
                ':id_taxonomy_term' => $this->idTaxTerm,
                ':id_resource_group' => $this->idResGroup,
                ':id_feature_category' => $this->idFeatureCat,
            ), array('fetch' => PDO::FETCH_ASSOC)
        );
        $return = array();
//        print_r($this);
//        print_r($result);die;
        if ($result) {
            while ($row = $result->fetchAssoc()) {
                /**
                 * CONTROLLO VISIBILITA' RISORSA <--> LIVELLO UTENTE
                 */
                if (!$this->isResourceAuthorized($row['visibility'])) {
                    //risorsa NON visibile
//                    unset($row); // DEBUG: lasciare risorse visibili x ora, mancano i dati standBy e visibility
                    $return[] = $row;
                } else {
                    $return[$row['category']][] = $row;
                }
            }
        }
        
        db_set_active();
//        print_r($return);die;
        $ret2 = array_reverse($return, TRUE);
        return $ret2;
        
    }
    
    /**
     * Product::isResourceAuthorized()
     * controlla livello di visibilita' della risorsa con il livello di privilegio dell'utente (company_class) in sessione
     * 
     * @param string $visibility
     * @return bool
     */
    private function isResourceAuthorized($visibility) {

        if (!isset($this->usrLevel)) {
            $this->calcUsrLevel();
        }
        $visibility == '' ? $visibility = 'PUBBLICO' : $visibility = $visibility;
        return $this->usrLevel >= $this->cfg['res'][$visibility];
    }

    private function calcUsrLevel() {

        $logged = $_SESSION['espresso2']['user']['isLogged'];
        if ($logged) {
//            $user = $session->get(LoginCostanController::SESSION_USER);
            $user = $_SESSION['espresso2']['user'];
            $this->usrLevel = $this->cfg['usr'][$user['company_class']]; //Utente loggato	
        } else {
            $this->usrLevel = $this->cfg['usr']['PUBBLICO']; //Utente pubblico
        }
    }

    private function setUsrLevel($usrLevel = null) {
        $this->usrLevel = isset($usrLevel) ? $usrLevel : $this->cfg['usr']['PUBBLICO'];
    }
    
    
}

?>

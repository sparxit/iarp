

(function($) {

    $(document).ready(function() {

        $('.page-areadownload .year_header > a').click(function(e){
            e.preventDefault();
            var year = $(this).attr('rel');
            var header = $(this).parent();
            var table = $('.table_container#'+year);
            table.slideToggle();
            header.toggleClass('open');
        });
        
    });


}(jQuery));
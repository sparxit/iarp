<?php

/**
 * Class for single Espresso2 PersonalDoc
 * 
 */
class Esp2UserDoc {

//    public $prodIsAccessory; // FALSE=prodotto normale, TRUE= prodotto accessorio (sottoprodotto)
    public $prodIsShort; // FALSE=prodotto completo, TRUE= prodotto con solo nome e mainImg
    public $idProduct;
    public $idLang;
    public $idChannelDefault = 2;
    public $idChannel;
    public $idTaxonomyTerm;
    public $ImgIdResClass;
    public $erpCode;
    public $prodname;
    public $mainImg;
    public $dataStart;
    public $dataEnd;
    public $description;
    public $usrLevel;
    public $cfg;
    public $features;
    public $resources;
//    public $docs;
    public $doc_tax_tree;
    public $resCount = array(); //array sub conteggio risorse x tipo (img, video, etc)
//    private $resHasVideo; //bool, true if the current resource has a video link amongst the features
//    public $html_tpl;
    private $filters; //search filters
    
    
    public function __construct($id_product, $ar_cfg = array(), $filters) {
//        print_r($ar_cfg);
        if (!$this->checkIsProduct($id_product))        return false;
//        if ($ar_cfg['accessory'])                       $this->prodIsAccessory = true;
        if ($ar_cfg['short_version'])                   $this->prodIsShort = true;

        $this->cfg = Esp2UserDocUtility::getPrivileges(); //mappa livelli privilegi utente
        $this->idProduct = $id_product;
        $this->idTaxonomyTerm = $ar_cfg['id_taxonomy_term'];
        $this->idLang = $ar_cfg['id_lang'];
        $ar_cfg['id_channel'] == '' ? $this->idChannel = $this->idChannelDefault : $this->idChannel = $ar_cfg['id_channel'];
//        $this->idCatalogue = $ar_cfg['id_catalogue'];
        $this->ImgIdResClass = $ar_cfg['img_id_res_class'];
//        $this->idSectionsResgroup = Esp2UserDocUtility::getSectionsIdResgroup();
//        $this->idCaptionFeature = Esp2UserDocUtility::getCaptionIdFeat();
//        $this->idCaptionFeatureCat = Esp2UserDocUtility::getCaptionIdFeatcat();
        
        $this->getProduct( $filters );
//        print_r($this);die;

        return;
    }

    /**
     * Checks if product exists
     * 
     * @param integer $idProduct
     * @return boolean
     */
    private function checkIsProduct($idProduct) {

        db_set_active('espresso2');
        $sel = db_select('product', 'p')
                ->fields('p');
        $sel->condition("id_product", $idProduct, "=");
        $result = $sel->execute();
        $row = $result->fetchAssoc();
        db_set_active();
        if (!$row)
            return false;
        else
            return true;
    }

    /**
     * Retrieves all product details (features, resources, etc ) and stores them.
     * 
     * @param $filters array of query filters (title, date start, date end)
     * 
     * @return object Esp2Product
     */
    public function getProduct( $filters ) {
//        echo __CLASS__."::".__FUNCTION__." idProd:".$this->idProduct.PHP_EOL;
        if(!empty($filters))        $this->filters=$filters;
        
        if($this->prodIsShort) { 
            $this->resources = $this->getProductResources( null, $this->idCaptionFeatureCat, $this->idCaptionFeature,Esp2UserDocUtility::P_RESCLASS_MAIN_IMG);
            $this->setMainImg();
            return; //short version of product for product list
        }

        $this->features = $this->getProductFeatures();

//        $this->resources = $this->getProductResources( null, $this->idCaptionFeatureCat, $this->idCaptionFeature); // CHIAMATA ATTIVA, con anche CAPTION x sezioni
        $this->resources    = $this->getProductResources(); //tutte le risorse senza filtri
        return;
    }

    /**
     * loops through product features to retrieve the P_PRODNAME_FEATURE feature
     */
//    private function setProdName() {
//        if (!is_array($this->features) || count($this->features) <= 0) {
//            return false;
//        }
//            
//        if ($this->prodIsAccessory) {
//            $name_feature = Esp2UserDocUtility::ACCESSORYNAME_FEATURE; //codice feature x il nome di un accessorio
//        } else {
//            $name_feature = Esp2UserDocUtility::P_PRODNAME_FEATURE; //codice feature x il nome prodotto standard
//        }
//        foreach ($this->features as $cnt => $aFeature) {
//            if ($aFeature['feature_code'] == $name_feature) {
//                $this->prodname = $aFeature['value_string'];
//                return true;
//            }
//            
//        }
//        return false;
//    }

    /**
     * loops through product resources to retrieve the P_RESCLASS_MAIN_IMG resource
     */
//    private function setMainImg() {
//        if (!is_array($this->resources) || count($this->resources) <= 0) {
//            return false;
//        }
//        foreach ($this->resources as $cnt => $aResource) {
//            if ($aResource['resource_class'] == Esp2UserDocUtility::P_RESCLASS_MAIN_IMG) {
//                $this->mainImg = $aResource['file_name'];
//                return true;
//            }
//        }
//        return false;
//    }
    
    
    /**
     * loops through product resources to retrieve the CL_RIS_SCHEDAPDF resource class resource
     */
//    private function setPdfSheet() {
//        if (!is_array($this->resources) || count($this->resources) <= 0) {
//            return false;
//        }
//        foreach ($this->resources as $cnt => $aResource) {
//            if ($aResource['resource_class'] == Esp2UserDocUtility::P_RESCLASS_PDF_SHEET) {
//                $this->pdfSheet = $aResource['file_name'];
//                return true;
//            }
//        }
//        return false;
//    }

 

    /* Restituisce le features delle risorse
     * OPZIONALE: Filtro per lingua e codice feature
     */
    private function getFeaturesResourceLang($idResource, $featureCode = '') {
//        print_r(func_get_args());die;
        $where_clause = ' AND ((fvl.id_lang = :id_lang) OR (fvl.id_lang = 0)) ';
        if ($featureCode != '') {
            $where_clause .= ' AND f.code = :feature_code ';
        }
        $where_clause .= ' AND fvl.cancelled = 0 ';
        db_set_active('espresso2');
        $query = 'SELECT f.code as code,fvl.*
                            FROM resource_has_feature_value rhfv
                            INNER JOIN feature_value_lang fvl ON fvl.id_feature_value = rhfv.id_feature_value
                            INNER JOIN feature as f ON f.id_feature = fvl.id_feature
                            WHERE rhfv.id_resource = :id_resource 
                            ' . $where_clause . '
                            ORDER BY fvl.prog';
//        echo $query;die;
        $result = db_query(
                $query, array(
            ':id_lang' => $this->idLang,
            ':id_resource' => $idResource,
//            ':feature_code' => $featureCode,
                ), array('fetch' => PDO::FETCH_ASSOC)
        );
//        print_r($result);die;
        $return = array();
        if ($result->rowCount()>0) {
            while ($row = $result->fetchAssoc()) {
                $return[ $row['code'] ] = $row;
            }
        } else {
//            echo $result->rowCount()." features found for resource $idResource and lang ".$this->idLang."<br>";
        }
        db_set_active();
//        print_r($return);die;
        return $return;
    }

    /**
     * Funzione per restituire tutte le risorse di un prodotto, filtrate x tassonomia di appartenenza (es. DOC)
     */
    /*
      private function getDocTaxonomyResource( $id_taxonomy_term, $id_product ) {

      $idLang = $this->getRequest()->getSession()->get(LoginCostanController::SESSION_ID_LANG_FRONTEND);
      $this->calcUsrLevel();
      $resources = $this->getDoctrine()->getRepository('Espresso2PimBundle:Product')->getResourceByTaxonomy ( $id_product, $id_taxonomy_term, $this->usrLevel, $idLang );
      $ciclo=0;
      if(count($resources)>0) {
      foreach($resources as $cnt => $aRes) {

      if( !$this->isResourceAuthorized($aRes['res_visibility']) ) {
      $resources[$cnt]['isAuthorized'] = 'NO';
      unset($resources[$cnt]);
      continue;
      }
      $aFeatures = $this->getDoctrine()->getRepository('Espresso2PimBundle:Resource')->
      getFeaturesResourceLang($aRes['id_resource'], $idLang, null);
      foreach($aFeatures as $kk => $aFeature) {
      $resources[$cnt]['features'][$aFeature['code']] = $aFeature;
      }
      $ciclo++;
      }
      }
      return $resources;
      }
     */

    /**
     * Restituisce array di tutte i valori delle features associate al prodotto, raggruppate per id feature e codice feature
     * 
     * @return array 
     */
    public function getProductFeatures() {
//        echo __CLASS__."::".__FUNCTION__."(".__LINE__.")".PHP_EOL;
        global $language;
        db_set_active('espresso2');
        $query = "SELECT feature_value_lang_1.id_feature_value as id_feat_value, feature_value_lang_1.id_feature, 
                fv.name as feature_name,
                feature_value_lang_1.string_value as value_string, 
                feature_value_lang_1.text_value as value_text, 
                feature_value_lang_1.boolean_value as value_boolean, 
                feature_value_lang_1.integer_value as value_integer, 
                feature_value_lang_1.decimal_value as value_decimal, 
                f.code as feature_code, f.id_feature_type, 
                phfv.id_feature_category as id_feature_category,
                fc.code as feature_category,
                fcl.name as feature_category_name,
                feature_value_lang_1.id_lang 
                FROM product_has_feature_value as phfv  
                INNER JOIN feature_category fc on (phfv.id_feature_category=fc.id_feature_category)
                left JOIN feature_category_lang fcl on(fc.id_feature_category=fcl.id_feature_category and (fcl.id_lang=:id_lang OR fcl.id_lang=0))
                INNER JOIN feature_value_lang as feature_value_lang_1 ON phfv.id_feature_value = feature_value_lang_1.id_feature_value
                INNER JOIN feature f on(feature_value_lang_1.id_feature=f.id_feature)
                and feature_value_lang_1.id_lang in(0, :id_lang)
                left join feature_lang fv on(f.id_feature=fv.id_feature and (fv.id_lang=:id_lang or fv.id_lang=0) )
                WHERE 1 
                and phfv.id_product = :id_product
                and feature_value_lang_1.cancelled=0
                #and feature_value_lang_1.boolean_value=1
                order by feature_value_lang_1.prog
                ";
        $result = db_query(
                $query, array(
                    ':id_lang' => $this->idLang, //1
                    ':id_product' => $this->idProduct, //3276
                ), array('fetch' => PDO::FETCH_ASSOC)
        );

        $return = array();
//        echo "Count:".$result->rowCount();die;
        if ($result->rowCount()>0) {
            while ($row = $result->fetchAssoc()) {
//                $row['media'] = Esp2UserDocUtility::getFeatureResource($row['id_feature'], $row['feature_code'], Esp2UserDocUtility::getIdLang($language->language));
                $return[ $row['feature_code'] ] = $row;
            }
        }
        db_set_active();
        return $return;
    }

    /**
     * Retrieves product resources
     * 
     * @param integer $idResourceGroup
     * @param integer $idFeatureCategory
     * @param integer $idFeature
     * @param char $codeResourceClass
     * @return type
     */
    public function getProductResources( $idResourceGroup = null, $idFeatureCategory = null, $idFeature = null, $codeResourceClass=null) {
        
        db_set_active('espresso2');
        $idProduct = $this->idProduct;
        $idLang = $this->idLang;
        $idChannel = $this->idChannel;
        
        /** Filtro per: Resource GROUP ? **/
        if ($idResourceGroup != null) {
            $sql_resgroup = 'AND rgc.id_resource_group = ' . $idResourceGroup;
        } else {
            $sql_resgroup = '';
        }
        
        /** Filtro per: Resource CLASS ? **/
        $sql_resgclass = '';
        if (isset($codeResourceClass)){
                $sql_resgclass = " AND rc.code = '$codeResourceClass'";
        }
        
        /** Filtro per: privilegi GESTORE ?? **/
        if($_SESSION['espresso2']['user']['company_class']!='GESTORE')
//        	 $sql_hide_standby = "HAVING pubblica=1";
//               $sql_hide_standby = " AND isResourceStandby(r.id_resource)=1 ";
        	 $sql_hide_standby = "";
        else
             $sql_hide_standby = "";
        
        /** Filtro per: Feature e FeatureCategory ? **/
        if (isset($idFeature) && isset($idFeatureCategory)) {

            $sel = ",fvl.string_value as caption";
            $join = 'LEFT JOIN ( resource_has_feature_value AS rhfv
                         INNER JOIN feature_value_lang AS fvl ON rhfv.id_feature_value = fvl.id_feature_value 
        	    AND rhfv.id_feature_category = $idFeatureCategory 
                    AND fvl.id_feature = $idFeature 
                    AND fvl.id_lang in( 0, :id_lang)
        	    )  ON r.id_resource = rhfv.id_resource';
        } else {
            $sel = "";
            $join = "";
        }   
        
        $query = 'SELECT 
                isResourceStandby(r.id_resource) as isResStandBy,
                r.id_resource, r.code, r.localized, rc.store_dam, rc.id_resource_class,rc.code as resource_class,
                rl.id_lang, 
                getResourceVisibility(r.id_resource) as visibility,
                m.id_media, m.id_channel, m.erp_code, m.uri, m.uri_thumb, m.mime_type, m.file_name, m.keywords
                ' . $sel . '			 
                FROM resource AS r 
                INNER JOIN product_has_resource AS phr ON r.id_resource= phr.id_resource
                INNER JOIN resource_class AS rc ON r.id_resource_class = rc.id_resource_class
                LEFT JOIN resource_group_channel AS rgc ON (r.id_resource = rgc.id_resource AND rgc.id_channel = :id_channel ' . $sql_resgroup . ')
                INNER JOIN resource_lang AS rl ON (r.id_resource= rl.id_resource AND (rl.id_lang=:id_lang OR rl.id_lang=0) )
                ## !!!! TEST: togliere id channel is null !!!!
                INNER JOIN media AS m ON rl.id_media = m.id_media  AND (m.id_channel = :id_channel )
                ' . $join . '					
                WHERE 
                phr.id_product = :id_product 
                '.$sql_hide_standby.' 
                '.$sql_resgclass.'
                ORDER BY phr.prog';
        
                //#OR phr.id_product IN (select product_relation.id_main_product from product_relation where product_relation.id_product = :id_product)
                
        $result = db_query(
                $query, array(
                ':id_lang' => $this->idLang,
                ':id_product' => $this->idProduct,
                ':id_channel' => $this->idChannel,
                ), array('fetch' => PDO::FETCH_ASSOC)
        );
//        echo print_r($result->fetchAssoc());die;
        $return = array();
        if ($result->rowCount()>0) {
            $ciclo = 0;
            while ($row = $result->fetchAssoc()) {
                
//                echo $row['id_resource']." :: CICLO RISORSA $ciclo"."--".empty($this->filters).PHP_EOL;
//                echo print_r($row);
                $res_feat = $this->getFeaturesResourceLang($row['id_resource'] );
                $row['res_features'] = $res_feat;
                $res_date = date("Y-m-d", $res_feat['DATA']['integer_value']);
                $res_ts   = $res_feat['DATA']['integer_value'];

                /** 
                 * CONTROLLO FILTRI sulle feature delle  risorse (nome e data)
                 **/
                if(!empty($this->filters)) {
//                    echo "APPLICO FILTRI".PHP_EOL;
                    foreach ($res_feat as $feat_code => $feature ) {
                        switch($feat_code){
                            case "NOME":
                                if( strlen($this->filters['title'])>0 ) {
                                    if( stripos( $feature['string_value'] , $this->filters['title'])===false ) {
//                                        echo $row['id_resource']." :: STRING NOT MATCH: ".$this->filters['title']." in ".$feature['string_value'].PHP_EOL;
                                        unset($row); //remove current resource from result list
                                        break 2;
                                    } else {
//                                        echo $row['id_resource']." :: STRING MATCH: ".$this->filters['title'].PHP_EOL;
                                    }
                                } 
                                break;
                            case "DATA":
                                strlen($this->filters['date_from'])>0  ?  $ts_from = strtotime($this->filters['date_from']." 00:00:00")   : $ts_from = strtotime("2000-01-01 00:00:00");
                                strlen($this->filters['date_to'])>0    ?  $ts_to   = strtotime($this->filters['date_to']." 23:59:59")     : $ts_to   = strtotime( date("Y-m-d H:i:s") );
                                if( $ts_from < $res_ts &&  $ts_to > $res_ts ) {
//                                    echo $row['id_resource']." :: DATE MATCH $res_date".PHP_EOL;
                                } else {
//                                    echo $row['id_resource']." :: DATE NOT MATCH $res_date".PHP_EOL;
                                    unset($row);
                                    break 2;
                                }
                                break;
                        }
                    }
                    if(isset($row)) {
//                        echo $row['id_resource']." :: RISORSA ESISTE ANCORA".PHP_EOL;
                    } else {
//                        echo $row['id_resource']." :: RISORSA CANCELLATA".PHP_EOL;
                        continue;
                    }
                } else {
//                    echo "NESSUN FILTRO".PHP_EOL;
                }
                    

                /**
                 * CONTROLLO VISIBILITA' RISORSA <--> LIVELLO UTENTE
                 */
                if (!$this->isResourceAuthorized($row['visibility'])) {
                    //risorsa NON visibile
//                    unset($row); 
                    $return[$res_date] = $row; //DEBUG
                } else {
                    //conteggio tipo risorsa
                    $this->resCount[$row['resource_class']][$row['mime_type']] ++;
//                    $row ['res_url'] = $this->getResUrl($row);
                    $return[$res_date] = $row;
                }
                $ciclo++;
            }
        } else {
//            echo $this->idProduct." --> No resources found<br>";echo "<hr>";
            $return = array();
        }
        db_set_active();
//        echo"<br>";print_r($return);die;
        ksort($return);
        return $return;
    }

    /**
     * Product::isResourceAuthorized()
     * controlla livello di visibilita' della risorsa con il livello di privilegio dell'utente (company_class) in sessione
     * 
     * @param string $visibility
     * @return bool
     */
    private function isResourceAuthorized($visibility) {

        if (!isset($this->usrLevel)) {
            $this->calcUsrLevel();
        }
        $visibility == '' ? $visibility = 'PUBBLICO' : $visibility = $visibility;
        $res_level = $this->cfg['res'][$visibility];
        return $this->usrLevel >= $res_level;
    }

    private function calcUsrLevel() {

        $logged = $_SESSION['espresso2']['user']['isLogged'];
        if ($logged) {
//            $user = $session->get(LoginCostanController::SESSION_USER);
            $user = $_SESSION['espresso2']['user'];
            $this->usrLevel = $this->cfg['usr'][$user['company_class']]; //Utente loggato	
        } else {
            $this->usrLevel = $this->cfg['usr']['PUBBLICO']; //Utente pubblico
        }
    }

    private function setUsrLevel($usrLevel = null) {
        $this->usrLevel = isset($usrLevel) ? $usrLevel : $this->cfg['usr']['PUBBLICO'];
    }

    /**
     * Retrieves resource url
     * NOT IMPLEMENTED
     *      * 
     * @return string
     */
    private function getResUrl() {

        $url = '';
        return $url;
    }

    /**
     * Retrieves Html Template (HTML_CODE) associated to the product and its corrensponding feature values (Title, Text1, text2, id_vimeo)
     * 
     * 1. resource with resclass=RS_DOCHTML --> resource feature (TEMPLATE_CODE) value == resource code --> feature = HTML_CODE
     * 2. resource feature (TEMPLATE_CODE) value
     * 3. resource code = feature value
     * 4. resource feature = HTML_CODE
     * 
     * @param type $idResource
     * @param type $idMedia
     * @param type $storeDam
     */
    private function templatePreviewAction($idResource, $idMedia, $storeDam) {
        
        $feat_cat = Esp2UserDocUtility::getFeatureCategoryByCode(Esp2UserDocUtility::TPL_HTML_FEATCAT);
        $template = Esp2UserDocUtility::getFeatureByCode(Esp2UserDocUtility::TPL_HTML_FEATURE);
        $this->html_tpl = $template;
        return true;
    }
    
}

?>

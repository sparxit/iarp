<?php
/**
 * Class for Espresso2 UserDocs page
 * retrieves all products with 
 */
class Esp2UserDocs {

    private $idLang;
    private $username;
    private $idChannel;
    private $idTaxTerm;
    private $prodClass;
    private $idProdClass;
//    private $idResGroup;
    private $idOwnerFeature; //id attributo proprietario
    private $idFeatureCat;
    private $usrLevel;
    private $cfg;
    private $prod_cfg; //parametri per singolo prodotto
    

    public function __construct( $username, $langCode, $prod_cfg  ) {
        $this->username     = $username;
        $this->prod_cfg     = $prod_cfg;
        $this->idLang       = Esp2UserDocUtility::getIdLang($langCode);
        $this->idChannel    = Esp2UserDocUtility::getIdChannel();
        $this->idTaxTerm    = Esp2UserDocUtility::getIdPersonalDocTaxTerm();
        $this->cfg          = Esp2UserDocUtility::getPrivileges();
//        $this->idResGroup   = Esp2UserDocUtility::getIdPersonalDocResGroup(); //nn serve qui, le risorse vengono estratte nella classe prodotto userdoc singolo
        $this->prodClass    = Esp2UserDocUtility::getProductClass();
        $this->idProdClass  = Esp2UserDocUtility::getIdProductClass();
        $this->idFeatureCat = Esp2UserDocUtility::getIdFeatcat(Esp2UserDocUtility::P_USERDOC_FEATURE_CAT );
        $this->idOwnerFeature = Esp2UserDocUtility::getIdFeature();
//        $this->titleIdFeature = Esp2UserDocUtility::getTitleIdFeature();
//        print_r($this);die;
        return true;
    }
    
    /**
     * Retrieves all products with defined product_class and with 'owner' feature (as defined in Utility Class) = current username
     * @return type
     */
    public function getDocs ( $title=null, $date_from=null, $date_to=null ) {
//        print_r(func_get_args());die;
//        ini_set("display_errors", "on");error_reporting(E_ALL);
        
        /** =========================================================================== 
         * NON C'E' controllo sullo stato StandBy/Pubblica per i documenti utente
         * ===========================================================================  **/
//        if(isset($_SESSION['espresso2']['user']) && $_SESSION['espresso2']['user']['company_class'] != 'GESTORE') {
//            $sql_hide_standby = " HAVING pubblica=1";
//        } else {
//            $sql_hide_standby = "";
//        }
        $sql_hide_standby = "";
        db_set_active('espresso2');
        $query = "SELECT 
                phtt.id_taxonomy_term,
                p.id_product,
                p.erp_code,
                pc.code as prod_class,
                #phfv.id_feature_category,
                #fc.code as feat_cat,
                #fvl.id_feature,
                #f.code as feature,
                fvl.string_value as name,
                p.active = 1,
                isProductStandby(p.id_product) as pubblica	
                FROM 
                product_has_taxonomy_term as phtt
                INNER JOIN product as p ON p.id_product = phtt.id_product
                join product_class pc on(p.id_product_class=pc.id_product_class)
                ## prod name
                INNER JOIN (
                        product_has_feature_value as phfv  
                        INNER JOIN feature_value_lang as fvl ON (
                                                phfv.id_feature_value = fvl.id_feature_value
                                                AND phfv.id_feature_category = :id_feature_category
                                                AND fvl.id_feature = :id_feature 
                                                and fvl.id_lang in( 0, :id_lang )
                                                )
                        JOIN feature f on(fvl.id_feature=f.id_feature)
                        ##join feature_category fc on(phfv.id_feature_category=fc.id_feature_category)
                ) ON p.id_product = phfv.id_product

                WHERE 
                phtt.id_taxonomy_term = :id_taxonomy_term 
                AND pc.code = :prod_class
                AND p.active = 1
                AND fvl.string_value = :username
                $sql_hide_standby
                ORDER BY phtt.prog ASC, phtt.id
            ";
//        echo $this->prodClass, PHP_EOL, $this->idOwnerFeature, PHP_EOL, $this->username, PHP_EOL, $this->idFeatureCat, PHP_EOL, $this->idOwnerFeature, PHP_EOL, $this->idTaxTerm, PHP_EOL;
//        echo "DocsQuery:".$query;die;
        $result = db_query(
                $query, array(
                ':id_lang' => $this->idLang,
                ':prod_class' => $this->prodClass,
                ':id_feature' => $this->idOwnerFeature,
                ':username' => $this->username,
                ':id_feature_category' => $this->idFeatureCat,
                ':id_taxonomy_term' => $this->idTaxTerm,
//                ':id_resource_group' => $this->idResGroup,
//                ':id_feature_category' => $this->idFeatureCat,
            ), array('fetch' => PDO::FETCH_ASSOC)
        );
        $return = array();
//        print_r($result);die;
        
        $filters = array();
        if ($result) {
            /** FORM FILTERS **/
            if( strlen($title)>0 )      
                $filters['title'] = $title;
            if( strlen($date_from)>0 )      
                $filters['date_from'] = $date_from;
            if( strlen($date_to)>0 )      
                $filters['date_to'] = $date_to;

            while ($row = $result->fetchAssoc()) {

                $p = new Esp2UserDoc( $row['id_product'] , $this->prod_cfg, $filters);
//        print_r($p);die;
                /** IF THERE ARE NO RESOURCES (i.e. user documents) ==> unset whole product **/
                if( empty($p->resources)) {
                    unset($row);
                }
                else{
                    $row['product'] = $p;
                    $return[] = $row;
                }

            }
        }

        db_set_active();
//        print_r($return);die;
        $ret2 = array_reverse($return, TRUE);
        return $ret2;
        
    }
    
    /**
     * Product::isResourceAuthorized()
     * controlla livello di visibilita' della risorsa con il livello di privilegio dell'utente (company_class) in sessione
     * 
     * @param string $visibility
     * @return bool
     */
    private function isResourceAuthorized($visibility) {

        if (!isset($this->usrLevel)) {
            $this->calcUsrLevel();
        }
        $visibility == '' ? $visibility = 'PUBBLICO' : $visibility = $visibility;
        return $this->usrLevel >= $this->cfg['res'][$visibility];
    }

    private function calcUsrLevel() {

        $logged = $_SESSION['espresso2']['user']['isLogged'];
        if ($logged) {
//            $user = $session->get(LoginCostanController::SESSION_USER);
            $user = $_SESSION['espresso2']['user'];
            $this->usrLevel = $this->cfg['usr'][$user['company_class']]; //Utente loggato	
        } else {
            $this->usrLevel = $this->cfg['usr']['PUBBLICO']; //Utente pubblico
        }
    }

    private function setUsrLevel($usrLevel = null) {
        $this->usrLevel = isset($usrLevel) ? $usrLevel : $this->cfg['usr']['PUBBLICO'];
    }
    
    
}

?>

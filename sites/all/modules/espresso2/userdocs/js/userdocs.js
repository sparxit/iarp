

(function($) {

    $(document).ready(function() {

        var dF = jQuery( "#date_from" );
        var dT = jQuery( "#date_to" );

                dF.datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'yy-mm-dd',
                        showOn: "button",
                        buttonImage: "/sites/all/modules/espresso2/userdocs/images/calendar.png",
                        buttonImageOnly: true
                });
                dT.datepicker({
                        showOtherMonths: true,
                        selectOtherMonths: true,
                        changeMonth: true,
                        changeYear: true,
                        dateFormat: 'yy-mm-dd',
                        showOn: "button",
                        buttonImage: "/sites/all/modules/espresso2/userdocs/images/calendar.png",
                        buttonImageOnly: true
                });
        
    });


}(jQuery));
<?php // echo"<br>Docs:<br>"; print_r($aDocs);die; ?>
<link rel="stylesheet" href="/<?php echo $path ?>/css/userdocs.css">
<script type="text/javascript" src="/<?php echo $path; ?>/js/userdocs.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>


<div class="userdocs_container">
    <?php
    /* Check if user is logged */
    if($_SESSION['espresso2']['user']['isLogged']!=1) {
        echo "<h5>". t('You are not authorized to access this page.')."</h5>";
        return;
    }

    ?>
    <form action='<?php echo $_SERVER["REQUEST_URI"] ?>' method='post' id='userdocs-search-form'>
    <div id="userdocs_search" class="">
        <div class="search_box search_left">
            <h5><?php echo t('Filter by title'); ?></h5>
            <div class='string_search'>
                    <input type='text' class='input-text' value='<?php echo $_POST['search_string']; ?>' size='25' name='search_string' />
            </div>
        </div>
        <div class="search_box search_right">
            <h5><?php echo t('Filter by date'); ?></h5>

                <div class='date_from_box'>
                        <!--<label><?php echo t('From date'); ?></label>-->
                        <input type='text' id='date_from' name='date_from' value='<?php echo $_POST['date_from']; ?>' placeholder="<?php echo t('From date'); ?>">
                        <!--<img class="ui-datepicker-trigger" src="/<?php echo $path; ?>/images/calendar.png" alt="" title="">-->
                </div>
                <div class='date_to_box'>
                        <!--<label><?php echo t('To date'); ?></label>-->
                        <input type='text' id='date_to' name='date_to' value='<?php echo $_POST['date_to']; ?>' placeholder="<?php echo t('To date'); ?>">
                        <!--<img class="ui-datepicker-trigger" src="/<?php echo $path; ?>/images/calendar.png" alt="" title="">-->
                </div>
        </div>
        <div class="search_submit">
            <h5><br></h5>
            <div class="submit">
                    <input type="submit" alt="Search" title="Search" name="op" value="<?php echo t('Search'); ?>">
            </div>
        </div>
    </div>
    </form>
    <?php if( empty($aDocs) ): ?>
        <h5><?php echo t('No results found'); ?>.</h5>
    <?php else: ?>
    
    <ul>
        <li class="userdocs_item userdocs_header" id="">
            <span class="userdocs_thumb"></span>
            <span class="userdocs_title"><?php echo t('Title'); ?></span>
            <span class="userdocs_date"><?php echo t('Date'); ?></span>
        </li>
    <?php foreach ($aDocs as $doc): ?> 
        <?php $d = $doc['product']; ?>
        
        <?php foreach ($d->resources  as $date => $res): ?> 
        <li class="userdocs_item" id="<?php echo $res['id_resource']; ?>">
                <span class="userdocs_thumb">
                    <a href="<?php echo variable_get('url_esp2_exporteweb')."/". $res['file_name']; ?>" target="_blank">
                        <img src="/<?php echo $path."/images/comunicati_stampa.png"; ?>">
                    </a>
                </span>
                <span class="userdocs_title"><?php echo $res['res_features']['NOME']['string_value']; ?></span>
                <span class="userdocs_date"><?php echo $date; ?></span>
        
        </li>
        <?php endforeach; ?>
        
        
    <?php endforeach; ?>
    </ul>
    <?php endif; ?>
</div>



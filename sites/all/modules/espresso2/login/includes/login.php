<?php

class Esp2Login {

    const AUTH_URL = "http://192.168.1.182/espresso2/web/app.php/pim/Authentication";
    const ESP2_USER_MODULE = "web_eurocryor";
    const ESP2_USER_GESTORE = "GESTORE";
    function __construct() {

    }

    public function login($username, $password ) {

        $data = array();
        $data["username"] = $username;
        $data["password"] = $password;
        $data["module"] = self::ESP2_USER_MODULE;
//        $logged = $this->checkLoginInternal($data);
        $logged = $this->checkLoginAction($data);
//        print_r($_SESSION['espresso2']);die;
        return $logged;
    
    }
    
    private function checkLoginInternal( $data  ) {

        /** FAKE LOGIN temporanea **/
        $stored['username'] = 'tksoluzioni';
        $stored['password'] = 'password';
        $stored['module'] = 'web_eurocryor';
        $stored['company_class'] = 'INTERNO';
        $stored['isLogged'] = false;
        
        if($data['username']==$stored['username'] && $data['password']==$stored['password'] && $data['module']==$stored['module'] ) {
            $stored['isLogged'] = true;
            $_SESSION['espresso2']['user'] = $stored;
            return array("response"=>TRUE, "code"=>"AUTH01", "authenticated"=>1);
        } else {
            return array("response"=>FALSE, "code"=>"AUTH01", "authenticated"=>0);
        }
        /** FAKE LOGIN temporanea **/
        
        $salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $pass =  $data['password'].'{'.$salt.'}';
        
        db_set_active('espresso2');
        $query = "select u.* from user u 
            join user_module um on(u.id_user=um.id_user)
            join module m on(um.id_module=m.id_module)
            where username=:username and password=PASSWORD(:password) and m.code=:module";
        
        $result = db_query(
                $query, 
                array( 
                    ':username' => $data['username'],
                    ':password' => $pass,
                    ':module'   => $data['module'],
                ), 
                array( 'fetch' => PDO::FETCH_ASSOC )  
        );
        
        $row = $result->fetchAssoc();
        db_set_active();
        if(!result) {
            return false;
        } else {
            return $row;
        }
                
    }
    
    public function checkLoginAction($dataPost){
//        print_r(func_get_args());
    	$data = array();
        $data["username"] = $dataPost["username"];
        $data["password"] = $dataPost["password"];
        $data["module"] = self::ESP2_USER_MODULE;
        /**
         * external authentication --> sistemare AuthController su .181
         */
        $hostController = self::AUTH_URL;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $hostController);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);
        
        if ($response === false) {
            throw new Exception("Problem reading data from $url, $php_errormsg");
//            return $php_errormsg;
        } else {

            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $array = json_decode($json, TRUE);
            $return = Array();
            
            if (( $array['authenticated'] == 0)) {
                $return['response'] = false;
                $return['code'] = $array['error'];
            } else {
                /** Authenticated **/
//                $_SESSION['espresso2']['user']['userLevel'] = 'PUBBLICO';
                $_SESSION['espresso2']['user'] = $array;
                $_SESSION['espresso2']['user']['username'] = $data['username'];
                $_SESSION['espresso2']['user']['module'] = $data['module'];
                $_SESSION['espresso2']['user']['isLogged'] = 1;
                $return['response'] = true;
            }
        }
        return $return;
    }

    
    public function logout(){
        
        unset($_SESSION['espresso2']);
        header('Location: http://'.$_SERVER['HTTP_HOST']); //redirect to home page
        return true;
    }

}

?>

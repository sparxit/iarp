
<div id="esp2-login">

    <form accept-charset="UTF-8" id="esp2-login-form" method="post" action="/login" >
        <div>
            <div class="form-item form-type-textfield form-item-name">
                <label for="edit-username">
                    Username <span title="" class="form-required" >*</span>
                </label>
                <input type="text" class="form-text required" maxlength="255" size="60" value="" name="username" id="edit-username">
            </div>
            <div class="form-item form-type-textfield form-item-mail">
                <label for="edit-mail">
                    Password <span title="" class="form-required">*</span>
                </label>
                <input type="password" class="form-text required" required="" maxlength="255" size="60" value="" name="password" id="edit-password">
            </div>


            <div id="edit-actions" class="form-actions form-wrapper">
                <input type="submit" class="button default form-submit" value="Login" name="op" id="edit-submit">
            </div>
        </div>
            <div class="hide contact-form-responce">
                <p></p>
            </div>
    </form>
    <?php if(isset($errorMsg)): ?>
    <div class="login-error"><?php echo $errorMsg; ?></div>
    <?php endif; ?>
</div>
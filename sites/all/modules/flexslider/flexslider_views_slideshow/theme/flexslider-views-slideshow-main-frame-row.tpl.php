<?php

/**
 * @file
 * Template for the Flex Slider row
 *
 * @author Mathew Winstone (minorOffense) <mwinstone@coldfrontlabs.ca>
 */
?>
  <li class="<?php print $classes; ?> tk-li-slide_<?php echo $count; ?>">
    <?php print $rendered_items; ?>
  </li>
  
<?php

/**
 * @file
 * Template for the FlexSlider row container
 *
 * @author Mathew Winstone (minorOffense) <mwinstone@coldfrontlabs.ca>
 */
?>
<div class="flex-nav-container">
  <div class="flexslider">
    <ul id="flexslider_views_slideshow_<?php print $variables['vss_id']; ?> tk-ul-slide_<?php print $variables['vss_id']; ?>" class="<?php print $classes; ?>">
      <?php print $rendered_rows; ?>
    </ul>
  </div>
</div>
<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".tk-prelink").click(function() {
				$.fancybox({
				'overlayColor': '#000',
				'autoscale' : false,
				'transitionIn' : 'elastic',
				'transitionOut': 'elastic',
				'padding' : 0,
				//'title' : this.title,
				'width':'100%',
				'height':'100%',
				'type' : 'swf',
				'href' : 'http://vimeo.com/moogaloop.swf?clip_id=1119834&title=0&amp;byline=0&amp;portrait=0', //this.href,
				//'swf' : { 'wmode':'transparent', 'allowfullscreen':'true'}
				});
				return false;
			});
		});
     </script>
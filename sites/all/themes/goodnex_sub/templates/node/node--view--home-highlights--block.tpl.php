<?php 
//print_r($content); die;
global $root, $base_url, $language;
$lng = $language->language;
$share_url = $base_url.'/node/'.$node->nid;
isset($content['field_portfolio_image']) ? $image = $content['field_portfolio_image'] : $image='';
isset($content['field_portfolio_title']) ? $inner_title = $content['field_portfolio_title'] : $inner_title='';

$link = "#";
if (count($node->field_portfolio_link)>0) {
  $link_ref = field_get_items('node', $node, 'field_portfolio_link', $lng);
  $link = $link_ref[0]['url'];
}

//dpm($lng);
//dpm($base_url);
//dpm($node->field_portfolio_link);
//dpm($link);
//echo "LANG:".$lng."<br>";
//print_r($node);
?>

  <div class="span3 portfolio-item portfolio-item-first" id="<?php echo $node->nid; ?>">
        <h3><?php print $title; ?> <span><img src="/sites/all/themes/goodnex_sub/images/plus-icon.png" /></span></h3>
        <div class="content-tip">
                <span class="tooltip-img"><?php print render($image); ?></span>
                <h5><?php print ($node->field_portfolio_title[$lng][0]['value']); ?></h5>
                <p><?php print render($content['field_portfolio_description']); ?></p>
                <div class="inner_link">
                    &raquo; <a href="<?php print $link; ?>" class="portfolio-inner-link"><?php print $node->field_portfolio_inner_link[$lng][0]['value']; ?></a>
                </div>
                    
                
        </div>
    
  </div>





<!-- <div class="span3 portfolio-item portfolio-item-first">
    <div class="view_portfolio_image"><a href="<?php print $link; ?>"><?php print render($image); ?></a></div>
    <a href="<?php print $link; ?>">
    <div class="mask">
      <h3><?php print $title; ?></h3>
      <div class="portfolio_tags"><?php print render($content['field_portfolio_description']); ?></div>
      <?php /*<a href="<?php print $node_url; ?>" class="info">Learn More</a> */ ?>
    </div>
    </a>
  </div>-->

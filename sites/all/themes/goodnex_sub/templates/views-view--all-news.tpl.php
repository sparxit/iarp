<?php
//echo('<hr>View All news old<hr>');die;
/**
 * @file
 * Main view template.
 *
 * Variables available:
 * - $classes_array: An array of classes determined in
 *   template_preprocess_views_view(). Default classes are:
 *     .view
 *     .view-[css_name]
 *     .view-id-[view_name]
 *     .view-display-id-[display_name]
 *     .view-dom-id-[dom_id]
 * - $classes: A string version of $classes_array for use in the class attribute
 * - $css_name: A css-safe version of the view name.
 * - $css_class: The user-specified classes names, if any
 * - $header: The view header
 * - $footer: The view footer
 * - $rows: The results of the view query, if any
 * - $empty: The empty text to display if the view is empty
 * - $pager: The pager next/prev links to display, if any
 * - $exposed: Exposed widget form/info to display
 * - $feed_icon: Feed icon to display, if any
 * - $more: A link to view more, if any
 *
 * @ingroup views_templates
 */
?>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.11.0/jquery-ui.js"></script>
<script>
		  /** Set contextual filters:
		   * 	the number of items in this array must match the numbers and the order of view's contextual filters
		   *	1. title
		   *	## 2. body (disabled)
		   *	3. date range
		  **/
		var aSearch = new Array( "*",  "*" );	  
		var fDate = '';var tDate = ''; //from date | to date
		
		/** Single date filter **/
		function setSingleDateFilter( dd , target) {

			if( target=='f' ) {
				fDate = dd;
			} 
			if( target=='t' ) {
				tDate = dd;
			} 
			
			aSearch[1] = fDate +'--'+ tDate;
			if(aSearch[1]=='--')
					aSearch[1]='*';

		}
		 
		/**  Prepares contextual filter strings and submits the form with GET query string **/
		function searchSubmit() {
		
			var sSearch = '';
			for(i=0; i<aSearch.length; i++ ) {
				sSearch = sSearch+'/'+aSearch[i];
			}
			
			// Clean previous filters from url
			var pathArray = window.location.pathname.split( '/' );
			pathArray.splice(0,1); //remove 1st array (empty)
			
			// trim degli argomenti va fatto SOLO se non è il primo caricamento dellla pagina (solo se ci sono degli argomenti nel path)
			var minLen;
			if(pathArray.length<=2) { //es. /en/news-and-events
				minLen=pathArray.length+aSearch.length;
			}
			else {
				minLen=pathArray.length;
				var path_num = minLen - aSearch.length;
				pathArray.splice(path_num, (pathArray.length - path_num) );
			}
			
			var pathStr = '';
			for( n=0;n<pathArray.length;n++ ) {
				pathStr = pathStr+'/'+pathArray[n]
			}

			var url = window.location.protocol +'//'+ window.location.host + pathStr + sSearch;
			location.href = url;
		}
		
			  jQuery(document).ready(function() {
			  
			  		var dF = jQuery( "#news_date_from" );
			  		var dT = jQuery( "#news_date_to" );

						dF.datepicker({
							showOtherMonths: true,
							selectOtherMonths: true,
							changeMonth: true,
							changeYear: true,
							dateFormat: 'yymmdd',
							showOn: "button",
							buttonImage: "/sites/all/themes/astrum/images/calendar.png",
							buttonImageOnly: true
						});
						dT.datepicker({
							showOtherMonths: true,
							selectOtherMonths: true,
							changeMonth: true,
							changeYear: true,
							dateFormat: 'yymmdd',
							showOn: "button",
							buttonImage: "/sites/all/themes/astrum/images/calendar.png",
							buttonImageOnly: true
						});
						dF.on('change', function(){
							setSingleDateFilter( dF.val() , 'f');
						});
						dT.on('change', function(){
							setSingleDateFilter( dT.val() , 't');
						});
						
						jQuery('#custom-search-form input[name="keys"]').on('change', function(){
							aSearch[0] = jQuery(this).val();
							//aSearch[1] = jQuery(this).val();
						});
						jQuery('.years_array li').on('click', 'a', function(){
							// Set Date filter with $(this) year
							var anno = jQuery(this).attr('rel'); 
							aSearch[1] = anno+'0101--'+anno+'1231';
							searchSubmit();
						});
						
						jQuery('#custom-search-form .submit input').click(function(e){
							e.preventDefault();
							searchSubmit();
							return false;
						});

			  });

</script>
<?php $years = astrum_get_news_years(); ?>

<form action="/search/node" method="post" id="custom-search-form">
	<div class="form-item">
		<div>
			<label><?php print t('Search news'); ?></label><input type="text" class="input-text" value="" size="25" name="keys" />
			<input type="hidden" value="<?php print drupal_get_token('search_form'); ?>" name="form_token" />
			<input type="hidden" value="search_form" id="edit-search-form" name="form_id" />
			<input type="hidden" name="type[news]" id="edit-type-news" value="news" />
		</div>
		<div class="clearfix" style="margin-bottom:10px;"></div>
		<div class="date_from_box">
			<label><?php print t('From'); ?></label><input type="text" id="news_date_from" name="date_from">
		</div>
		<div class="date_to_box">
			<label><?php print t('To'); ?></label><input type="text" id="news_date_to" name="date_to">
		</div>
		<div class="years_array">
			<ul>
			<?php foreach($years as $yr): ?>
					<li><a rel="<?php echo $yr; ?>"><?php echo $yr; ?></a></li>
			<?php endforeach;?>
			</ul>
		</div>
		<div class="submit">
			<input type="submit" value="<?php print t('Search'); ?>" name="op" title="<?php print t('Search'); ?>" alt="<?php print t('Search'); ?>" />
		</div>
			
		<hr>
				
	</div>
</form>


<div class="<?php print $classes; ?>">
  <?php print render($title_prefix); ?>
  <?php if ($title): ?>
    <?php print $title; ?>
  <?php endif; ?>
  <?php print render($title_suffix); ?>
  <?php if ($header): ?>
    <div class="view-header">
      <?php print $header; ?>
    </div>
  <?php endif; ?>

  <?php if ($exposed): ?>
    <div class="view-filters">
      <?php print $exposed; ?>
    </div>
  <?php endif; ?>

  <?php if ($attachment_before): ?>
    <div class="attachment attachment-before">
      <?php print $attachment_before; ?>
    </div>
  <?php endif; ?>

  <?php if ($rows): ?>
    <div class="view-content">
      <?php print $rows; ?>
    </div>
  <?php elseif ($empty): ?>
    <div class="view-empty">
      <?php print $empty; ?>
    </div>
  <?php endif; ?>

  <?php if ($pager): ?>
    <?php print $pager; ?>
  <?php endif; ?>

  <?php if ($attachment_after): ?>
    <div class="attachment attachment-after">
      <?php print $attachment_after; ?>
    </div>
  <?php endif; ?>

  <?php if ($more): ?>
    <?php print $more; ?>
  <?php endif; ?>

  <?php if ($footer): ?>
    <div class="view-footer">
      <?php print $footer; ?>
    </div>
  <?php endif; ?>

  <?php if ($feed_icon): ?>
    <div class="feed-icon">
      <?php print $feed_icon; ?>
    </div>
  <?php endif; ?>

</div><?php /* class view */ ?>

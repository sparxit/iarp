(function ($) {
  
  jQuery(document).ready(function () {
    
  //** OLD Functions to evaluate the dims of the highlights home block images sizes, related to the container width and their # numbers *****
  
    var highlightsNumber = jQuery('.view-home-highlights .views-row').length ;
    //alert (highlightsNumber);
    var widthPecentage = ((100/highlightsNumber) -3) + '%';
    //alert(widthPecentage);
    //jQuery('.view-home-highlights .view-content .views-row').width(widthPecentage);
    
    $(window).resize(function() {
    //alert($(window).width());
    });
    
  
  //** Functions for the tk_nav_homeblocks menu links behaviours *****
			  $('#nav_link_prodotti').click(function(){
					$('html, body').animate({
						scrollTop: $(".tk-panels-anchor").offset().top -100
					}, 1000);
			  });
				 
			  $('#nav_link_evidenza').click(function(){
					$('html, body').animate({
						scrollTop: $(".tk-portfolio").offset().top -100
					}, 1000);
			  });

			  $('#nav_link_panels_new').click(function(){
					$('html, body').animate({
						scrollTop: $("#tk-panels-new").offset().top -100
					}, 1000);
			  }); 
                          
			  $('#nav_link_highlights').click(function(){
					$('html, body').animate({
						scrollTop: $(".tk-highlights").offset().top -100
					}, 1000);
			  }); 

  
  });
  
})(jQuery);

// Test of QUO Module
(function ($) {
  $(document).ready(function(){

      function sliderIstantlyBack () {
        $$('#ei-slider').style('z-index', -1);
        //alert($$('#ei-slider').style('z-index'));
        setTimeout(function(){
        $$('#ei-slider').style('z-index', 5);
        },1000);
      
      }
      
      //$('#tk-scrolling-panels-wrapper').on('scroll', function(){
      //  sliderIstantlyBack ();
      //});
      
      //$$(document).on('swipe', function(){
      // sliderIstantlyBack ();
      //});
      
  });
})(jQuery);
<?php

/**
 * @file
 * Display ds_node_full column stacked template.
 */

  // Add sidebar classes so that we can apply the correct width in css.
  if (($image && !$page_content) || ($page_content && !$image)) {
    $classes .= ' group-one-column';
  }
?>
<<?php print $layout_wrapper; print $layout_attributes; ?> class="ds_node_full <?php print $classes;?> clearfix">

  <?php if (isset($title_suffix['contextual_links'])): ?>
  <?php print render($title_suffix['contextual_links']); ?>
  <?php endif; ?>

  <<?php print $header_wrapper ?> class="group-header <?php print $header_classes; ?>">
    <?php print $header; ?>
  </<?php print $header_wrapper ?>>

  <?php if ($image): ?>
    <<?php print $image_wrapper ?> class="group-image <?php print $image_classes; ?>">
      <?php print $image; ?>
    </<?php print $image_wrapper ?>>
  <?php endif; ?>

  <?php if ($page_content): ?>
    <<?php print $page_content_wrapper ?> class="group-page_content <?php print $page_content_classes; ?>">
      <?php print $page_content; ?>
    </<?php print $page_content_wrapper ?>>
  <?php endif; ?>

  <<?php print $footer_wrapper ?> class="group-footer <?php print $footer_classes; ?>">
    <?php print $footer; ?>
  </<?php print $footer_wrapper ?>>

</<?php print $layout_wrapper ?>>

<?php if (!empty($drupal_render_children)): ?>
  <?php print $drupal_render_children ?>
<?php endif; ?>

<? if ( $node->type == 'news') { //dpm($node); ?>

  <script type="text/javascript">
    jQuery(document).ready(function () {
      
      //alert ('ciao');
      
        var imagewidth = jQuery('.group-image').width();
          //alert(imagewidth);
          //alert ('ciao');
          
        jQuery('.field-slideshow-wrapper, .field-slideshow, .field-slideshow-slide, .field-slideshow-slide a').width(imagewidth);
        jQuery('.field-slideshow').css("padding-right", 0);
        jQuery('.field-slideshow-slide img').attr("width", imagewidth).attr("heigth", 'auto');
        
          
        //jQuery.each('.group-image', function( key, value ) {
        //  //alert( key + ": " + value );
        //  alert( key);
        //
        //});
    });
  </script>
  
<? } ?>

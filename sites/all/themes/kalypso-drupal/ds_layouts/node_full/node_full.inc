<?php
function ds_node_full() {
  return array(
    'label' => t('Node - Full'),
    'regions' => array(
        'header' => t('Header'),
        'image' => t('Image'),
        'page_content' => t('Page Content'),
        'footer' => t('Footer'),
    ),
    // Add this line if there is a default css file.
    'css' => TRUE,
  );
}
?>
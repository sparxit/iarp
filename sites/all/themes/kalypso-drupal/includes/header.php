<?php 
function kalypso_header($page){
  global $root;
?>

<?
//Carico il nodo 42 che corrisponde all'immagine di background
$node = node_load('42');
//dpm($node);
$background_url_ref = field_get_items('node', $node, 'field_background_image');
$background_url = $background_url_ref[0]['uri'];
//dpm($background_url);
?>
	<div id="page_wrapper">
	<div id="background_image_wrapper" style="background: url('<?php file_create_url($background_url); ?>');">
    	<img src="<?php echo file_create_url($node->field_background_image['und'][0]['uri']) ?>" />
    </div>
    
    <div class="container">
      <div class="row">  
      
       <?php 
           if (drupal_is_front_page() && theme_get_setting('enable_language_selector') == '1' ) {
				?>
                
                <div class="tk-language_selector front_page">
                <?php print render($page['language_selector']); ?> 
				</div>
                
                <?php 
	      	}
        ?>
      
        <div class="span2"> 
          <div id="vert_nav">
          <header>
          <?php if (theme_get_setting('branding_type') == 'logo'): ?>
          <div id="main_title">
            <a href="<?php print base_path();?>"><img src="<?php print file_create_url('public://logo-bonnet-bg.png'); ?>" /></a>
          </div>
          <?php endif; ?>
          
          <?php if (theme_get_setting('branding_type') == 'text'): ?>
            <a href="<?php print base_path();?>">
            <div id="main_title">
              <h1 id="main_title_text"><?php print variable_get('site_name'); ?></h1>
            </div>
            </a>
          <?php endif; ?>
	  
	  
            <!-- begin menu -->
	    <?php if ($page['main_menu']): ?>
            <div id="menu" class="row">
	          	<div id="main-menu" class="navigation">
		          	<div id="menu_wrap">
					<?php print render($page['main_menu']); ?>

				     </div>
				</div> 
			</div>
	    <?php endif; ?>	    
            <!-- end menu -->   
            
            
            
                
            </header> 
          </div> 
        </div>
        <!-- end main span2 -->  
<script type="text/javascript">
function scrollbar() {
    var viewportHeight = window.innerHeight ? window.innerHeight : $(window).height();

    if (jQuery.browser.msie) {
        if(parseInt(jQuery.browser.version) == 7) {
            viewportHeight -= 3;
        }
    }

    if(viewportHeight <= $('#wrapper').height()) {
        return true;
    } else {
        return false;
    }
}
</script>          
<script type="text/javascript">
  jQuery(document).ready(function ($) {
	
	  $().UItoTop({ easingType: 'easeOutQuart' });
	 
      $('input[type="submit"]').addClass('btn');
      
   	  $('ul.menu').superfish();
	
	  $('ul#quotes').quote_rotator();
	
	  $('#menu').mobileMenu();
	  
	  $("#google_map").fitMaps( {w: '100%', h:'370px'} ); 
	 
	  jQuery("ul.accordion li").each(function(){
	    if(jQuery(this).index() > 0){
	    jQuery(this).children(".accordion-content").css('display','none');
	    }
	    else{
	    jQuery(this).find(".accordion-head-image").addClass('active');
	    }
	
	    jQuery(this).children(".accordion-head").bind("click", function(){
	    jQuery(this).children().addClass(function(){
	    if(jQuery(this).hasClass("active")) return "";
	      return "active";
	    });
	    jQuery(this).siblings(".accordion-content").slideDown();
	    jQuery(this).parent().siblings("li").children(".accordion-content").slideUp();
	    jQuery(this).parent().siblings("li").find(".active").removeClass("active");
	    });
	  });
	});
    
</script>
<?php }
?>


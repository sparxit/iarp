<?php

function kalypso_bootstrap_slider($page){
  global $root, $language;
  $lng = $language->language;
  $slide_number = theme_get_setting('slides_number');
?>
	<div class="row">
    <div class="span10">
      <div id="myCarousel" class="carousel slide">
      
		   	<div class="carousel-inner">
			    <?php $i = '1'; while ($i <= $slide_number) { ?>
			    <div class="<?php if ($i == '1') {echo "active ";} ?>item">
			      <a href="<?php echo theme_get_setting('slide_url_'.$i.''); ?>">
			      	<img src="<?php print file_create_url(theme_get_setting('slide_path_'.$i.'')); ?>">
			      </a>
			      <?php if (theme_get_setting('slide_caption_'.$i.'') != '') : ?>
			    	<a href="<?php echo theme_get_setting('slide_url_'.$i.''); ?>"><div class="carousel-caption">
				    	<p><?php echo theme_get_setting('slide_caption_'.$i.''); ?></p>
				    </div></a><!-- end caption -->
				    <?php endif; ?>
			    </div><!-- end item -->
			    <?php $i++; } ?>
			  </div>
			  <!-- end carousel-inner -->
			
			  <!-- Carousel nav -->
			  <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
			  <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
		  
		  </div>
		  <!-- end myCarousel -->            
		</div>
  </div>
  
  <script type="text/javascript">
		jQuery(document).ready(function ($) {
		  $('.carousel').carousel({
		    interval: 5000
		  })
		})
  </script>
          
<?php }

function kalypso_nivo_slider($page){
  global $root, $language;
  $lng = $language->language;
  $slide_number = theme_get_setting('slides_number');
?>
	<div class="row">
    <div class="span10">
      <div class="slider-wrapper theme-default">
        
        <div id="slider" class="nivoSlider">
          <?php $i = '1'; while ($i <= $slide_number) { ?>  
          	<a href="<?php echo theme_get_setting('slide_url_'.$i.''); ?>">
            	<img src="<?php print file_create_url(theme_get_setting('slide_path_'.$i.'')); ?>" alt="slider" title="#htmlcaption<?php echo $i;?>">
            </a>
          <?php $i++; } ?>              
        </div>
        
        <?php $i = '1'; while ($i <= $slide_number) { ?> 
        <div id="htmlcaption<?php echo $i;?>" class="nivo-html-caption">
         <?php echo theme_get_setting('slide_caption_'.$i.''); ?>
        </div>
        <?php $i++; } ?> 
        
      </div>
    </div>
  </div>
  
  <script type="text/javascript">
    jQuery(document).ready(function ($) {
    	$('#slider').nivoSlider();
    });
  </script>
<?php }

function kalypso_elastic_image_slider($page){
  global $root, $language;
  $lng = $language->language;
  $slide_number = theme_get_setting('slides_number');
  
$query = new EntityFieldQuery;    

$sliders_result = $query
     ->entityCondition('entity_type', 'node')
     ->propertyCondition('status', 1) // Getting published nodes only.
     ->propertyCondition('type', 'slider') //Getting 'employees' type only.
     // How do I include custom field as part of query?
     ->execute();
     


$slider_nodes = node_load_multiple(array_keys($sliders_result['node']));

$slide_number = count($sliders_result['node']);

//dpm($slider_nodes);
//dpm($slide_number);

?>
      <div id="ei-slider" class="ei-slider">
        <ul class="ei-slider-thumbs">
        	<li class="ei-slider-element">Current</li>
          <?php
	  $sliders = array();
	  foreach ($slider_nodes as $i=>$slider) {
	  
	  //Immagine Slide
	  $slide_img_ref = field_get_items('node', $slider, 'field_slide_image');
	  $slide_img = $slide_img_ref[0]['uri']; ?>
          
	  <li><a href="#">Slide <?php echo $i; ?></a><img src="<?php print file_create_url($slide_img); ?>" alt="thumb" height="60" width="150" /></li>
          <?php } ?>
        </ul>
        <!-- ei-slider-thumbs -->
        
		
        <ul class="ei-slider-large">
	  
          <?php foreach ($slider_nodes as $i=>$slider) {
	  
	  //Immagine Slide
	  $slide_img_ref = field_get_items('node', $slider, 'field_slide_image');
	  $slide_img = $slide_img_ref[0]['uri'];
	  
	  //Link Slide
	  $slide_url_ref = field_get_items('node', $slider, 'field_slide_link');
	  $slide_url = $slide_url_ref[0]['url'];
	  
	  //Titolo Slide
	  $slide_title_ref = field_get_items('node', $slider, 'title_field');
	  $slide_title = $slide_title_ref [0]['value'];
	  
	  //Sotto_Titolo Slide
	  $slide_subtitle_ref = field_get_items('node', $slider, 'body');
	  $slide_subtitle = $slide_subtitle_ref [0]['value'];
	  
		$check_vimeo = strpos($slide_url, "://vimeo.com");
		$check_youtube =strpos($slide_url,"www.youtube.com");
		
		$begin_a_link_tag = '<a href="#">';
		
		if ($check_vimeo > 0) {
		    $video_url = $slide_url;
		    $video_url = str_replace("https://vimeo.com", "http://player.vimeo.com/video", $slide_url );

		    $begin_a_link_tag = '<a class="colorbox-load" href="'.$video_url.'?width=800&amp;height=600&amp;iframe=true&amp;fullscreen=1&amp;autoplay=1">';
		}
		
		elseif ($check_youtube > 0) {
		    $video_url = str_replace("youtube.com/watch?v=", "youtube.com/v/", $slide_url );
		    $begin_a_link_tag = '<a class="colorbox-load" href="'.$video_url.'?fs=1&amp;width=800&amp;height=600&amp;hl=en_US1&amp;iframe=true&amp;rel=0&amp;autoplay=1&amp;controls=0&amp;version=3&amp;enablejsapi=1">';
		} else {
		    $begin_a_link_tag = '<a href="/'.$lng.'/'.$slide_url.'">';
		}
		
		//echo "[debbuging]: ".$slide_url." | ".$begin_a_link_tag;
		//https://vimeo.com/63561960
		 
	      ?>
          <li>
          	<?php echo $begin_a_link_tag; ?>
            	<img src="<?php print file_create_url($slide_img); ?>" alt="slider">
            </a>
            <?php echo $begin_a_link_tag; ?><div class="ei-title">
            	<?php echo "<h2>".$slide_title."</h2><h3>".$slide_subtitle."</h3>"; ?>
            </div></a>
          </li>
          <?php } ?>        
        </ul>
        <!-- ei-slider-large -->
        
        
      </div>
      <!-- ei-slider -->
  
  <script type="text/javascript">
    jQuery(document).ready(function ($) {
    	$('#ei-slider').eislideshow({
				animation			: 'center',
				autoplay			: true,
				slideshow_interval	: 5000,
				titlesFactor		: 0
		  });
    });
  </script>
  <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".tk-lightboxlink").click(function() {
				$.fancybox({
				'overlayColor': '#000',
				'autoscale' : false,
				'transitionIn' : 'elastic',
				'transitionOut': 'elastic',
				'padding' : 0,
				//'title' : this.title,
				'width':'100%',
				'height':'100%',
				'type' : 'swf',
				'href' : 'http://vimeo.com/moogaloop.swf?clip_id=1119834&title=0&amp;byline=0&amp;portrait=0', //this.href,
				//'swf' : { 'wmode':'transparent', 'allowfullscreen':'true'}
				});
				return false;
			});
		});
     </script>
<?php } ?>
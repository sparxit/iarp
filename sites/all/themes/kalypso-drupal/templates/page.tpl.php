<?php

kalypso_header($page);
?>

        <div>
        
          
          <div class="row">
            <div id ="main_content_wrap" class="<?php
	    //if (theme_get_setting('page_layout') == 'sidebar_right') { echo "span6"; } else { echo "span10"; }
	    if ($is_front==false) { echo "span6"; } else { echo "span10"; };
	    
	    ?>">
              <div id="main_content">
       <?php 
           if (!drupal_is_front_page() && theme_get_setting('enable_language_selector') == '1' ) {
				?>
                
                <div class="tk-language_selector_page">
                <?php print render($page['language_selector']); ?> 
				</div>
                
                <?php 
	      	}
        ?>
		
		    <?php if ($messages && user_is_logged_in()): ?>
                    <div id="messages"><div class="section clearfix">
                      <?php print $messages; ?>
                    </div></div> <!-- /.section, /#messages -->
                  <?php endif; ?>
		      
             	 
				  <?php if ($tabs): ?>
				    <div class="tabs">
				      <?php print render($tabs); ?>
				    </div>
				  <?php endif; ?>
				  
                <?php if ($breadcrumb): ?>
                    <div id="breadcrumb" class="clearfix"><?php print $breadcrumb; ?></div>
                <?php endif; ?>
		
					      <?php print render($page['help']); ?>
					      <?php if ($action_links): ?>
					        <ul class="action-links">
					          <?php print render($action_links); ?>
					        </ul>
					      <?php endif; ?>
					      
                      <?php print render($title_prefix); ?>

                      <?php if ($title): ?>
                        <header class="clearfix">
                          <h1 id="page-title">
                            <?php print $title; ?>
                          </h1>
                        </header>
                      <?php endif; ?>

                      <?php print render($title_suffix); ?>
	             	
	             	<?php print render($page['content']); ?>
	            </div>
	          </div>
        
		        <?php if (theme_get_setting('page_layout') == 'sidebar_right'): ?>
		        
		          <div class="span4">
		            <div id="sidebar_wrap">
		          	<?php if ($page['sidebar_first']): ?>
		          	<aside id="sidebar-first" role="complementary" class="sidebar clearfix">
		            <?php print render($page['sidebar_first']); ?>
		            </aside>  <!-- /#sidebar-first -->
		            <?php endif; ?>
		            <?php if ($page['sidebar_second']): ?>
		            <aside id="sidebar-second" role="complementary" class="sidebar clearfix">
			          <?php print render($page['sidebar_second']); ?>
			          </aside>  <!-- /#sidebar-first -->
			          <?php endif; ?>
		          </div>        
		        </div>
		        <?php endif; ?>
		      </div>
        </div>    
      </div>
    </div>  
  
<?php kalypso_footer($page);?>
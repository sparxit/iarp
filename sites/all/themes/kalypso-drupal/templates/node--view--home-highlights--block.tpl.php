<?php 
global $root, $base_url, $language;
$lng = $language->language;
$share_url = $base_url.'/node/'.$node->nid;
$image = $content['field_portfolio_image'];
$link = "#";
if (count($node->field_portfolio_link)>0) {
  $link_ref = field_get_items('node', $node, 'field_portfolio_link', $lng);
  $link = $link_ref[0]['url'];
}

//dpm($lng);
//dpm($base_url);
//dpm($node->field_portfolio_link);
//dpm($link);

?>

  <div class="span3 portfolio-item portfolio-item-first">
    <div class="view_portfolio_image"><a href="<?php print $link; ?>"><?php print render($image); ?></a></div>
    <a href="<?php print $link; ?>">
    <div class="mask">
      <h3><?php print $title; ?></h3>
      <div class="portfolio_tags"><?php print render($content['field_portfolio_description']); ?></div>
      <?php /*<a href="<?php print $node_url; ?>" class="info">Learn More</a> */ ?>
    </div>
    </a>
  </div>
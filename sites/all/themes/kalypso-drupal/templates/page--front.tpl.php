<?php kalypso_header($page); global $root;

drupal_add_js(drupal_get_path('theme', 'kalypso') .'/js/home_behaviours.js');
?>

	      <?php 
	        
	        if (theme_get_setting('enable_slider') == '1' && theme_get_setting('slider_type') == 'default' ) {
				?>
                <div id="tk-slider-wrapper">
		  <div class="tk-slider">
                <?php
	      	  kalypso_elastic_image_slider($page);
			    ?>
				</div></div>
                <?php    	}  ?>

      <!-- main span10 -->
	    <div class="span10">
	      
	      <?php if (theme_get_setting('enable_highlight') == '1') { ?>
	      <div class="row">
	        <div class="span10">
            
            
            <div id="tk_nav_homeblocks">
	    <div id="nav_link_prodotti"><?php if (theme_get_setting('enable_panels') == '1') { echo t(theme_get_setting('panels_title')).'&nbsp;&nbsp;|&nbsp;&nbsp'; }?></div> 
	    <div  id="nav_link_evidenza"><?php if (theme_get_setting('enable_portfolio') == '1') { echo t(theme_get_setting('portfolio_title')).'&nbsp;&nbsp;|&nbsp;&nbsp'; }?></div>
	    <div id="nav_link_panels_new"><?php if (theme_get_setting('enable_panels_new') == '1') { echo t(theme_get_setting('panels_new_title')).'&nbsp;&nbsp;|&nbsp;&nbsp';; }?></div>
	    <div id="nav_link_highlights"><?php if (theme_get_setting('enable_highlights') == '1') { echo t(theme_get_setting('highlights_title')); }?></div>
	    </div>
            
	          <div id="page_heading">
	          <h2 class="page_heading_text"><?php echo theme_get_setting('highlight_text'); ?></h2>
	          </div>
	        </div>
	      </div> 
	      <?php } ?>

          
        <div id="tk-scrolling-panels-wrapper">	      
	<?php if (theme_get_setting('enable_panels') == '1') { ?>
	<div id="tk-panels-wrapper">
        <div class="tk-panels-margin"></div>  
        <div class="tk-panels-anchor"></div> 
        <div id="tk-panels" class="tk-panels">  
	      <div class="row">
	        <div class="span10">
		  		    <?php if ($messages && user_is_logged_in()): ?>
                    <div id="messages"><div class="section clearfix">
                      <?php print $messages; ?>
                    </div></div> <!-- /.section, /#messages -->
                  <?php endif; ?>
	          <div class="section_title"><span class="section_title_text"><?php echo theme_get_setting('panels_title'); ?></span></div>
	        </div> 
	      </div>
	    
	      <div class="row">
	        <div id="panels"> 
	          
	          <div class="span_panels">
	          	<div class="panel_wrap">
	              <?php print render($page['panel_1']); ?> 
	            </div>
	          </div>
	    
	        <div class="span_panels">
	          <div class="panel_wrap">
	            <?php print render($page['panel_2']); ?> 
	          </div>
	        </div>
	    
	        <div class="span_panels">
	        	<div class="panel_wrap">
	        	  <?php print render($page['panel_3']); ?>  
	          </div>
	        </div>
            <div class="span_panels">
	          	<div class="panel_wrap">
	              <?php print render($page['panel_4']); ?> 
	            </div>
	          </div>
	    
	        <div class="span_panels">
	          <div class="panel_wrap">
	            <?php print render($page['panel_5']); ?> 
	          </div>
	        </div>
	    
	        <div class="span_panels">
	        	<div class="panel_wrap">
	        	  <?php print render($page['panel_6']); ?>  
	          </div>
	        </div>
	        
	      </div>
	    </div>
        
      </div>


      
      
	    <?php if($page['panel_full']) {?>
     <div class="tk-panel-full">
	    <div class="row">
	        <div id="panels"> 
	          <?php print render($page['panel_full']); ?>
	        </div>
	    </div>
      </div>	</div> 
	    <?php } } ?>	    <!-- end panels -->
	    
      
      <!-- Panels_New -->	    
	<?php if (theme_get_setting('enable_panels_new') == '1') { ?>
	<div id="tk-panels-new-wrapper">
	<div class="tk-panels-new-margin"></div>  
        <div class="tk-panels-new-anchor"></div> 
	<div id="tk-panels-new" class="tk-panels">
  	      <div class="row">
		<div class="span10">
	  	   <?php if ($messages && user_is_logged_in()): ?>
                    <div id="messages"><div class="section clearfix">
                      <?php print $messages; ?>
                    </div></div> <!-- /.section, /#messages -->
	  	    <?php endif; ?>
		    
		    <div class="section_title"><span class="section_title_text"><?php echo t(theme_get_setting('panels_new_title')); ?></span></div>
      </div>
		</div>
            
	    <?php if ($page['panels_new']): ?>
	    
                <?php print render($page['panels_new']); ?>
		
            <?php endif; ?>
	    
	</div></div><!-- end panels_new -->
	
	<?php }  ?>
	
	     <?php if (theme_get_setting('enable_portfolio') == '1') { ?>
	   <div id="tk-portfolio-wrapper">
	   <div id="tk-portfolio" class="tk-portfolio">  
         <div class="row">
	        <div class="span10">
	          <div class="section_title"><span class="section_title_text"><?php echo t(theme_get_setting('portfolio_title')); ?></span></div>
	        </div> 
	      </div>   
	     
	      <div id="portfolio">
	        <div class="row">
	          <?php print render($page['portfolio']); ?>  
	        </div>
	      </div>
        </div></div><!-- end portfolio --> 
	      <?php } ?>
	   
	     <?php if (theme_get_setting('enable_highlights') == '1') { ?>
	     <div id="tk-highlights-wrapper">
	   <div id="tk-highlights" class="tk-highlights">
           <div class="row">
	      <div class="span10">
	          <div class="section_title"><span class="section_title_text"><?php echo t(theme_get_setting('highlights_title')); ?></span></div>
	      </div>
	      	</div> 
	   
	      <div id="highlights">
	        <div class="row">
	          <?php print render($page['highlights_home_content']); ?>  
	        </div>
	      </div>
        </div></div> <!-- end highlights --> 
	      <?php } ?>
      
      </div> <!-- scrolling-panels --> 	
	
	      <?php if (theme_get_setting('enable_contact') == '1') { ?>
	      <div class="row">
	        <div class="span10">
	          <div class="section_title"><span class="section_title_text"><?php echo theme_get_setting('contact_title'); ?></span></div>
	        </div>
	      </div>
	      
	      <?php if (theme_get_setting('enable_map') == '1') { ?>
	      <div class="row">
	        <div class="span10">
	          <div id="google_map">
	            <iframe width="1170" height="370" src="<?php echo theme_get_setting('map_address'); ?>"></iframe><br />      
	          </div>
	        </div>
	      </div>
	      <?php } ?>
	    
	      <div class="row">   
	        <div class="span4">
	          <?php print render($page['contact_1']); ?> 
	        </div> 
	      
	        <div class="span2">        
	        <?php print render($page['contact_2']); ?> 
	        </div>
	   
	        <div class="span4">
	          <?php print render($page['contact_3']); ?>  
	        </div>
	      </div>
	      
	      <?php if($page['contact_full']) {?>
	      <div class="row">
	        <div id="panels"> 
	          <?php print render($page['contact_full']); ?>
	        </div>
	      </div>
	      <?php } } ?>     
	    
	      <?php if (theme_get_setting('enable_clients') == '1') { ?>
	    <div class="tk-clients">  
          <div class="row">
	        <div class="span10">
	          <div class="section_title"><span class="section_title_text"><?php echo theme_get_setting('clients_title'); ?></span></div>
	        </div>
	       </div>
	     </div>
         
	      <div id="clients" class="row" >       
	        <?php print render($page['clients']); ?> 
	      </div>
	      
	      <?php } ?>
	      
	    </div>
	  </div>       
	</div>
	<!-- end main body container -->  

<?php kalypso_footer($page); // Call Footer ?>